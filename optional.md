# Optional

---
#### Motivation
```Java
public User getUserById(int id) {
    for (User user : users)
        if (user.getId() == id)
            return user;
    return null;
}
```
<!-- .element: class="fragment"-->

<ul>
    <li class="fragment"><code>return null</code> notwendig für Compiler</li>
    <li class="fragment">Probleme
        <ul>
            <li class="fragment fade-in-then-semi-out"><code>null</code></li>    
            <li class="fragment">für den Aufrufer unerwartet</li>    
        </ul>
    </li>
</ul>
    
----
#### Lösung: `Exception`
```Java
class NoSuchUserException extends Exception() { ... }
```
<!-- .element: class="fragment"-->

```Java
public User getUserById(int id) throws NoSuchUserException {
    for (User user : users)
        if (user.getId() == id)
            return user;
    throw new NoSuchUserException();
}
```
<!-- .element: class="fragment"-->

<ul>
    <li class="fragment fade-in-then-semi-out"><code>null</code> ✔️</li>    
    <li class="fragment fade-in-then-semi-out">für den Aufrufer unerwartet ✔️</li>    
    <li class="fragment">checked Exception 🤮</li>    
</ul>

----
#### Lösung: `RuntimeException`
```Java
public User getUserById(int id) {
    for (User user : users)
        if (user.getId() == id)
            return user;
    throw new NoSuchElementException();
}
```
<!-- .element: class="fragment"-->

<ul>
    <li class="fragment fade-in-then-semi-out"><code>null</code> ✔️</li>    
    <li class="fragment">für den Aufrufer unerwartet 😕</li>    
</ul>

----
#### Lösung: Spezielles Objekt
```Java
class User {

    public static final User NO_SUCH_USER = new User(...);
    ...
}
```
<!-- .element: class="fragment"-->

```Java
public User getUserById(int id) {
    for (User user : users)
        if (user.getId() == id)
            return user;
    return User.NO_SUCH_USER;
}
```
<!-- .element: class="fragment"-->

<ul>
    <li class="fragment fade-in-then-semi-out"><code>null</code> ✔️</li>    
    <li class="fragment fade-in-then-semi-out">für den Aufrufer unerwartet ✔️</li>    
    <li class="fragment">gefährlich ⚠️(Programm crasht nicht)</li>    
</ul>

----
<h4>Lösung: <code class="yellow">@Nullable</code></h4>

```Java
public @Nullable User findUserById(int id) {
    for (User user : users)
        if (user.getId() == id)
            return user;
    return null;
}
```
<!-- .element: class="fragment"-->

<ul>
    <li class="fragment fade-in-then-semi-out"><code>null</code> ✔️</li>    
    <li class="fragment">für den Aufrufer unerwartet ✔️</li>    
    <li class="fragment">IDE-abhängig 😕</li>    
    <li class="fragment">nicht standardisiert 😕</li>    
</ul>

----
#### Lösung: `Optional`
```Java
public Optional<User> findUserById(int id) {
    for (User user : users)
        if (user.getId() == id)
            return Optional.of(user);
    return Optional.empty();
}
```
<!-- .element: class="fragment"-->

<ul>
    <li class="fragment fade-in-then-semi-out"><code>null</code> ✔️</li>    
    <li class="fragment">für den Aufrufer unerwartet ✔️</li>    
    <li class="fragment">standard</li>    
</ul>

---
#### `Optional<T>`
```Java
public final class Optional<T> {

    private final T value;
    ...
}
```
<!-- .element: class="fragment"-->

Container, der vielleicht `null` enthält
<!-- .element: class="fragment"-->

----
#### Basics
<dl>
    <dt class="fragment"><code>Optional&ltT> of(T value)</code></dt>
    <dd class="fragment">erzeugt nichtleeres <code>Optional</code></dd>
    <dd class="fragment">kapselt <code>value</code></dd>
    <dt class="fragment"><code>Optional&ltT> empty()</code></dt>
    <dd class="fragment">erzeugt leeres <code>Optional</code></dd>
    <dt class="fragment"><code>Optional&ltT> ofNullable(T value)</code></dt>
    <dd class="fragment">leer, falls <code>value == null</code></dd>
    <dt class="fragment"><code>T get()</code></dt>
    <dd class="fragment">liefert gespeicherten <code>value</code> oder wirft <code>NoSuchElementException</code></dd>
</dl>

note: `of` wirft Exception falls `value == null`

---
#### Ablauf
```Java
Optional<T> optional = method();
if (optional.isPresent()) {
    T value = optional.get();
    doStuff(value);
} else {
    handleError();
}
```
<!-- .element: class="fragment"-->

```Java
Optional<User> optionalUser = findUserById(42);
if (optionalUser.isPresent()) {
    User user = optionalUser.get();
}
```
<!-- .element: class="fragment"-->

Diese Vorgangsweise klappt immer, geht aber besser
<!-- .element: class="fragment"-->

---
#### Best practice
<ul>
    <li class="fragment fade-in-then-semi-out">Returnwert: <code>Optional</code></li>
    <li class="fragment fade-in-then-semi-out">Variable: <code>null</code></li>
    <li class="fragment fade-in-then-semi-out">Collections: leere Collection</li>
    <li class="fragment fade-in-then-semi-out">Arrays: leeres Array</li>
    <li class="fragment">Parameter von Methoden: Methode überladen</li>
</ul>

note: Instanzvariable - `Optional` nicht *serializable*

---
## Functional Interfaces

----
#### `Predicate<T>`
```Java
public interface Predicate<T> {

    boolean test(T t);
}
```
<!-- .element: class="fragment" -->

Testet, ob eine Bedingung erfüllt ist
<!-- .element: class="fragment" -->

```Java
Predicate<String> isEmpty = String::isEmpty;
Predicate<Integer> isPositive = i -> i > 0;
Predicate<Employee> isAvailable = Employee::isAvailable;
```
<!-- .element: class="fragment" -->

----
#### `Supplier<T>`
```Java
public interface Supplier<T> {

    T get();
}
```
<!-- .element: class="fragment" -->

Liefert ein Objekt
<!-- .element: class="fragment" -->

```Java
Supplier<Employee> newEmployee = Employee::new;
final Random random = new Random();
Supplier<Integer> randomInt = random::nextInt;
Supplier<Integer> randomLottoNumber = () -> random.nextInt(45) + 1;
```
<!-- .element: class="fragment" -->

----
#### `Consumer<T>`
```Java
public interface Consumer<T> {
    
    void accept(T t);
}
```
<!-- .element: class="fragment" -->    
         
Führt eine `void`-Methode mit dem Objekt aus
<!-- .element: class="fragment" -->

```Java
Consumer<String> print = System.out::print;
Consumer<Collection<?>> clear = Collection::clear;
Consumer<Employee> dismiss = Employee::dismiss;
```
<!-- .element: class="fragment" -->
         
----
#### `Function<T, R>`
```Java
public interface Function<T, R> {

    R apply(T t);
}
```
<!-- .element: class="fragment" -->

Transformiert Daten von `T` nach `R`
<!-- .element: class="fragment" -->

```Java
Function<String, Integer> length = String::length;
Function<Integer, Double> cubicRoot = i -> Math.pow(i, 1.0 / 3);
Function<Employee, Project> getProject = Employee::getProject;
```
<!-- .element: class="fragment" -->

```Java
public interface UnaryOperator<T> extends Function<T, T> {
}
```
<!-- .element: class="fragment" -->

---
#### Advanced
```Java
public final class Optional<T> {

    ...
    public void ifPresent(Consumer<? super T> action)
    public void ifPresentOrElse(Consumer<? super T> action, 
            Runnable emptyAction)
    public Optional<T> filter(Predicate<? super T> predicate)
    public <U> Optional<U> map(Function<? super T, ? extends U> mapper)
    public T orElse(T other)
    public T orElseThrow()
    public <X extends Throwable> T orElseThrow(
            Supplier<? extends X> exceptionSupplier) throws X
    ...
}
```
<!-- .element: class="fragment stretch"-->

----
`value` wird von `value.method()` konsumiert
<!-- .element: class="fragment" -->

```Java
if (optionalUser.isPresent()) {
    User user = optionalUser.get();
    user.remove();
} else 
    handleError();
```
<!-- .element: class="fragment" -->

```Java
optionalUser.ifPresent(User::remove);
optionalUser.ifPresentOrElse(User::remove, this::handleError);
```
<!-- .element: class="fragment" -->

**Returnwert** von `value.method()` ist Ziel
<!-- .element: class="fragment" -->
  
```Java
if (optionalUser.isPresent()) {
    User user = optionalUser.get();
    String name = user.getName();
}
```
<!-- .element: class="fragment" -->

```Java
Optional<String> optionalName = optionalUser.map(User::getName);
```
<!-- .element: class="fragment" -->

`empty -> empty`
<!-- .element: class="fragment" -->

----
Defaultwert falls `empty`
<!-- .element: class="fragment" -->

```Java
User user;
if (optionalUser.isPresent()) {
    user = optionalUser.get();
} else 
    user = User.NO_SUCH_USER;
```
<!-- .element: class="fragment" -->

```Java
User user = optionalUser.orElse(User.NO_SUCH_USER);
```
<!-- .element: class="fragment" -->

Exception falls `empty`
<!-- .element: class="fragment" -->

```Java
if (optionalUser.isPresent()) {
    User user = optionalUser.get();
} else
    throw new NoSuchElementException();
```
<!-- .element: class="fragment" -->

```Java
User user = optionalUser.orElseThrow();
User user = optionalUser.orElseThrow(NoSuchUserException::new);
```
<!-- .element: class="fragment" -->