# IO - Streams

---
#### Schließen von Resourcen
```Java
String readFirstLineFromFile(String filename) throws IOException {
    BufferedReader reader = new BufferedReader(
            new FileReader(filename));
    try {
        return reader.readLine();
    } finally {
        reader.close();
    }
}
```
<!-- .element: class="fragment stretch fade-in-then-semi-out" -->

oder besser
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
String readFirstLineFromFile(String filename) throws IOException {
    try (BufferedReader reader = new BufferedReader(
            new FileReader(filename))) {
        return reader.readLine();
    }
}
```
<!-- .element: class="fragment stretch" -->

----
#### `try`-with
<ol>
    <li class="fragment fade-in-then-semi-out"><code>Autoclosable</code> implementieren</li>
    <li class="fragment fade-in-then-semi-out"><code>try</code>-with führt <code>close</code> aus</li>
</ol>

```Java
class Resource implements AutoCloseable {

    @Override
    public void close() throws IOException {
        System.out.println("closed");
    }
}
```
<!-- .element: class="fragment" -->

auch mehrere
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
try (Resource resource = new Resource();
     OtherResource another = new OtherResource()) {
    // Verwendung
}   // Ausgabe "close"
```
<!-- .element: class="fragment" -->

---
#### Motivation
```Java
public class Student {

    private int id;
    private String name;
    private School school;

    ...
}
```
<!-- .element: class="fragment" -->

```Java
School htl = new School("HTL");
Student student = new Student(42, "Bob", htl);
```
<!-- .element: class="fragment" -->

`student` speichern/senden
<!-- .element: class="fragment" -->

----
#### Text
```XML
<Student>
  <id>42</id>
  <name>Bob</name>
  <school>
    <name>HTL</name>
  </school>
</Student>
```
<!-- .element: class="fragment" -->

```JSON
{
  "id": 42,
  "name": "Bob",
  "school": {
    "name": "HTL"
  }
}
```
<!-- .element: class="fragment" -->

```YAML
--&hyphen;
id: 42
name: "Bob"
school:
  name: "HTL"
```
<!-- .element: class="fragment" -->

note: 
* XML
* JSON
* YAML

----
#### Binär
```Plain Text
AC ED 00 05 73 72 00 1A 73 65 72 69
61 6C 69 7A 69 6E 67 2E 64 6F 6D 61
69 6E 2E 53 74 75 64 65 6E 74 0E C3
4C D7 CF 32 96 43 02 00 03 49 00 02
69 64 4C 00 04 6E 61 6D 65 74 00 12
4C 6A 61 76 61 2F 6C 61 6E 67 2F 53
74 72 69 6E 67 3B 4C 00 06 73 63 68
6F 6F 6C 74 00 1B 4C 73 65 72 69 61
6C 69 7A 69 6E 67 2F 64 6F 6D 61 69
6E 2F 53 63 68 6F 6F 6C 3B 78 70 00
00 00 2A 74 00 03 42 6F 62 73 72 00
19 73 65 72 69 61 6C 69 7A 69 6E 67
2E 64 6F 6D 61 69 6E 2E 53 63 68 6F
6F 6C 74 4C D2 E9 49 ED 86 3D 02 00
01 4C 00 04 6E 61 6D 65 71 00 7E 00
01 78 70 74 00 03 48 54 4C
```
<!-- .element: class="fragment stretch" -->

---
![overview](assets/io-streams/streams-overview.png)
<!-- .element: style="margin-top:-5%" -->

----
#### Überblick

|   |Lesen|Schreiben|
|---|:-:|:-:|
|bytes|`%InputStream`|`%OutputStream`|
|chars|`%Reader`|`%Writer`|
|Files|`Files.`|`Files.`

---
#### Bytestreams
```Java
public abstract class OutputStream implements Closeable, Flushable {

    public abstract void write(int b)   // schreibt NIEDRIGSTES Byte von b
    public void write(byte b[]) 
    ...
}
```
<!-- .element: class="fragment stretch" -->

```Java
public abstract class InputStream implements Closeable {

    public abstract int read()  // liest EIN Byte, returnt [0, 255]
    public int read(byte b[])   // returnt Anzahl an gelesenen Byte
    public byte[] readNBytes(int len)
    public byte[] readAllBytes()
    ...
}
```
<!-- .element: class="fragment stretch" -->

* Implementiertung für diverse IO-Devices
<!-- .element: class="fragment fade-in-then-semi-out" -->
* 👎 mühsam 👎
<!-- .element: class="fragment" -->

---
#### `DataOutput`
```Java
public interface DataOutput {

    void writeBoolean(boolean v) throws IOException;
    void writeByte(int v) throws IOException;
    void writeShort(int v) throws IOException;
    void writeChar(int v) throws IOException;
    void writeInt(int v) throws IOException;
    void writeLong(long v) throws IOException;
    void writeFloat(float v) throws IOException;
    void writeDouble(double v) throws IOException;
    void writeBytes(String s) throws IOException;   // ⚠️ nur ASCII ⚠️
    void writeChars(String s) throws IOException;   // wieviele bytes?
    void writeUTF(String s) throws IOException;     // 2B length, data
}
```
<!-- .element: class="fragment stretch" --> 
 
----
#### `DataInput`
```Java
public interface DataInput {

    int skipBytes(int n) throws IOException;  // returnt wieviele geskippt
    boolean readBoolean() throws IOException;
    byte readByte() throws IOException;
    int readUnsignedByte() throws IOException;
    short readShort() throws IOException;
    int readUnsignedShort() throws IOException;
    char readChar() throws IOException;
    int readInt() throws IOException;
    long readLong() throws IOException;
    float readFloat() throws IOException;
    double readDouble() throws IOException;
    String readLine() throws IOException;   // ⚠️ nur ASCII ⚠️
    String readUTF() throws IOException;
}
```
<!-- .element: class="fragment stretch" --> 

werfen `EOFException` bei unerwartetem Dateiende
<!-- .element: class="fragment" --> 

---
#### `RandomAccessFile`
```Java
public class RandomAccessFile 
        implements DataOutput, DataInput, Closeable {

    public RandomAccessFile(String name, String mode)
    public native long length()
    public native long getFilePointer()
    public void seek(long pos)
    public native void setLength(long newLength)
}
```
<!-- .element: class="fragment" --> 

<ul>
    <li class="fragment fade-in-then-semi-out">implementiert <code>DataInput</code> und <code>DataOutput</code></li>
    <li class="fragment fade-in-then-semi-out">sehr mächtig</li>
</ul>
<dl>
    <dt class="fragment"><code>void seek(long pos)</code></dt>
    <dd class="fragment fade-in-then-semi-out">Filepointer wird auf Position <code>pos</code> gesetzt</dd>
    <dt class="fragment"><code>void setLength(long newLength)</code></dt>
    <dd class="fragment fade-in-then-semi-out">schneidet die Datei einfach ab</dd>
</dl>

----
<pre class="fragment stretch"><code class="hljs Java" data-trim data-noescape>
try (RandomAccessFile file = new RandomAccessFile("test.dat", "rw")) {
    file.write(-1);      // 1 Byte
    file.writeInt(42);    // 4 Byte
    file.writeUTF("Godzilla");
    file.writeUTF("ゴジラ");
    file.seek(5);
    String s = file.readUTF();
}
</code></pre>

```Plain Text
FF 00 00 00 2A 00 08 47 6F 64 7A 69 6C 6C 61 00
09 E3 82 B4 E3 82 B8 E3 83 A9
```
<!-- .element: class="fragment stretch" --> 

---
#### `DataOutputStream`/`DataInputStream`
```Java
public class FilterOutputStream extends OutputStream {

    protected OutputStream out;
    ...
}
```
<!-- .element: class="fragment" --> 

```Java
public class DataOutputStream extends FilterOutputStream 
        implements DataOutput {

    public DataOutputStream(OutputStream out) {
        super(out);
    }

    ...
}
```
<!-- .element: class="fragment" --> 

```Java
new DataInputStream(
        new BufferedInputStream(
                new FileInputStream(FILENAME)));
```
<!-- .element: class="fragment" --> 

[Decorator Pattern](https://refactoring.guru/design-patterns/decorator)
<!-- .element: class="fragment" --> 

----
<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
byte[] data;
try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
     DataOutputStream output = new DataOutputStream(baos)) {
    output.writeInt(42);
    output.write(256);
    output.writeUTF("string");
    data = baos.toByteArray();
}
try (DataInputStream input = new DataInputStream(
        new ByteArrayInputStream(data))) {
    int i = input.readInt();
    int b = input.read();
    String string = input.readUTF();
}
</code></pre>

---
#### Charstreams
```Java
public abstract class Writer implements Appendable, Closeable, Flushable {

    public void write(int c)    // schreibt einen char
    public void write(String str)
    public Writer append(char c)
    public Writer append(CharSequence csq)
    ...
}
```
<!-- .element: class="fragment stretch" --> 

```Java
public abstract class Reader implements Readable, Closeable {

    public int read()   // liest einen char
    public long skip(long n)
    public int read(char[] cbuf)
    ...
}
```
<!-- .element: class="fragment stretch" --> 

* Implementiertung für diverse IO-Devices
<!-- .element: class="fragment fade-in-then-semi-out" -->
* 👎 mühsam 👎
<!-- .element: class="fragment" -->

---
#### `PrintWriter`/`BufferedWriter`
```Java
public class PrintWriter extends Writer {

    protected Writer out;

    public void print(...)
    public void println(...)
    ...
}
```
<!-- .element: class="fragment" -->

<ul>
    <li class="fragment fade-in-then-semi-out">bekannt von <code>System.out</code></li>
    <li class="fragment fade-in-then-semi-out">unabhängig vom <em>line ending</em></li>
    <li class="fragment fade-in-then-semi-out">am besten um einen <code>BufferedWriter</code> wrappen, um die Performance zu erhöhen</li>
</ul>

```Java
new PrintWriter(new BufferedWriter(new CharArrayWriter()))
```
<!-- .element: class="fragment" -->

---
#### `BufferedReader`
```Java
public class BufferedReader extends Reader {

    private Reader in;

    public String readLine() // -> null bei EOF
    ...
}
```
<!-- .element: class="fragment" -->

zeilenweises Lesen unabhängig vom line ending
<!-- .element: class="fragment" -->

----
```Java
byte[] data;
try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
     PrintWriter writer = new PrintWriter(
             new BufferedWriter(new OutputStreamWriter(baos)))) {
    writer.println(42);
    writer.println("Hello World");
    writer.printf("%.3f", Math.PI);
    writer.flush();     // stream wird beim closen geflushed
    data = baos.toByteArray();
}
try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(new ByteArrayInputStream(data)))) {
    String line;
    while ((line = reader.readLine()) != null) {
        ...
    }
}
```
<!-- .element: class="fragment stretch" -->

---
#### Best practice

<ul>
    <li class="fragment fade-in-then-semi-out">try-with verwenden</li>
    <li class="fragment fade-in-then-semi-out">buffern</li>
    <li class="fragment fade-in-then-semi-out strike"><code>available()</code></li>
    <li class="fragment fade-in-then-semi-out strike"><code>ready()</code></li>
    <li class="fragment fade-in-then-semi-out">zur EOS-Bestimmung Exceptions catchen</li>
    <li class="fragment">beim Lesen/Schreiben von Dateien <br>
        <code>Files</code>-Methoden verwenden</li>
</ul>

note:
* `available()` und `ready()` Momentaufnahmen, implementierungsabhängig