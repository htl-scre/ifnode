<style>
.center {
    text-align: center !important;
}
.right {
    text-align: right !important;
}
table, th {
    border: 3px solid white;
}
</style>

![java_logo](./assets/Java/java_logo.png)

---
#### Features

* Erstrelease 1995
<!-- .element: class="fragment fade-in-then-semi-out" -->
* angelehnt an C/C++
<!-- .element: class="fragment fade-in-then-semi-out" -->
* objektorientiert
<!-- .element: class="fragment fade-in-then-semi-out" -->
* interpretiert
<!-- .element: class="fragment fade-in-then-semi-out" -->
* statisch typisiert
<!-- .element: class="fragment fade-in-then-semi-out" -->

![tiobe_2020](./assets/Java/tiobe_2020.png)
<!-- .element: class="fragment" style="height:300px" -->

note: TIOBE - Anzahl Suchmaschinenanfragen 

----
#### Interpretiert?
![python_execution](./assets/Java/python_execution.png)  
Compilation bei Bedarf

----
![java_execution](./assets/Java/java_execution.png)

----
#### Details
```Python
def sum(a, b):
    return a + b
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```ARM Assembly
  2           0 LOAD_FAST                0 (a)
              2 LOAD_FAST                1 (b)
              4 BINARY_ADD
              6 RETURN_VALUE
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```plain
111000100110100011010001101101111110110000001001011011000011011011 
010011001000010001011000011011000011100011101101010110101011000101 
100100000101101000100111100000101101101111011011000000101001111100 
```
<!-- .element: class="fragment" -->

note: import dis; dis.dis(sum)

---
### Statisch typisiert
```Java
byte b = 127;
short countries = 195;
int austrians = 8902600;
long population = 7_793_010_144L;
float average = 2.573F;
double gravity = 9.80665;
boolean typed = true;
char character = 'a';
String name = "Java";
```

----
|Typ|Byte|Min|Max|
|:---:|:---:|---|---|
|`byte`| 1 |-128|127|
|`short`| 2 |-32 768|32 767|
|`int`| 4 |-2 147 483 648|2 147 483 647|
|`long`| 8 |-2⁶³<!-- .element: class="fragment" -->|2⁶³-1 <!-- .element: class="fragment" -->|

|Typ|Byte|Stellen exakt|
|:---:|:---:|:---:|
|`float`| 4 |~7|
|`double`| 8 |~16|
<!-- .element: class="fragment" -->

----
#### Two's Complement
```plain
 5 = 0101
```
<pre class="fragment"><code class="hljs plain" data-trim data-noescape>
-5?
<span class="fragment">~5 -> 1010</span>
<span class="fragment">     +   1</span>
<span class="fragment">      ----
      1011 = -5</span>
</code></pre>

![zahlenrad](./assets/Java/twos-complement.png)
<!-- .element: class="fragment" -->

----
#### Overflow
```Java
int max = 2_147_483_647;
System.out.println(max + 1);
```
<!-- .element: class="fragment" -->

```plain
-2147483648
```
<!-- .element: class="fragment" -->

```Java
double max = 1.7976931348623157E308;
System.out.println(2 * max);
```
<!-- .element: class="fragment" -->

```plain
Infinity
```
<!-- .element: class="fragment" -->

---
#### Syntax

<ul>
    <li class="fragment fade-in-then-semi-out">Anweisungen werden mit <code>;</code> abgeschlossen
    <pre><code class="hljs Java" data-trim data-noescape>
doThis();
    </code></pre>
    </li>
    <li class="fragment">Blöcke werden durch <code>{ }</code> gekennzeichnet
    <pre><code class="hljs Java" data-trim data-noescape>
public static void main(String[] args) {
    System.out.println("Hello World");
}
    </code></pre>
    </li>
</ul>

----
#### Hello World
```Python
myFile.py
if __name__ == '__main__':
    print('Hello World')
```

```plaintext
python myFile.py
Hello World
```

```Java
public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Hello World");
    }
} 
```
<!-- .element: class="fragment" -->

```plaintext
javac HelloWorld.java
java HelloWorld
Hello World
```
<!-- .element: class="fragment" -->

note: `sys.argv` == String[] args

---
#### Variablen
```Python
🐍
number_of_students = 25
number_of_students = 24
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
Typ nameOfTheVariable;
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
int numberOfStudents = 25;
int numberOfStudents = 24; <span class="fragment">// 🚫 Typ nur bei Deklaration</span>
<span class="fragment">numberOfStudents = 24;</span>
</code></pre>

```Java
double uninitialised;
```
<!-- .element: class="fragment" -->

---
#### Operationen
<pre class="fragment"><code class="hljs Python" data-trim data-noescape>
🐍
3 + 'Python' -> <span class="fragment">💥</span>
</code></pre>

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
3 + "Java" -> <span class="fragment">"3Java"</span>
</code></pre>

```Java
String + ??? = String
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
3 + 2 + "!"  -> <span class="fragment">"5!"</span>
</code></pre>

----
#### Arithmetik
```Python
🐍
8 / 3 = 2.6666666666666665
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
8 / 3 = 2
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
int / int = int
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
int n = 2;
System.out.println(++n);    // n += 1, dann Ausgabe
System.out.println(n++);    // Ausgabe, dann n =+ 1
System.out.println(n);
```
<!-- .element: class="fragment" -->
```plain
3
3
4
```
<!-- .element: class="fragment" -->

---
#### if
```Java
if (grade == 1) {
    System.out.println("Sehr gut");
}
```
<!-- .element: class="fragment" -->

```Java
if (i % 2 == 0) {
    System.out.println("Gerade");
} else {
    System.out.println("Ungerade");
}
```
<!-- .element: class="fragment" -->

```Java
if (condition) {
    // true
} else {
    // false
}
```
<!-- .element: class="fragment" -->

```Java
if (condition) {
    // true
} else if (otherCondition) {
    // condition false, otherCondition true
} else {
    // condition false, otherCondition false
}
```
<!-- .element: class="fragment" -->

----
#### and/or/not
```Python
🐍
if month == 2 and day == 30 or month > 12:
    raise ValueError('Illegal date')
```
<!-- .element: class="fragment" -->

```Java
if (month == 2 && day == 30 || month > 12) {
    throw new IllegalArgumentException("Illegal date");
}
```
<!-- .element: class="fragment" -->

```Python
🐍
not(7 / 2 == 3)
```
<!-- .element: class="fragment" -->

```Java
!(7 / 2 == 3)
```
<!-- .element: class="fragment" -->

----
#### `switch`/`case`
```Python
🐍
if grade == 'Sehr gut':
    result = 1
elif grade == 'Gut':
    result = 2
...
else:
    raise ValueError('Invalid grade: ' + grade)
```
<!-- .element: class="stretch" -->

```Java
int result = switch (grade) {
    case "Sehr gut", "sehr gut" -> 1;
    case "Gut" -> 2;
    ...
    default -> 
        throw new IllegalArgumentException("Invalid grade: " + grade);
};
```
<!-- .element: class="fragment stretch" -->

---
#### for
```Java
for (int i = 0; i < 3; i++) {
    System.out.println(i);
}
```
<!-- .element: class="fragment" -->

```plain
0
1
2
```
<!-- .element: class="fragment" -->

```Java
for (int i = 0; i < 4; i+=2) {
    System.out.println(i);
}
```
<!-- .element: class="fragment" -->

```plain
0
2
```
<!-- .element: class="fragment" -->

----
```Java
for (init; condition; update) {
    // code
}
```
<!-- .element: class="fragment" -->

```Java
init;
if (condition) {
    // code
}
update;
if (condition) {
    // code
}
update;
if (condition) {
    // code
}
...
```
<!-- .element: class="fragment" -->

```Java
while, break, continue -> wie in Python
```
<!-- .element: class="fragment" -->

---
#### Strings
```Java
char oneCharacter = 'c';
String text = "Hello World";
String number = "42";
```
<!-- .element: class="fragment" -->

```Python
🐍
as_int = int(number)
```
<!-- .element: class="fragment" -->

```Java
int asInt = Integer.parseInt(number);
```
<!-- .element: class="fragment" -->

----
#### Stringformatierung
```Python
🐍
pi = 3.1415926
print(f'pi = {pi:06.2f}')
```
<!-- .element: class="fragment" -->

```plaintext
pi = 003.14
```
<!-- .element: class="fragment" -->

```Java
double pi = 3.1415926;
System.out.printf("pi = %06.2f", pi);
String asString = String.format("pi = %06.2f", pi);
```
<!-- .element: class="fragment" -->

----
#### Einlesen
```Python
🐍
input_string = input('Age: ')
age = int(input_string)
```
<!-- .element: class="fragment" -->

```Java
Scanner scanner = new Scanner(System.in);
System.out.println("Age: ");
int age = scanner.nextInt();
```
<!-- .element: class="fragment" -->

----
#### Manipulieren
<pre class="fragment"><code class="hljs Python" data-trim data-noescape>
🐍
len('String') -> 6
</code></pre>

```Java
"String".length() -> 6
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs Python" data-trim data-noescape>
🐍
'String'[1] -> <span class="fragment">'t'</span>
</code></pre>

```Java
"String".charAt(1) -> 't'
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs Python" data-trim data-noescape>
🐍
'String'[2:5] -> <span class="fragment">'rin'</span>
</code></pre>

```Java
"String".substring(2,5) -> "rin"
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs Python" data-trim data-noescape>
🐍
'Seperated by delimiter'.split(' ') 
    -> <span class="fragment">['Seperated', 'by', 'delimiter']</span>
</code></pre>

```Java
"Seperated#by#delimiter".split("#")
    -> ["Seperated", "by", "delimiter"]
```
<!-- .element: class="fragment" -->

note: noch viele mehr

---
#### `try` / `catch` / `throw`
```Python
🐍
age = input('Age: ')
try:
    age = int(age)
except ValueError as error:
    print('Illegal age: ' + age)
```
<!-- .element: class="fragment" -->

```Java
String input = scanner.nextLine();
int age = 0;
try {
    age = Integer.parseInt(input);
} catch (NumberFormatException e) {
    System.out.println("Illegal age: " + input);
}
```
<!-- .element: class="fragment" -->

```Python
🐍
raise ValueError('Error accessing ' + database)
```
<!-- .element: class="fragment" -->

```Java
throw new IllegalArgumentException('Error accessing ' + database)
```
<!-- .element: class="fragment" -->

---
#### Arrays
* Syntax ähnlich Python
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Definierte Länge
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Typisiert
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Hilfsmethoden <!-- .element: class="fragment fade-in-then-semi-out" --> unter <code>Arrays.</code> verfügbar


<pre class="fragment"><code class="hljs Python" data-trim data-noescape>
🐍
list = [42]
list.append(3)
print(list[1]) -> <span class="fragment">3</span>
</code></pre>

```Python
🐍
list = [42, 'nein', list]
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
int[] array = new int[3];
System.out.println(array) -> <span class="fragment">[I@6acbcfc0</span>
System.out.println(Arrays.toString(array)) -> <span class="fragment">[0, 0, 0]</span>
array[0] = 42;
</code></pre>

note: `toString` returnt `'@' + hashCode()`

----
#### Mehr Dimensionen
```Java
int[][] matrix = new int[3][2];
for (int row = 0; row < matrix.length; row++) {
    for (int col = 0; col < matrix[row].length; col++) {
        matrix[row][col] = 10 * row + col;
    }
}
```

<table border="1">
    <thead>
        <tr>
            <td></td>
            <th colspan="2" class="center">col</th>
        </tr>
        <tr>
            <th>row</th>
            <th class="right" style="border-bottom: 3px solid white">0</th>
            <th class="right" style="border-bottom: 3px solid white">1</th>
        </tr>
    </thead>
    <tbody>
        <tr class="fragment">
            <th class="right">0</th>
            <td class="right">0</td>
            <td class="right">1</td>
        </tr>
        <tr class="fragment">
            <th class="right">1</th>
            <td class="right">10</td>
            <td class="right">11</td>
        </tr>    
        <tr class="fragment">
            <th class="right">2</th>
            <td class="right">20</td>
            <td class="right">21</td>
        </tr>  
    </tbody>
</table>

----
#### for-in
```Python
🐍
for item in list:
    print(item)
```

```Java
for (int i = 0; i < array.length; i++) {
    int item = array[i];
    System.out.println(item);
}
```
<!-- .element: class="fragment" -->

```Java
for (int item : array) {
    System.out.println(item);
}
```
<!-- .element: class="fragment" -->

---
#### Methoden/Routinen/Funktionen
```Python
🐍
def foo(number: int) -> float:
    """Takes an integer, returns a float"""
    return 'Hello World'

foo('42')
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
/**
* Takes an integer, returns a double
*/
double foo(int number) {
    return "Hello World"; <span class="fragment">🚫</span>
}
</code></pre>

```Java
double foo(int number) {
    return 1.0;
}
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
foo("42"); <span class="fragment">🚫</span>
</code></pre>

```Java
void methodReturnsNothing(String s) {
    System.out.println(s);
}
```
<!-- .element: class="fragment" -->

---
#### Command Line Arguments
![ping](./assets/Java/ping.png)
<!-- .element: class="fragment" -->

Vorteile gegenüber GUIs:
<!-- .element: class="fragment" -->
* automatisierbar
<!-- .element: class="fragment fade-in-then-semi-out" -->
* weniger Aufwand
<!-- .element: class="fragment" -->

----
#### ☕
```Java
public class CLI {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(args));
    }
}
```
<!-- .element: class="fragment" -->

```Plain Text
java CLI These are command line arguments
```
<!-- .element: class="fragment" -->

```Plain Text
[These, are, command, line, arguments]
```
<!-- .element: class="fragment" -->

----
#### 🐍
```Python
import sys

if __name__ == '__main__':
    print(sys.argv)
```
<!-- .element: class="fragment" -->

```Plain Text
python CLI These are command line arguments
```
<!-- .element: class="fragment" -->

```Plain Text
['cli.py', 'These', 'are', 'command', 'line', 'arguments']
```
<!-- .element: class="fragment" -->