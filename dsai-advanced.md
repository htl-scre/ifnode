## DSAI Pt 2
---

#### Workflow
![workflow](./assets/dsai/workflow.jpg)
<!-- .element: class="r-stretch" -->

---
#### Performancemessung
* Schüler soll Mathe lernen
<!-- .element: class="fragment fade-in-then-semi-out" -->
* lernt das Buch auswendig
<!-- .element: class="fragment fade-in-then-semi-out" -->
* kann alles
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Schularbeitsbeispiel nicht im Buch -> 5
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Trainingsperformance kann lügen
<!-- .element: class="fragment fade-in" -->

----
#### Train-Test-Split
* Daten werden geteilt
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Training mit Trainingsset
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Testset simuliert unbekannte Daten
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Testperformance aussagekräftig
<!-- .element: class="fragment fade-in" -->

![train-test-split](./assets/dsai/training-testing.png)
<!-- .element: class="fragment fade-in" -->

---
### Underfitting/Overfitting
<!-- .element: class="fragment fade-in" -->
### Bias/Variance
<!-- .element: class="fragment fade-in" -->

![underfit-overfit](./assets/dsai/underfit-overfit.png)
<!-- .element: class="fragment fade-in" -->

----
![underfit-overfit](./assets/dsai/underfit-overfit-graph.png)

---
### SVM
![svm](./assets/dsai/svm_linear.webp)

----
### Kernel Trick
<iframe width="560" 
    height="315" 
    src="https://www.youtube.com/embed/OdlNM96sHio" 
    title="YouTube video player" 
    frameborder="0" 
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
</iframe>

----
### Gaussian RBF
![rbf](./assets/dsai/rbf.webp)

----
![rbf-params](./assets/dsai/rbf-params.webp)
<!-- .element: class="fragment fade-in" -->
* $$\gamma = \frac{1}{2\sigma^2}$$
<!-- .element: class="fragment fade-in" -->
* Einfluss einer observation
<!-- .element: class="fragment fade-in" -->


---
### `SVC(kernel='linear', C=100)`
<ul>
    <li class="fragment fade-in"><code>C</code> $\frac{1}{C}$ Faktor der $l_2$-Regularisierung</li>
    <li class="fragment fade-in"><code>C=0.01</code> ⟹ training-missclassifies ok</li>
    <li class="fragment fade-in"><code>C=1000</code> ⟹ training-missclassifies NEIN</li>
    <li class="fragment fade-in">bei <strong>allen</strong> sklearn-models</li>
</ul>

![linear-svm](./assets/dsai/fish_linear.png)
<!-- .element: class="fragment fade-in" -->



----
### `SVC(kernel='rbf', C=100, gamma='scale')`
![rbf-svm](./assets/dsai/fish_svm.png)

----
### `SVC(kernel='rbf', C=100, gamma='auto')`
$$\gamma = \frac{1}{n_{features}}$$
![overfitting](./assets/dsai/fish_svm_overfit.png)

----
### `SVC(kernel='rbf', C=500, gamma=1)`
![superfitting](./assets/dsai/fish_svm_superfit.png)

---
#### Underfitting
* Model kann sich nicht gut an Trainingsdaten anpassen
<!-- .element: class="fragment fade-in" -->

Lösung
<!-- .element: class="fragment fade-in" -->
* Komplexeres Model
<!-- .element: class="fragment fade-in" -->

---
#### Overfitting
* Model passt sich zu gut an Trainingsdaten an
<!-- .element: class="fragment fade-in" -->
* Model generalisiert schlecht
<!-- .element: class="fragment fade-in" -->

Lösung
<!-- .element: class="fragment fade-in" -->
* Komplexität reduzieren
<!-- .element: class="fragment fade-in" -->
* ANNs: Dropout-Layer
<!-- .element: class="fragment fade-in" -->
* Model regularisieren
<!-- .element: class="fragment fade-in" -->

---
### $l_1/l_2$ Regularization

Modell einschränken, damit Parameter $\theta$ klein bleiben
<!-- .element: class="fragment fade-in" -->
$$cost_{reg} = cost + \lambda\cdot reg$$
<!-- .element: class="fragment fade-in" -->

$$reg_{l1} = \sum|{\theta}|$$
<!-- .element: class="fragment fade-in" -->
$$reg_{l2} = \sum\theta^2$$
<!-- .element: class="fragment fade-in" -->