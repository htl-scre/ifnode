# Microservices

---
#### Monolith

![](./assets/microservices/monolith.svg)
<!-- .element: class="fragment fade-in" -->

---
#### Microservice

![](./assets/microservices/microservice.svg)
<!-- .element: class="fragment fade-in" -->

----
* Aufbau aus separaten Applikationen
<!-- .element: class="fragment fade-in" -->
  * Architektur
<!-- .element: class="fragment fade-in" -->
  * Techstack
<!-- .element: class="fragment fade-in" -->
  * Datenbank
<!-- .element: class="fragment fade-in" -->

![](./assets/microservices/microservice-dbs.svg)
<!-- .element: class="fragment fade-in" -->

---
#### Kommunikation - Monolith

![](./assets/microservices/checkout-monolith.svg)
<!-- .element: class="fragment fade-in" -->

----
#### Kommunikation - Microservice

![](./assets/microservices/checkout-microservice.svg)
<!-- .element: class="fragment fade-in" -->

Error beim Speichern? 
<!-- .element: class="fragment fade-in" -->

<div class="fragment fade-in">
  <p class="fragment strike">Rollback!</p>
</div>

----
#### Saga

![](./assets/microservices/checkout-saga.svg)
<!-- .element: class="fragment fade-in" -->

```python
try:
  for service in services:
    service()
  confirm_transaction()
except:
  compensate(services)
```
<!-- .element: class="fragment fade-in" -->

---
#### Gateway

![](./assets/microservices/gateway.webp)
<!-- .element: class="fragment fade-in" -->

```python
if req.path.like('/catalog/**'):
  return requests.get("http://23.235.12.56:4675/**")
```
<!-- .element: class="fragment fade-in" -->

----
#### Eureka
23.235.12.56:4675?
<!-- .element: class="fragment fade-in-then-semi-out" -->

![](./assets/microservices/eureka.webp)
<!-- .element: class="fragment fade-in" style="height:55vh"-->

```python
if req.path.like('/catalog/**'):
  return requests.get("lb://catalog-service/**")
```
<!-- .element: class="fragment fade-in" -->

---
#### Showdown

<table>
  <thead>
    <tr>
      <th>Area</th>
      <th>Monolith</th>
      <th>Microservices</th>
    </tr>
  </thead>
  <tbody>
    <tr class="fragment fade-in">
      <td>Development</td>
      <td align="center">👎</td>
      <td align="center">👍</td>
    </tr>
    <tr class="fragment fade-in">
      <td>SRP</td>
      <td align="center">👎</td>
      <td align="center">👍</td>
    </tr>
    <tr class="fragment fade-in">
      <td>Komplexität</td>
      <td align="center">👍</td>
      <td align="center">👎</td>
    </tr>
    <tr class="fragment fade-in">
      <td>Cross-Cutting</td>
      <td align="center">👍</td>
      <td align="center">🤔</td>
    </tr>
    <tr class="fragment fade-in">
      <td>Flexibilität</td>
      <td align="center">👎</td>
      <td align="center">👍</td>
    </tr>
    <tr class="fragment fade-in">
      <td>Testing</td>
      <td align="center">👍</td>
      <td align="center">🤔</td>
    </tr>
    <tr class="fragment fade-in">
      <td>Deployment</td>
      <td align="center">👍</td>
      <td align="center">🤔</td>
    </tr>
    <tr class="fragment fade-in">
      <td>Scaling</td>
      <td align="center">👎</td>
      <td align="center">👍</td>
    </tr>
  </tbody>
</table>

note: 
* Development einzelner Services bequemer
* Flexibilität in Techstack
* Scaling bei Monolith nur als Ganzes