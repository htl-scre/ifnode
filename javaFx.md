# JavaFX

---
#### JavaFX
<ul>
    <li class="fragment fade-in-then-semi-out">Java Platform zur Erstellung Graphischer Applikationen</li>
    <li class="fragment fade-in-then-semi-out"><em>nicht</em> in Java SE enthalten</li>
    <li class="fragment fade-in-then-semi-out">
    <p>Nachfolger von AWT und Swing<br>
    ⚠️viele Klassen in mehreren Frameworks <br>
    Beim Importieren aufpassen!</p>
    </li>
</ul>  
<pre class="fragment"><code class="hljs java" data-trim data-noescape>
import java.awt</span>.Button;
import javafx</span>.scene.control.Button;
</code></pre>

---
#### Maven
<ul>
    <li class="fragment fade-in-then-semi-out">Verwaltet Sourcecode</li>
    <li class="fragment fade-in-then-semi-out">Struktur <br>
        <img alt="maven Struktur" src="assets/FX/maven_structure.png">
    </li>
    <li class="fragment">ProjectObjectModel</li>
</ul>

----
```xml
<project xmlns="namespace"
         xmlns:xsi="schemaInstance"
         xsi:schemaLocation="schemaURL">
    <groupId>at.htlstp</groupId>
    <artifactId>MavenProject</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <javaFxVersion>15.0.1</javaFxVersion>
        <maven.compiler.release>15</maven.compiler.release>
    </properties>

    <dependencies>
        <!-- download -->
    </dependencies>
</project>
```
<!-- .element: class="stretch" -->

----
```xml
<dependencies>
    <dependency>
        <groupId>org.openjfx</groupId>
        <artifactId>javafx-base</artifactId>
        <version>15.0.1</version>
    </dependency>
    <dependency>
        <groupId>org.openjfx</groupId>
        <artifactId>javafx-controls</artifactId>
        <version>${javaFxVersion}</version>
    </dependency>
    <dependency>
        <groupId>org.openjfx</groupId>
        <artifactId>javafx-graphics</artifactId>
        <version>${javaFxVersion}</version>
    </dependency>
    <dependency>
        <groupId>org.openjfx</groupId>
        <artifactId>javafx-fxml</artifactId>
        <version>${javaFxVersion}</version>
    </dependency>
</dependencies>
```
<!-- .element: class="stretch fragment" -->

---
#### Resources
Files in resources werden in das kompilierte Programm aufgenommen
<!-- .element: class="fragment" -->

![resources-demo](./assets/FX/resources.png)
<!-- .element: class="fragment" -->

----
<dl>
    <dt class="fragment">resources</dt>
    <dd class="fragment fade-in-then-semi-out">Daten, die zum Compilezeitpunkt fix sind, z.B.:</dd>
    <dd><ul>
        <li class="fragment fade-in-then-semi-out">Graphiken</li>
        <li class="fragment fade-in-then-semi-out">Sounds</li>
        <li class="fragment fade-in-then-semi-out">Layoutinformation</li>
    </ul></dd>
    <dt class="fragment">nicht resources</dt>
    <dd class="fragment fade-in-then-semi-out">Daten, die der User bearbeiten möchte</dd>
    <dd class="fragment fade-in-then-semi-out">Daten, die erst zur Laufzeit benötigt werden</dd>
    <dd><ul>
        <li class="fragment fade-in-then-semi-out">Input</li>
        <li class="fragment">custom Data(Mods)</li>
    </ul></dd>
</dl>


---
#### White Screen
```Java
public class WhiteStage extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.show();
    }
}
```
<!-- .element: class="fragment" -->

```Java
public class Launcher {
    public static void main(String[] args) {
        Application.launch(WhiteStage.class, args);
    }
}
```
<!-- .element: class="fragment" -->

![white stage](assets/FX/white_stage.png)
<!-- .element: class="fragment" -->

---
#### Hierarchie
![javafx ](assets/FX/javafx-hierarchy.png)
<!-- .element: class="fragment" -->

---
#### FXML
```Java
public class FxmlApplication extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL resource = getClass().getResource("/layout/gui.fxml");
        Parent root = FXMLLoader.load(resource);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
```
<!-- .element: class="fragment fade-in-then-semi-out stretch" -->
```xml
<AnchorPane fx:controller="fxml.Controller" ... />
```
<!-- .element: class="fragment fade-in-then-semi-out stretch" -->
```Java
public class Controller implements Initializable {

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // GUI ready
    }
}
```
<!-- .element: class="fragment stretch" -->

---
#### Event
* Ausgelöst durch Interaktion mit GUI
<!-- .element: class="fragment fade-in-then-semi-out" -->
* durch <!-- .element: class="fragment fade-in-then-semi-out" -->`element.setOnXXX` wird ein Handler definiert

```Java
button.setOnAction(new EventHandler<ActionEvent>() {
    @Override
    public void handle(ActionEvent event) {
        System.out.println(event.getSource() + "clicked");
    }
});
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
textField.setOnAction(ev -> label.setText(textField.getText()));
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
checkBox.setOnAction(ev -> handleCheckBoxAction());
```
<!-- .element: class="fragment" -->

---
#### Alert
```Java
Alert alert = new Alert(Alert.AlertType.ERROR);
alert.setTitle("Error");
alert.setHeaderText("Huge mistake");
alert.setContentText("You have no idea what's going on");
alert.showAndWait();
```
<!-- .element: class="fragment" -->

![error](assets/FX/error.png)
note: https://code.makery.ch/blog/javafx-dialogs-official/
        
---
#### Observable
<ul>
    <li>Elemente haben <em>Properties</em>, deren Änderung in einem <em>Listener</em> eine Aktion auslösen</li>
    <li class="fragment fade-in-then-semi-out"><code>InvalidationListener</code>
        <pre ><code class="hljs java" data-trim data-noescape>
tfSlider.textProperty()
        .addListener(observable -> 
                txtSlider.setText(tfSlider.getText()));
        </code></pre>
    </li>
    <li class="fragment fade-in-then-semi-out"><code>ChangeListener</code>
        <pre><code class="hljs java" data-trim data-noescape>
tfSlider.textProperty()
        .addListener((observable, oldValue, newValue) -> 
                txtSlider.setText(newValue));    
        </code></pre>
    </li>
    <li class="fragment"><em>Binding</em>
        <pre><code class="hljs Java" data-trim data-noescape>
txtSlider.textProperty().bind(tfSlider.textProperty());
        </code></pre>
    </li>
</ul>

----
#### Binding
<dl>
    <dt><code>subscriber.bind(publisher)</code></dt>
    <dd>
        <ul>
            <li class="fragment fade-in-then-semi-out"><code>publisher = x </code> <span class="monospace">  -></span> <code>subscriber = x</code></li>
            <li class="fragment fade-in-then-semi-out"><code>subscriber = x</code> <span class="monospace">-></span> <code>Exception</code> 💥</li>
        </ul>
    </dd>
</dl>

```Java
txtSlider.textProperty().bind(tfSlider.textProperty());
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
button.disableProperty().bind(cbDisabled.selectedProperty());
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
slider.valueProperty().bind(textField.textProperty()); 🚫
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
txtFahrenheit.textProperty().bind(
        sliderCelsius.valueProperty()    // DoubleProperty
                .multiply(9.0 / 5).add(32).asString()
);
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```
a.bindBidirectional(b);
a.bindBidirectional(c);
```
<!-- .element: class="fragment" -->
note: was passiert beim Letzten?

----
#### Converter
```Java
tfSlider.textProperty().bindBidirectional(
        slider.valueProperty(), new StringConverter<Number>() {
    @Override
    public String toString(Number number) {
        return String.format("%.0f", number.doubleValue());
    }

    @Override
    public Number fromString(String string) {
        try {
            return Integer.parseInt(string);
        } catch (NumberFormatException ex) {
            alert.showAndWait();
            return 0;
        }
    }
});
```
<!-- .element: class="fragment stretch" -->
<!-- .element: class="fragment stretch" -->

----
#### Multiple Dependencies
Problem: `x` updaten, wenn sich `a`, `b` oder `c` updaten
<!-- .element: class="fragment" -->
```Java
a.addListener(value -> updateX());
b.addListener(value -> updateX());
c.addListener(value -> updateX());
...

private void updateX() {    
    x.setValue(calcX());    // greift auf a,b,c zu
}
```
<!-- .element: class="fragment" -->

```Java
StringBinding binding = Bindings.createStringBinding(
        () -> calcX(),      // X getX
        a, b, c);
x.bind(binding);
```
<!-- .element: class="fragment" -->

---
#### ListView
<ul>
    <li class="fragment fade-in-then-semi-out"><code>.getItems()</code></li>
    <li class="fragment fade-in-then-semi-out"><code>.getSelectionModel()</code></li>
</ul>

```Java
listView.getItems().add(toAdd);
listView.getSelectionModel().selectFirst();
```
<!-- .element: class="fragment" -->

----
#### Custom ListView
<code>ListView&ltX&gt</code>ruft <code>item.toString()</code> auf
<!-- .element: class="fragment" -->
```Java
lvX.setCellFactory(lv -> new XListCell());
```
<!-- .element: class="fragment" -->
```Java
public class XListCell extends ListCell<X> {
    @Override
    protected void updateItem(X item, boolean empty) {
        super.updateItem(item, empty);
        if (empty || item == null) {
            setGraphic(null);
            setText(null);
        } else
            setText(item.toString());
    }
}
```
<!-- .element: class="fragment" -->

----
```Java
public class CustomListCell extends ListCell<CustomClass> {
    @FXML
    private Label label;

    private FXMLLoader loader;

    @Override
    protected void updateItem(CustomClass item, boolean empty) {
        super.updateItem(item, empty);
        setText(null);
        if (empty || item == null)
            setGraphic(null);
        else {
            boolean fxmlLoaded = loader != null;
            if (!fxmlLoaded) {
                loader = new FXMLLoader(getClass().getResource(FXML));
                loader.setController(this);
                loader.load();
            }
            setupGui(item);     // label.setText(item.name())...
            setGraphic(loader.getRoot());
        }
    }
```
<!-- .element: class="stretch" -->