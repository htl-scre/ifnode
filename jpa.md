# JPA
### Java Persistence API

---
#### Probleme mit JDBC
* Connectionverwaltung
<!-- .element: class="fragment fade-in-then-semi-out" -->
* CRUD-Operationen boilerplate
<!-- .element: class="fragment fade-in-then-semi-out" -->
* SQL Db-spezifisch
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Typsicherheit
<!-- .element: class="fragment" -->

----
#### Architektur
![or-mapping](assets/JPA/data_access_layers.svg)
note: Bridge zwischen OOP und Relationalen Datenbanken.  

---
#### Persistence Unit
<span class="monospace">resources/META-INF/persistence.xml</span>
```XML
<persistence-unit name="at.htlstp.jpa.library">
    <class>domain.Entity</class>
    <properties>
        <property name="jakarta.persistence.jdbc.driver" 
            value="org.h2.Driver"/>
        <property name="jakarta.persistence.jdbc.url" 
            value="jdbc:h2:mem:bibliothek"/>
        <property name="hibernate.dialect" 
            value="org.hibernate.dialect.H2Dialect"/>

        <property name="hibernate.show_sql" value="true"/>
        <property name="hibernate.format_sql" value="true"/>
        <property name="hibernate.hbm2ddl.auto" value="update"/>
    </properties>
</persistence-unit>
```
<!-- .element: class="stretch" -->

----
#### `hibernate.hbm2ddl.auto`
<dl>
<div class="fragment fade-in-then-semi-out">
    <dt class="monospace">none</dt>
    <dd>default</dd>
</div>
<div class="fragment fade-in-then-semi-out">
    <dt class="monospace">create-only</dt>
    <dd>Erzeugt <code>create</code>-Statements</dd>
</div>
<div class="fragment fade-in-then-semi-out">
    <dt class="monospace">drop</dt>
</div><div class="fragment fade-in-then-semi-out">
    <dt class="monospace">create</dt>
    <dd><code>drop</code>, dann <code>create-only</code></dd>
</div></div><div class="fragment fade-in-then-semi-out">
    <dt class="monospace">create-drop</dt>
    <dd><code>drop</code>, dann <code>create-only</code>, finally <code>drop</code></dd>
</div></div></div><div class="fragment fade-in-then-semi-out">
    <dt class="monospace">validate</dt>
</div></div></div></div><div class="fragment">
    <dt class="monospace">update</dt>
</div>
</dl>
note: Vorsicht bei Zugriff auf bestehende Db

---
#### Entities
```Java
@Entity
@Table(name = "defaulting_to_classname")
public class Customer implements Serializable {
    @Id
    @GeneratedValue
    private Integer id;

    @NotNull
    private String name;

    @NaturalId
    private String email;
}
```
```SQL
create table defaulting_to_classname (
       id integer not null,
        email varchar(255),
        name varchar(255) not null,
        primary key (id))
alter table defaulting_to_classname 
       add constraint UK_ruvgttp unique (email)
```
<!-- .element: class="fragment" -->
note: Wo wird der Value generated?

----
#### Generation Strategies
```Java
@Id
private Long assignedByJava;
```
<!-- .element: class="fragment fade-in-then-semi-out" -->
```Java
@Id
@GeneratedValue //(strategy = GenerationType.AUTO)
private Integer databaseDefaultStrategy;
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long assignedByTableColumn;
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
@Id
@GeneratedValue(
    strategy = GenerationType.SEQUENCE,
	generator = "customer_generator")
@SequenceGenerator(
	name = "customer_generator",
	sequenceName = "customer_sequence")
private Short assignedByOwnSequence;
```
<!-- .element: class="fragment" -->

note: `UUID`

----
#### Annotations
```Java
@Column(
    name = "columnName",
    unique = true,
    length = 10)
private String javaName;
```
<!-- .element: class="fragment fade-in-then-semi-out" -->
```SQL
create table Customer (
       id integer not null,
        columnName varchar(10),
        primary key (id))
alter table Customer 
       add constraint UK_5ru7su37v unique (columnName)
```
<!-- .element: class="fragment fade-in-then-semi-out" -->
```Java
@ElementCollection
private Collection<String> aliases;
```
<!-- .element: class="fragment fade-in-then-semi-out" -->
```SQL
create table Customer_aliases (
       Customer_id integer not null,
        aliases varchar(255))
```
<!-- .element: class="fragment" -->

----
```
@Entity
public class Customer {
    @Id
    private Short id;

    @Transient
    private String notForDb;
}
```
<!-- .element: class="fragment fade-in-then-semi-out" -->
```SQL
create table Customer (
       id smallint not null,
        primary key (id))
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
@Entity
@Check(constraints = """
        CASE 
             WHEN name IS NOT NULL THEN LENGTH(name) > 3 
             ELSE true 
        END""")
public class Customer {
```
<!-- .element: class="fragment" -->

----
#### `jakarta.validation`
```Java
@NotNull
@Size(max = 100)
@Email
private String email;
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
@PositiveOrZero
private int age;
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
@ElementCollection
@Size(min = 1, max = 5)
private Collection<String> aliases;
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
@Past
private LocalDate dateOfBirth;
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
@Pattern("^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$")
private String ip;
```
<!-- .element: class="fragment fade-in-then-semi-out" -->


```Java
@AssertTrue
public boolean isBeginningBeforeEnd() {
    return beginning.isBefore(end);
}
```
<!-- .element: class="fragment" -->



----
#### Dependencies
```xml
<dependencies>
  <dependency>
    <groupId>org.hibernate.validator</groupId>
    <artifactId>hibernate-validator</artifactId>
    <version>7.0.4.Final</version>
  </dependency>
  <dependency>
    <groupId>org.glassfish</groupId>
    <artifactId>jakarta.el</artifactId>
    <version>4.0.1</version>
  </dependency>
</dependencies>
```
note: ExpressionLanguage

---
#### `EntityManager`
```Java
var factory = Persistence
        .createEntityManagerFactory("at.htlstp.jpa.library");
var entityManager = factory.createEntityManager();
```
<!-- .element: class="fragment" -->

* Java Object
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Verwaltet <!-- .element: class="fragment fade-in-then-semi-out" -->Entitäten in einem *Persistence Context*
* Am <!-- .element: class="fragment fade-in-then-semi-out" -->besten in try-with
* Schreibende <!-- .element: class="fragment" -->Db-Operationen **nur** in Transaktion

----
#### Verwendung
```Java
var factory = Persistence
        .createEntityManagerFactory("at.htlstp.jpa.library");
try(var entityManager = factory.createEntityManager()) {
    var transaction = entityManager.getTransaction();
    transaction.begin();
    entityManager.persist(customer);
    transaction.commit();
} catch (RuntimeException ex) {
    if (transaction.isActive())
        transaction.rollback();
    throw ex;
}
```
<!-- .element: class="stretch" -->

----
#### States
<img alt="JpaEntitiyStates" src="assets/JPA/jpa_entity_states.png" height="250px">
<dl>
    <div class="fragment fade-in-then-semi-out">
              <dt><em>transient</em></dt>
              <dd>Objekt war nie <em>managed</em></dd>
    </div>
    <div class="fragment fade-in-then-semi-out">
              <dt><em>managed</em>/<em>persistent</em></dt>
              <dd><code>entityManager.flush()</code> <span class="monospace">-></span> DbUpdate</dd>
    </div>
    <div class="fragment fade-in-then-semi-out">
              <dt><em>detached</em></dt>
              <dd>Objekt war <em>managed</em></dd>
    </div>
</dl>

----
#### Methoden
<dl>
    <dt class="fragment fade-in-then-semi-out"><code>persist(entity)</code></dt>
    <dt class="fragment fade-in-then-semi-out"><code>find(entityClass, pk)</code></dt>
    <dt class="fragment fade-in-then-semi-out"><code>remove(entity)</code></dt>
    <div class="fragment">
        <dt><code>merge(entity)</code></dt>
        <dd>
            <span class="fragment">lädt entity in den Context</span>
            <ol>
                <li class="fragment">updated entity im Context mit übergebener</li>
                <li class="fragment"><code>find(entityClass, entity.pk)</code></li>
                <li class="fragment"><code>persist(entity)</code></li>
            </ol>
            <span class="fragment">returnt die Entity aus dem Context</span>
        </dd>
    </div>
</dl>
note: `refresh` synchronisiert *managed* Zustand mit Db

----
#### `equals`/`hashCode`
<ul>
    <li class="fragment fade-in-then-semi-out"><code class="yellow">@NaturalId</code> ✔️</li>
    <li class="fragment fade-in-then-semi-out"><code class="yellow">@Id</code> <em>transient</em> ⚠️
        <pre><code class="hljs Java" data-trim data-noescape>
var hashCode = entity.hashCode();
entityManager.persist(entity);         // id generiert
var newHashCode = entity.hashCode();
        </code></pre>
    </li>
    <li class="fragment">nicht überschreiben <em>detached</em> ⚠️</li>
    <li class="fragment"><code class="yellow">@Id</code> nicht verwenden ⚡ zu Db</li>
</ul>

---
## Beziehungen

----
#### Generell
* Unidirektional
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Bidirektional
<!-- .element: class="fragment" -->
  - eine<!-- .element: class="fragment fade-in-then-semi-out" --> Seite ist *Owner*
  <li class="fragment fade-in-then-semi-out"><em>nur</em> der Owner darf in Db schreiben</li>
  - bei<!-- .element: class="fragment" --> nicht-owning Seite Attribut `mappedBy`

----
#### Lazy Loading
```Java
@Basic(fetch = FetchType.EAGER)
private String alwaysLoaded
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
private String defaultEager;
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
@Basic(fetch = FetchType.LAZY)
private String loadedIfNeeded;
```
<!-- .element: class="fragment" -->
<ul>
    <li class="fragment fade-in-then-semi-out">Performance</li>
    <li class="fragment fade-in-then-semi-out">lazy detached <span class="monospace">-></span> <code>Exception</code> 💥</li>
    <li class="fragment">Fix: <code class="sql">select ... join fetch lazyField</code></li>
</ul>

---
<h4><code class="yellow">@OneToOne</code></h4>

Each customer has one address
<!-- .element: class="fragment" -->

![1 to 1 relationship example](assets/JPA/1to1_customer_address.png)
<!-- .element: class="fragment" height:"100px" -->

<div class="fragment">
<pre><code class="hljs Java" data-trim data-noescape>
@Entity
public class Customer {
   @OneToOne(fetch = FetchType.EAGER)                 // default
   @JoinColumn(unique = true, name = "address_id")    // default
   private Address address;
</code></pre>
<pre><code class="hljs Java" data-trim data-noescape>
@Entity
public class Address {
   @OneToOne(mappedBy = "address")   // der Variablenname im Owner
   private Customer resident;        // entfernen -> unidirektional
</code></pre>
</div>
----
#### Anwendung
Besser über `@Embeddable`, außer
<!-- .element: class="left fragment fade-in-then-semi-out" -->
* Table erwünscht
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Entity auch an anderer Stelle verwendet
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Daten aus unterschiedlichen Dbs
<!-- .element: class="fragment fade-in-then-semi-out" -->

----
<h4><code class="yellow">@OneToMany</code>/<code class="yellow">@ManyToOne</code></h4>

* Each publisher publishes many books.
* Each book has one publisher  

![n to 1 relationship example](assets/JPA/nto1_book_publisher.png)
<!-- .element: class="fragment" -->

----
```Java
@Entity
public class Book {
    @ManyToOne(fetch = FetchType.EAGER)   // default
    @JoinColumn(unique = false)           // default
    private Publisher publisher;
```
```Java
@Entity
public class Publisher {
    @OneToMany(
        mappedBy = "publisher",
        fetch = FetchType.LAZY)           //default
    private Collection<Book> books = new HashSet<>();
```

----
<h4><code class="yellow">@ManyToMany</code></h4>

* Each movie has many actors in it
* Each actor stars in many movies

![n to 1 relationship example](assets/JPA/nton_movie_actor.png)
<!-- .element: class="fragment" -->
```Java
@Entity
public class Movie {
    @ManyToMany
    @JoinTable(name = "movie_actor",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "actor_id"))
```
<!-- .element: class="fragment" -->

----
```Java
@Entity
public class Movie {
    @ManyToMany(fetch = FetchType.LAZY)     //default
    private Collection<Actor> actors = new HashSet<>();

    public Collection<Actor> getActors() {
        return actors;
    }
```
<!-- .element: class="stretch" -->
```Java
@Entity
public class Actor {
    @ManyToMany(
            mappedBy = "actors",
            fetch = FetchType.LAZY)         //default
    private Collection<Movie> movies = new HashSet<>();

    public Stream<Movie> getMovies() {
        return movies.stream();
    }

    public void starInMovie(Movie movie) {
        movie.getActors().add(this);
        movies.add(movie);
    }
```
<!-- .element: class="stretch" -->

---
#### Owning
```Java
public class Movie {
    @ManyToMany
    private Collection<Actor> actors = new HashSet<>();
    ...
}

public class Actor {
    @ManyToMany(mappedBy = "actors")
    private Collection<Movie> movies = new HashSet<>();
    ...
}
```
<!-- .element: class="fragment fade-in" -->


```Java
entityManager.merge(movie);
entityManager.remove(movie); 
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

Movie gelöscht, Actor existiert
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
entityManager.merge(actor);
entityManager.remove(actor); 
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Plain Text
💥 Referential integrity constraint violation
```
<!-- .element: class="fragment exception fade-in" --> 

----
<h4><code class="yellow">@PreRemove</code></h4>

```Java
// Movie still owning

public class Actor {
    @ManyToMany(mappedBy = "actors")
    private Collection<Movie> movies = new HashSet<>();
    
    @PreRemove
    void removeFromAllMovies() {
        movies.forEach(movie -> movie.getActors().remove(this));
    }
    ...
}
```
<!-- .element: class="fragment stretch fade-in" -->

```Java
entityManager.merge(actor);
entityManager.remove(actor); 
```
<!-- .element: class="fragment stretch fade-in-then-semi-out" -->

Actor gelöscht, kommt in keinem Movie mehr vor
<!-- .element: class="fragment fade-in" -->

---
#### JoinTable mit Extras
![n to 1 relationship example](assets/JPA/jointable-with-extras.png)
<!-- .element: class="fragment" -->

Nur mapbar über eigene Entity
<!-- .element: class="fragment fade-in-then-semi-out" -->

```java
@Entity
public class Character {

  @ManyToOne
  private Actor actor;
  
  @ManyToOne
  private Movie movie;
  
  @Column(name = "character")
  private String name;
```
<!-- .element: class="fragment" -->

---
#### Cascade
```Java
@Entity
public class A {
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private B b;
```
<dl>
    <dt class="fragment fade-in-then-semi-out">Attribut bei <code class="yellow">@XtoY</code></dt>
    <dd></dd>
    <div class="fragment fade-in-then-semi-out">
        <dt><code>CascadeType.</code></dt>
        <dd><code>{PERSIST</code>, <code>MERGE</code>, <code>REMOVE</code>, <code>ALL}</code></dd>
    </div>
    <div class="fragment fade-in-then-semi-out">
        <dt><code>CascadeType.OP</code></dt>
        <dd><code>em.op(a);</code> <span class="monospace">-></span> <code>em.op(b);</code></dd>
    </div>
    <div class="fragment">
        <dt><code>orphanRemoval</code></dt>
        <dd><code>a.setB(otherB);</code><span class="monospace">-></span> <code>em.remove(b);</code></dd>
    </div>
</dl>
note: Weiters DETACH, REFRESH

---
#### Deletion
```Java
@Entity
public class Order { ... }
    
entityManager.remove(order); // Order deleted
```
<!-- .element: class="fragment fade-in" -->

```Java
@Entity
@SoftDelete
public class Order { }
    
entityManager.remove(orderWithId42);
```
<!-- .element: class="fragment fade-in" -->
| id | customer | deleted |
|:---|:---------|:--------|
| 41 | 575      | false   |
| 42 | 1243     | true    |
| 43 | 1243     | false   |
<!-- .element: class="fragment fade-in" -->

---
## JPQL

---
#### Eigenschaften
* SQL-ähnliche objektorientierte Abfragesprache
  <!-- .element: class="fragment fade-in-then-semi-out" -->
* Bezeichner aus Java, keine Tables
  <!-- .element: class="fragment fade-in-then-semi-out" -->
* Anwendung ausschließlich auf Entities
  <!-- .element: class="fragment" -->

---
#### Ausführung
```Java
var query = entityManager.createQuery("""
    select address from Address address""", 
    Address.class); // sonst Object
List<Address> addresses = query.getResultList();
```
<!-- .element: class="fragment fade-in-then-semi-out" -->
```Java 
query = entityManager.createQuery("""
    select address 
    from Address address 
    where address.id = 1""", Address.class);
Address address = query.getSingleResult();
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

<pre class="fragment"><code class="hljs Java" data-trim data-noescape="">
List<<span class="fragment highlight-current-blue">Object</span>> addresses = entityManager.createQuery("""
        select address 
        from Address address 
        where address.city = :city""")
    .setParameter("city", "St. Pölten")
    .getResultList();
</code></pre>
note: normale Queries returnen nur `Object`

---
#### Select
<pre><code class="hljs Java" data-trim data-noescape>
List<<span class="blue">Object[]</span>> result = entityManager.createQuery("""
        select address.city, address.street, address.zip 
        from Address address""")
        .getResultList();
result.stream()
        .map(Arrays::toString)
        .forEach(System.out::println);
</code></pre>

```Java 
[St. Pölten, Waldstraße 7, 3100]
[Wien, Antonigasse 13, 1180]
[Wien, Michaelerkuppel 10, 1010]
[Wien, Wienerbergerstraße 1, 1120]
[St. Pölten, Schießstattring 6, 3100]
[Wilhelmsburg, Obere Hauptstraße 42, 3150]
[St. Pölten, Rathausplatz 2, 3100]
[St. Pölten, Rathausplatz 3, 3100]
```
<!-- .element: class="fragment"-->

---
#### `where`
<div class="fragment">
    <pre><code class="hljs plaintext" data-trim data-noescape>
select book.id, book.title 
from Book book 
where book.title = <span class="blue">:title</span>
    </code></pre>
</div>

<div class="fragment">
    <pre><code class="hljs plaintext" data-trim data-noescape>
≈ select <span class="blue">new Book(book.id, book.title)</span> 
from Book book 
where book.title = :title
    </code></pre>
</div>

<div class="fragment">
    <pre><code class="hljs plaintext" data-trim data-noescape>
select <span class="blue">distinct</span> customer.lastname 
from Customer customer
    </code></pre>
</div>

<div class="fragment">
    <pre><code class="hljs plaintext" data-trim data-noescape>
select book 
from Book book 
where book.publisher.name <span class="blue">like '% HALL'</span>
    </code></pre>
</div>

<div class="fragment">
    <pre><code class="hljs plaintext" data-trim data-noescape>
select book 
from Book book 
<span class="blue">order by</span> book.author, book.title <span class="blue">desc</span>
    </code></pre>
</div>

----
<div class="fragment">
    <pre><code class="hljs plaintext" data-trim data-noescape>
select publisher.name, book 
from Publisher publisher, Book book 
where <span class="blue">publisher = book.publisher</span>
    </code></pre>
</div>

<div class="fragment">
    <pre><code class="hljs plaintext" data-trim data-noescape>
= select publisher.name, book from Publisher publisher 
<span class="blue">join</span> publisher.books book
    </code></pre>
</div>

<div class="fragment">
    <pre><code class="hljs plaintext" data-trim data-noescape>
select publisher
from Publisher publisher
<span class="blue">join</span> publisher.books book
where book.title like :title
    </code></pre>
</div>

<div class="fragment">
    <pre><code class="hljs plaintext" data-trim data-noescape>
select book 
from Book book 
where book.author <span class="blue">is null</span>
<span class="blue">or</span> book.id <span class="blue"><=</span> 1 <span class="blue">and</span> book.publisher.name <span class="blue">not</span> like '% HALL'
    </code></pre>
</div>

----
#### Collections
<div class="fragment">
    <pre><code class="hljs Java" data-trim data-noescape>
<span class="blue">Collection&lt;Book></span> books = em
        .createQuery("""
            select publisher.books 
            from Publisher publisher""")
        .getResultList();
    </code></pre>
</div>

<div class="fragment">
    <pre><code class="hljs plaintext" data-trim data-noescape>
select book 
from Book book 
where book.isbn <span class="blue">in ('404', '418')</span>
    </code></pre>
</div>

<div class="fragment stretch">
    <pre><code class="hljs plaintext" data-trim data-noescape>
select book 
from Book book 
where book.isbn <span class="blue">in :javaCollection</span>
    </code></pre>
</div>

<div class="fragment stretch">
    <pre><code class="hljs plaintext" data-trim data-noescape>
select book 
from Book book 
where book.id <span class="blue">between 1 and 3</span>
    </code></pre>
</div>

<div class="fragment stretch">
    <pre><code class="hljs plaintext" data-trim data-noescape>
select publisher 
from Publisher publisher 
where publisher.books <span class="blue">is empty</span>
    </code></pre>
</div>

----
<div class="fragment stretch">
    <pre><code class="hljs plaintext" data-trim data-noescape>
select publisher 
from Publisher publisher 
where <span class="blue">size(publisher.addresses)</span> >= 3
    </code></pre>
</div>

<div class="fragment stretch">
    <pre><code class="hljs plaintext" data-trim data-noescape>
select address 
from Address address, Publisher publisher 
where address.city = 'St. Pölten'
    </code></pre>
    <p><strong>alle</strong> Adressen in StP für <strong>alle</strong> Publisher</p>
</div>

<div class="fragment stretch">
    <pre><code class="hljs plaintext" data-trim data-noescape>
select address 
from Address address, Publisher publisher 
where address.city = 'St. Pölten' 
and address <span class="blue">member of</span> publisher.addresses
    </code></pre>
</div>

----
#### groupBy
<div class="fragment">
    <pre><code class="hljs plaintext" data-trim data-noescape>
List&lt;Object[]> result = entityManager.createQuery("""
            select book.publisher.name, count(book) 
            from Book book
            <span class="blue">group by</span> book.publisher.name 
            <span class="blue">having</span> count(book) > 0
            order by book.publisher.name""")
        .getResultList();
<span class="fragment">Map&lt;String, Long> map = result.stream()
        .collect(toMap(
                record -> (String) record[0],
                record -> (Long) record[1],
                Long::sum,  <span class="grey">// mergeFunction</span>
                <span class="blue">LinkedHashMap</span>::new));</span>
    </code></pre>
</div>

---
#### Aggregatfunktionen
<div class="fragment">
    <pre><code class="hljs plaintext" data-trim data-noescape>
select <span class="blue">count</span>(*) 
from Book    -> <span class="blue">long</span>
    </code></pre>
</div>

<div class="fragment">
    <pre><code class="hljs plaintext" data-trim data-noescape>
select oldest 
from Book oldest 
where oldest.publicationDate = 
    (select <span class="blue">min</span>(b.publicationDate) 
    from Book b)
    </code></pre>
</div>
<ul>
    <li class="fragment"><code>sum()</code> <span class="monospace">-></span> <code>long</code>/<code>double</code></li>
    <li class="fragment"><code>avg()</code> <span class="monospace">-></span> <code>double</code></li>
</ul>

---
#### Funktionen
<ul>
    <li class="fragment"><code>upper</code>/<code>lower(String s)</code></li>
    <li class="fragment"><code>current_date</code>/<code>time</code>/<code>timestamp()</code></li>
    <li class="fragment"><code>substring(String s, int offset, int length)</code></li>
    <li class="fragment"><code>trim(String s)</code></li>
    <li class="fragment"><code>length(String s)</code></li>
    <li class="fragment"><code>abs(Numeric n)</code></li>
    <li class="fragment"><code>sqrt(Numeric n)</code></li>
    <li class="fragment"><code>mod(Numeric a, Numeric b)</code></li>
</ul>

---
#### Embeddables
```Java
@Entity
public class Customer {
    @EmbeddedId
    private Uid uid;
    
    @Embedded
    private Address address;
}
```

```Java
@Embeddable
public class Uid implements Serializable {
    private String email;
}
```

```SQL
create table Customer (
       email varchar(255) not null,
        street varchar(255),
        zip integer,
        primary key (email))
```

concatenated keys

note: Wie kommt das SQL zustande?
`String`, `Integer` etc. kein Table. Address` ist ebenso `@Embeddable`, siehe SQL


---
#### Vererbung
![uml](assets/JPA/jpa_board_uml.png)
* *ein* Table mit einer Laufzeittyp-Column
* ein Table je Klasse, wobei ein fk zur Basisklasse besteht, wo die vererbten Daten sind

----
```Java
@Entity
@Inheritance
public class Topic {
    @Id
    @GeneratedValue
    private Long id;
```
```Java
@Entity
public class Post extends Topic {
    private String content;
}
```
```Java
@Entity
public class Announcement extends Topic {
    @NotNull
    private LocalDateTime validUntil;
}
 ```
----
#### Table Topic(Auszug)
| DTYPE | OWNER |  VALIDUNTIL | CONTENT |
| :--- | :--- | :--- | :--- |
| Post | owner | NULL | content |
| Announcement |  owner | 2020-04-21 20:39:37.292626 | NULL |
| Post | SCRE |  NULL | ok |
| Post | SCRE |  NULL | thx |
| Announcement |  CNN | 2020-04-21 20:39:37.292626 | NULL |

note: NULL jeweils bei Field der anderen Klassen; Constraints per Trigger/Script
