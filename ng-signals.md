# Angular Signals

---
#### Motivation
<ul>
  <li class="fragment fade-in-then-semi-out">Change Detection muss oft laufen, wenig zielgerichtet</li>
  <li class="fragment fade-in-then-semi-out">aktuell mit <code>zone.js</code></li>
  <li class="fragment">Event-Management erfordert Einarbeitung in <code>rxjs</code></li>
</ul>

---
#### `WriteableSignal`

<ul>
  <li class="fragment fade-in-then-semi-out"><code>s = signal(42)</code> erzeugt ein Signal mit Startwert 42</li>
  <li class="fragment fade-in-then-semi-out"><code>s()</code> liefert den aktuellen Wert</li>
  <li class="fragment fade-in-then-semi-out"><code>s.set(13)</code> setzt 13 als Wert</li>
  <li class="fragment"><code>s.update(value => value + 1)</code> ändert den Wert</li>
</ul>

----
```typescript
@Component(
  template: `
    <h1>Current value of the counter: {{count}}</h1>
    <button (click)="increment()">Increment</button>
`)
export class AppComponent {
  count: number = 0;
  increment() {
    this.count++;
  }
}
```
<!-- .element: class="fragment" -->

```typescript
@Component(
  template: `
    <h1>Current value of the counter: {{count()}}</h1>
    <button (click)="increment()">Increment</button>
`)
export class AppComponent {
  const count: WritableSignal<number> = signal(0);
  increment() {
    this.count.set(count() + 1);
  }
}
```
<!-- .element: class="fragment" -->

---
#### Computed Signals

```typescript
const doubleCount: Signal<number> = computed(() => count() * 2);
```
<!-- .element: class="fragment stretch" -->
<ul>
  <li class="fragment fade-in-then-semi-out stretch">entstehen aus existierenden Signals</li>
  <li class="fragment fade-in-then-semi-out stretch">read-only</li>
</ul>

```typescript
doubleCount.set(42); 🚫
``` 
<!-- .element: class="fragment stretch" -->

```typescript
computed(() => a() + b());
``` 
<!-- .element: class="fragment stretch" -->

<ul>
  <li class="fragment fade-in-then-semi-out">updated, wenn <code>a</code> oder <code>b</code> updaten</li>
  <li class="fragment fade-in-then-semi-out">lazy evaluated</li>
  <li class="fragment">gecached</li>
</ul>

----
```typescript
const showCount = signal(false);
const count = signal(0);
const conditionalCount = computed(() => {
  if (showCount()) {
    return `The count is ${count()}.`;
  } else {
    return 'Nothing to see here!';
  }
});
```
<!-- .element: class="fragment stretch" -->

<ul>
  <li class="fragment fade-in-then-semi-out">nur evaluierte Signals werden getracked</li>
  <li class="fragment fade-in-then-semi-out">showCount = false ⟹ count nicht getracked</li>
  <li class="fragment">ändert sich bei jeder Auswertung</li>
</ul>

---
#### `@Input` ⟹ `input()`
```angular181html
<book 
  [book]="{'title': 'RxJS vs Angular Signals'}"
  [price]=42
/>
```
<!-- .element: class="fragment stretch" -->

```typescript
@Component({...})
class BookComponent {
    @Input() book!: Book;
    @Input() copies = 0;
    @Input({required: true}) price: numer;
}
```
<!-- .element: class="fragment stretch" -->

```typescript
@Component({...})
class BookComponent {
  book = input<Book>(); // read-only
  copies = input<number>(0); // default value
  price = input.required<number>(); // required input   
}
```
<!-- .element: class="fragment stretch" -->

----
#### `@Output` ⟹ `output()`
```angular181html
<book (deleteBook)="deleteBookEvent($event)" />
```
<!-- .element: class="fragment stretch" -->

```typescript
@Component({...})
class BookComponent {
  @Output() deleteBook = new EventEmitter<Book>();
  
  onDeleteButtonPressed() {
    this.deleteBook.emit(this.book);
  }
}
```
<!-- .element: class="fragment stretch" -->

```typescript
@Component({...})
class BookComponent {
  deleteBook = output<Book>()

  onDeleteButtonPressed() {
    this.deleteBook.emit(this.book);
  }
}
```
<!-- .element: class="fragment stretch" -->

----
#### `model()`
```typescript
class BookComponent {
  book = model<Book>(); // ModelSignal, writeable

  changeTitle() {
    this.book.update((book) => {
      if (!book) return;
      book.title = "New title";
      return book;
    });
  }
}
```
<!-- .element: class="fragment stretch" -->

Two-Way Data Binding
<!-- .element: class="fragment" -->

---
#### Effects

```typescript
effect(() => doStuff(a(), b()));
``` 
<!-- .element: class="fragment stretch" -->
<ul>
    <li class="fragment fade-in-then-semi-out">läuft, wenn <code>a</code> oder <code>b</code> updaten</li>
</ul>

```typescript
effect(() => {
  console.log(`Current count: ${count()}`);
});
```
<!-- .element: class="fragment stretch" -->

<ul>
  <li class="fragment fade-in-then-semi-out">läuft, wenn sich <code>count</code> ändert</li>
  <li class="fragment fade-in-then-semi-out">im Konstruktor</li>
</ul>

```typescript
constructor() {
  effect(() => {
    console.log(`The count is: ${this.count()}`);
  });
```
<!-- .element: class="fragment stretch" -->

<ul>
  <li class="fragment fade-in-then-semi-out">logging</li>
  <li class="fragment">Custom DOM Verhalten > templates</li>
</ul>

---
#### Mehrere Components
```typescript
// main.ts
import { signal } from "@angular/core";

export const count = signal(0);
```
<!-- .element: class="fragment stretch" -->

```typescript
// app.component.ts
import { count } from "./main";

@Component({
  template: `
    <div>
      <p>Counter: {{ count() }}</p>
      <button (click)="increment()">Increment by 100</button>
    </div>`
})
export class HundredIncrComponent {
  count = count;

  increment() {
    this.count.update((value) => value + 100);
  }
```
<!-- .element: class="fragment stretch" -->

----
#### Besser: Service
```typescript
@Injectable({
  providedIn: "root",
})
export class CounterService {

  private counterSignal = signal(0);
  readonly counter = this.counterSignal.asReadonly();
  
  methodToMutateSignal() {
    this.counterSignal.update((val) => val + 1);
  }
}
```
<!-- .element: class="fragment stretch" -->
<ul>
    <li class="fragment fade-in-then-semi-out">Änderungen an einer Stelle fokusiert</li>
    <li class="fragment">Bei Bedarf wird das Service injected</li>
</ul>

---
#### `ChangeDetectionStrategy.Default`

```typescript
if (nothingToDo())
   for (let component of tree) {
       if (component.hasChanged()) 
           component.updateUI();
   }
```
<!-- .element: class="fragment" -->

![](./assets/ng-signals/default.gif)
<!-- .element: class="fragment" -->

----
#### `ChangeDetectionStrategy.OnPush`
```typescript
@Component({
    ...
    changeDetection: ChangeDetectionStrategy.OnPush
})
```
<!-- .element: class="fragment" -->

![](./assets/ng-signals/on-push.gif)
<!-- .element: class="fragment" -->

update nur, wenn nötig
<!-- .element: class="fragment" -->
