<style>
.box {
    width: 50px;
    height: 50px;
    background: dodgerblue;
    margin: 10px;
}
</style>

# CSS - Positioning

---
#### Box
![Box-Model](./assets/css-positioning/boxmodel.png)

----
#### Standardlayout
```html
<h4>Standardlayout</h4>
<ul>
    <li>Block-Elemente untereinander</li>
    <li>Block-Elemente nehmen 100% Breite ein</li>
    <li><strong>Inline</strong>-Elemente erzeugen 
        <em>keinen</em> Umbruch
    </li>
</ul>
```

<div class="html-render">
<ul>
    <li class="fragment fade-in-then-semi-out">Block-Elemente untereinander</li>
    <li class="fragment fade-in-then-semi-out">Block-Elemente nehmen 100% Breite ein</li>
    <li class="fragment"><strong>Inline</strong>-Elemente erzeugen 
        <em>keinen</em> Umbruch
    </li>
</ul>
</div>

----
```html
<p>1</p>
<span>2</span>
<span>3</span>
<p>4</p>
<span>5</span>
<p>6</p><p>7</p>
```
<div class="html-render fragment">
    <p>1</p>
    <span>2</span>
    <span>3</span>
    <p>4</p>
    <span>5</span>
    <p>6</p><p>7</p>
</div>

---
#### `display`
<dl>
    <dt class="fragment">block</dt>
        <dd class="fragment">100% Breite</dd>
        <dd class="fragment">Zeilenumbrüche vorher und nachher</dd>
        <dd class="fragment">respektiert width/height</dd>
    <dt class="fragment">inline</dt>
        <dd class="fragment">so breit wie nötig</dd>
        <dd class="fragment">keine Zeilenumbrüche</dd>
        <dd class="fragment">ignoriert width/height</dd>
</dl>

<a class="fragment" href="https://developer.mozilla.org/en-US/docs/Web/CSS/display#Result">weitere values</a>

----
#### Use Case Nav bar
```html
<ul>
    <li><a href="#">POS</a></li>
    <li><a href="#">AM</a></li>
    <li><a href="#">TINF</a></li>
</ul>
```
<!-- .element: class="fragment" -->

[Demo](https://codepen.io/scre/pen/wvWVyKV)

---
#### Flexbox
eindimensionale Layouts
<!-- .element: class="fragment" -->


[Demo](https://codepen.io/scre/pen/OJXKQmL)
<iframe class="html-render fragment" src="./assets/css-positioning/flex.html" 
onload="this.style.height=(this.contentWindow.document.body.scrollHeight+20)+'px';">
</iframe>

----
![flex-layout](./assets/css-positioning/flex_terms.png)
<!-- .element: class="fragment" -->

<a class="fragment" href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/" target="_blank">Customization</a>

---
#### Grid
* zweidimensionale Layouts
<!-- .element: class="fragment" -->

<iframe class="html-render fragment" src="./assets/css-positioning/grid.html" 
onload="this.style.height=(this.contentWindow.document.body.scrollHeight+20)+'px';">
</iframe>

----
![grid-layout](./assets/css-positioning/grid.png)
<!-- .element: class="fragment" -->

[Demo](https://codepen.io/collection/AYgOpy)
<!-- .element: class="fragment" -->

[Customization](https://css-tricks.com/snippets/css/complete-guide-grid/)
<!-- .element: class="fragment" -->
