# SOLID

---
#### Motivation
<ul>
    <li class="fragment fade-in-then-semi-out">The more things change, the more they <span class="fragment">break</span></li>
    <li class="fragment">Anforderungen ändern sich ⟹ Code ändert sich</li>
</ul>

![Waterfall Model](./assets/SOLID/waterfall-project-management-approach.png)
<!-- .element: class="fragment" -->

----
#### Agile
![Agile Model](./assets/SOLID/agile-project-management-approach.png)
<!-- .element: class="fragment" -->

---
#### Ziel
*ETC* - easier to change
<!-- .element: class="fragment" -->

<dl>
    <div class="fragment fade-in-then-semi-out">
        <dt>Clean Code</dt>
        <dd>Code leichter lesbar ⟹ ETC</dd>
    </div>
    <div class="fragment fade-in-then-semi-out">
        <dt>DRY - Don't repeat yourself</dt>
        <dd><strong>eine</strong> Änderung ⟹ ETC</dd>
    </div>
    <div class="fragment">
        <dt>Orthogonalität</dt>
        <dd>Keine Codeverflechtungen ⟹ ETC</dd>
        <dd class="fragment">z.B. Datenbank wird von MySQL auf PostgreSQL portiert, GUI bleibt gleich</dd>
    </div>
</dl>

----
#### SOLID
Guidelines, keine Regeln  
<!-- .element: class="fragment fade-in-then-semi-out" -->
<em>"Uncle Bob" Robert C. Martin</em>
<!-- .element: class="fragment fade-in-then-semi-out" -->



<ul class="first-letter-large">
    <li class="fragment fade-in-then-semi-out"><strong>S</strong>ingle responsibility principle</li>
    <li class="fragment fade-in-then-semi-out"><strong>O</strong>pen closed Principle</li>
    <li class="fragment fade-in-then-semi-out"><strong>L</strong>iskov substitution principle</li>
    <li class="fragment fade-in-then-semi-out"><strong>I</strong>nterface segregation principle</li>
    <li class="fragment"><strong>D</strong>ependency inversion principle</li>
</ul>

---
#### SRP - Single responsibility principle
![](./assets/SOLID/srp.png)

----
#### SRP - Single responsibility principle

*Edsger W. Dijkstra - Seperation of concerns*
<!-- .element: class="fragment fade-in-then-semi-out" -->

*Gather together the things that change for the same reasons. Separate things that change for different reasons.*
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
public class Employee {

    public Money calculatePay()
    public void save()
    public String reportHours()
}
```
<!-- .element: class="fragment" -->

* Buchhaltung verlangt Änderung ⟹ Recompile
<!-- .element: class="fragment fade-in-then-semi-out" -->
* DBA verlangt Änderung ⟹ Recompile
<!-- .element: class="fragment fade-in-then-semi-out" -->
* HR verlangt Änderung ⟹ Recompile
<!-- .element: class="fragment fade-in-then-semi-out" -->

note: Seperation of concerns - Dijkstra  
*reason to change* aus der Businessperspektive, nicht aus technischer Sicht

----
```Java
class Book {
 
    public String getTitle()
    public String getAuthor()
    public Page nextPage()
    public void printCurrentPage()
    public void save()
}
```
<!-- .element: class="fragment" -->

----
```Java
class Book {
  
     public String getTitle()
     public String getAuthor()
     public Page nextPage()
     public Page getCurrentPage()
}
```
<!-- .element: class="fragment" -->

```Java
interface Printer {
 
    void printPage(Page page);
}
```
<!-- .element: class="fragment" -->
 
```Java
class PlainTextPrinter implements Printer {
 
    public void printPage(Page page) {}
}

class HtmlPrinter implements Printer {
 
    public void printPage(Page page) 
}
```
<!-- .element: class="fragment" -->

---
#### Dependency Inversion Principle
![](./assets/SOLID/dip.png)

----
#### Dependency Inversion Principle
1. *High-level modules should not depend on low-level modules. Both should depend on the abstraction.*
<!-- .element: class="fragment fade-in-then-semi-out" -->
1. *Abstractions should not depend on details. Details should depend on abstractions.*
<!-- .element: class="fragment fade-in-then-semi-out" -->

![module-model](./assets/SOLID/without-dip.svg)
<!-- .element: class="fragment" -->

----
#### Mit DIP
![module-model](./assets/SOLID/with-dip.svg)
<!-- .element: class="fragment" -->

* Klassen haben nur Referenzen auf Interfaces
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Detail-Klassen implementieren diese Interfaces
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Wie kommt das Objekt in die Klasse?
<!-- .element: class="fragment" -->

----
#### Dependency Injection
```Java
public class TextEditor {
    private SpellChecker checker;

    public TextEditor() {
        this.checker = new EnglishSpellChecker();
    }
}
```
<!-- .element: class="fragment fade-in-then-semi-out"-->
"new is glue"
<!-- .element: class="fragment fade-in-then-semi-out"-->
```Java
public class TextEditor {
    private SpellChecker checker;

    public TextEditor(SpellChecker checker) {
        this.checker = checker;
    }
}
```
<!-- .element: class="fragment"-->

note: Setter Injection, Field Injection

---
#### Open closed principle
![](./assets/SOLID/ocp.png)

----
#### Open closed principle
*A Module should be open for extension but closed for modification.*
<!-- .element: class="fragment fade-in-then-semi-out" -->

Code ist so zu schreiben, dass neue Features eingefügt werden können, ohne den bestehenden Code zu verändern.
<!-- .element: class="fragment" -->

----
#### Extension? ⟹ `extends`!
![animal>eagle-uml](./assets/SOLID/animal-eagle-uml.png)
<!-- .element: class="fragment" -->

```Java
import Eagle;

class App {

    public static void main(String[] args) {
        Eagle eagle = new Eagle();
        eagle.makeSound();
        eagle.fly();
    }
}
```
<!-- .element: class="fragment" -->

`makeSound` returnt `String` ⟹ 🤔
<!-- .element: class="fragment" -->

notes:
* Änderung sollte sich auf Animal-User auswirken, nicht auf `Eagle`-User
* `extends` bei Klassen erzeugt coupling ⟹ rigidity

----
#### Vererbung
<dl>
    <dt>+</dt>
    <dd><ul>
        <li class="fragment fade-in-then-semi-out">DRY - Code existiert nur einmal</li>
        <li class="fragment fade-in-then-semi-out">Polymorphie</li>
    </ul></dd>
</dl>

```Java
class Derived extends Super {

    public int foo() {
        return getX() + super.foo();    
    }     
}
```
<!-- .element: class="fragment" -->

* Erzeugt Abhängigkeit ⟹ nicht ETC
<!-- .element: class="fragment" -->

<dl class="fragment">
    <dt>++</dt>
    <dd><ul>
        <li class="fragment fade-in-then-semi-out">DRY - Auslagern in eigene Klasse (SRP)</li>
        <li class="fragment">Polymorphie - Interfaces</li>
    </ul></dd>
</dl>

notes: Ist bei Interface dasselbe -> Dependency inversion principle

----
```Java
public interface CalculatorOperation { }
```
<!-- .element: class="stretch fragment" -->

```Java
public class Addition implements CalculatorOperation{
    private double left;
    private double right;
    private double result;

    ...
}
```
<!-- .element: class="stretch fragment" -->

```Java
public void calculate(CalculatorOperation operation) {
    if (operation instanceof Addition addition) {
        var result = addition.getLeft() + addition.getRight();
        addition.setResult(result);
    } else if (operation instanceof Subtraction subtraction) {
        var result = subtraction.getLeft() - subtraction.getRight();
        subtraction.setResult(result);
    } 
}
```
<!-- .element: class="stretch fragment" -->

Customer will `Multiplication`  
⟹ Änderung notwendig ⟹ nicht OCP
<!-- .element: class="stretch fragment" -->


----
```Java
public interface CalculatorOperation { 

    double perform();
}
```
<!-- .element: class="fragment" -->

```Java
public class Addition implements CalculatorOperation{
    private double left;
    private double right;

	@Override
    public double perform() {
        return left + right;
    }
}
```
<!-- .element: class="fragment" -->

```Java
public void calculate(CalculatorOperation operation) {
    var result = operation.perform();
}
```
<!-- .element: class="fragment" -->

OCP ✔️
<!-- .element: class="fragment" -->

----
#### `sealed`
```Java
sealed class Animal permits Dog, Cat, Alien { }
```
<!-- .element: class="fragment" -->

```Java
class Fish extends Animal { } 🚫
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
final class Dog extends Animal { }
```
<!-- .element: class="fragment" -->

```Java
sealed class Cat extends Animal permits Tiger { }
```
<!-- .element: class="fragment" -->

```Java
final class Tiger extends Cat { }
```
<!-- .element: class="fragment" -->

```Java
non-sealed class Alien extends Animal { }
```
<!-- .element: class="fragment" -->

```Java
var sound = switch(animal) {
	case Dog dog -> dog.bark();
	case Tiger tiger -> tiger.roar();
	case Alien alien -> alien.sound();
	// no default needed
}
```
<!-- .element: class="fragment" -->

---
![](./assets/SOLID/lsp.png)

----
#### Liskov substitution principle
*Sei **q(x)** eine beweisbare Eigenschaft von Objekten **x** des Typs **T**. Dann soll **q(y)** für Objekte **y** 
des Typs **S** wahr sein, wobei **S** ein Untertyp von **T** ist*
<!-- .element: class="fragment fade-in-then-semi-out" -->

*A program that uses an interface must not be confused by an implementation of that interface.*
<!-- .element: class="fragment fade-in-then-semi-out" -->

* normalerweise ✔️
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Verletzung: teilweises Implementieren eines interfaces
<!-- .element: class="fragment" -->

----
```Java
public class UserAdder {

    public void addUser(Collection<User> users, User user) {
        users.add(user);
    }
}
```
<!-- .element: class="fragment" -->

SRP ✔️
<!-- .element: class="fragment" -->

```Java
var adder = new UserAdder();
var users = List.of(new User("Albert"), new User("Bernd"));
adder.addUser(users, new User("Christina"));
```
<!-- .element: class="fragment" -->

```Plain Text
Exception in thread "main" java.lang.UnsupportedOperationException
```
<!-- .element: class="fragment" -->

---
#### Interface segregation principle
![](./assets/SOLID/isp.png)

----
#### Interface segregation principle
*Keep interfaces small so that users don’t end up depending on things they don’t need.*
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
public interface FileStorer {

    byte[] load(Path path);
    void delete(Path path);
    void save(Path path, Stream<String> lines);
    boolean exists(Path path);
    void createDirectory(Path path);
    Stream<Path> getSubDirectories(Path path);
    Stream<Path> getFiles(Path path);
}
```
<!-- .element: class="fragment" -->

Aus bestehendem interface wird für jede Client-Rolle ein neues interface abgesondert
<!-- .element: class="fragment" -->

----
#### RoleInterfaces
![without-isp](./assets/SOLID/without-isp.png)
<!-- .element: class="fragment" -->
![with-isp](./assets/SOLID/with-isp.png)
<!-- .element: class="fragment" -->

----
#### SRP vs ISP
* SRP trifft Aussagen über die Implementierung
<!-- .element: class="fragment fade-in-then-semi-out" -->
* ISP trifft Aussagen über Interfaces
<!-- .element: class="fragment fade-in-then-semi-out" -->



<ol style="margin-top: 10%">
    <li class="fragment">SRP Verletzung</li>
    <li class="fragment">neue Klassen</li>
    <li class="fragment">neue Rollen</li>
    <li class="fragment">ISP</li>
</ol>

---
#### Zusammenfassung

* SRP - nur eine Sache machen
<!-- .element: class="fragment fade-in-then-semi-out" -->
* OCP
<!-- .element: class="fragment fade-in-then-semi-out" -->
  * Klassen extenden keine Klassen
  <!-- .element: class="fragment fade-in-then-semi-out" -->
  * Composition > Inheritance
  <!-- .element: class="fragment fade-in-then-semi-out" -->
  * alte Funktionalität nicht beeinflussen
  <!-- .element: class="fragment fade-in-then-semi-out" -->
* LSP - Interfaces vollständig implementieren
<!-- .element: class="fragment fade-in-then-semi-out" -->
* ISP - SRP auch für Interface
<!-- .element: class="fragment fade-in-then-semi-out" -->
* DIP
<!-- .element: class="fragment fade-in-then-semi-out" -->
  * alle Typen Interfaces (außer domain)
  <!-- .element: class="fragment fade-in-then-semi-out" -->
  * kein new (außer domain)
  <!-- .element: class="fragment" -->

----
* Testbarkeit ✔️
<!-- .element: class="fragment fade-in-then-semi-out"-->
* Ersetzbarkeit ✔️
<!-- .element: class="fragment fade-in-then-semi-out"-->
* Wiederverwendbarkeit ✔️
<!-- .element: class="fragment fade-in-then-semi-out"-->