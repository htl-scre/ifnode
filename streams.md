# Java 8 Streams

---
#### Motivation
Code als Datentransformationen sehen
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
public int calcTotalWorkingHoursFromFile(Path path) throws IOException {
    try (BufferedReader reader = Files.newBufferedReader(path)) {
        String line;
        int sum = 0;
        while ((line = reader.readLine()) != null) {
            Employee employee = new Employee(line);
            if (employee.getJob() == Job.PROGRAMMER) {
                Project project = employee.getProject();
                sum += project.getWorkingHours();
            }
        }
        return sum;
    }
}
```
<!-- .element: class="stretch fragment" -->

----
<ul>
    <li class="fragment fade-in-then-semi-out">Zeile lesen</li>
    <li class="fragment fade-in-then-semi-out"><code>new Employee(line)</code></li>
    <li class="fragment fade-in-then-semi-out"><code>.getProject()</code></li>
    <li class="fragment fade-in-then-semi-out"><code>.getWorkingHours()</code></li>
    <li class="fragment fade-in-then-semi-out">Summe</li>
</ul>

```Elixir
String.split(String.upcase("wtf elixir"))
```
<!-- .element: class="fragment" -->

```Elixir
"wtf elixir" |> String.upcase() |> String.split()
```
<!-- .element: class="fragment" -->

```Elixir
defp calc_total_working_hours_from_file(file) do
    file
    |> File.read()  # line1\nline2\nline3...
    |> String.split("\n", trim: true)
    |> Employee.new()
    |> Employee.get_project()
    |> Project.get_workingHours()
    |> Enum.sum()
end
```
<!-- .element: class="fragment" -->

note: Elixir, basierend auf Erlang, functional

----
```Java
public int calcTotalWorkingHoursFromFile(Path path) throws IOException {
    try (Stream<String> stream = Files.lines(path)) {
        return stream.map(Employee::new)
                .filter(e -> e.getJob() == Job.PROGRAMMER)
                .map(Employee::getProject)
                .mapToInt(Project::getWorkingHours)
                .sum();
    }
}
```
<!-- .element: class="stretch fragment" -->

Streams ⟹ Lambdas ⟹ Functional interfaces
<!-- .element: class="fragment" -->

---
#### `Stream<T>`
<dl>
    <dt class="fragment">Datenpipeline</dt>
    <dd>
        <ol>
            <li class="fragment">source</li>
            <li class="fragment">intermediate operations</li>
            <li class="fragment">terminal operation</li>
        </ol>
    </dd>
    <dt class="fragment"><em>lazy</em></dt>
    <dd class="fragment">Operationen laden <strong>bei Bedarf</strong> neues Element</dd>
    <dd class="fragment">1TB großes File zusammenfassen ✔️</dd>
    <dt class="fragment">immutable</dt>
    <dd class="fragment">Methoden liefern neue Streams</dd>
</dl>

---
#### Source
```Java
Stream<String> names = Stream.of("Alfred", "Birgit");
```
<!-- .element: class="fragment" -->

```Java
LongStream ids = Arrays.stream(new long[]{42, 2, 37});
```
<!-- .element: class="fragment" -->

```Java
Stream<?> elements = collection.stream();
```
<!-- .element: class="fragment" -->

```Java
IntStream.range(0, 4);
    => [0, 1, 2, 3]
```
<!-- .element: class="fragment" -->

```Java
IntStream lottoNumbers = new Random().ints(1, 46);
```
<!-- .element: class="fragment" -->

```Java
Supplier<String> supplier = () -> "java";
Stream<String> javas = Stream.generate(supplier);
    => [java, java, java, java, ...]
```
<!-- .element: class="fragment" -->

```Java
Stream.iterate("-", s -> s + "-");
    => [-, --, ---, ----, -----, ...]
IntStream.iterate(1, i -> i *= 10)
    => [1, 10, 100, 1_000, 10_000, ...]
```
<!-- .element: class="fragment" -->

----
```Java
"P0S 🐢".chars()   // IntStream
                => [80, 48, 83, 32, 55357, 56354]
```
<!-- .element: class="fragment" -->

`IntStream` der `char` - Values
<!-- .element: class="fragment" -->

```Java
try (Stream<String> lines = Files.lines(path)) { 
    lines. ...
}
```
<!-- .element: class="fragment" -->

IO-Streams schließen!
<!-- .element: class="fragment" -->

---
#### Intermediate Operations
<dl>
    <dt class="fragment"><code>filter(predicate)</code></dt>
    <dd class="fragment">entfernt Elemente, für die <code>predicate.test() false</code> liefert</dd>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
Stream.of("--", "-", "---")
        .filter(s -> s.length() > 1);   <span class="fragment">=> [--, ---]</span>
    </code></pre>
    <dt class="fragment"><code>distinct()</code></dt>
    <dd class="fragment">entfernt doppelte Elemente</dd>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
Stream.of(42, 37, 42)
        .distinct();    <span class="fragment">=> [42, 37]</span>
    </code></pre>
</dl>

----
<dl>
    <dt class="fragment"><code>limit(long maxSize)</code></dt>
    <dd class="fragment">maximal <code>maxSize</code> Elemente dürfen weiter</dd>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
Stream.iterate("-", s -> s + "-")
        .limit(3);  <span class="fragment">=> [-, --, ---]</span>
    </code></pre>
    <dt class="fragment"><code>skip(long n)</code></dt>
    <dd class="fragment">überspringt <code>n</code> Elemente</dd>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
IntStream.range(4, 9)
        .skip(2);   <span class="fragment">=> [6, 7, 8]</span>
    </code></pre>
    <dt class="fragment"><code>sorted()</code></dt>
    <dd class="fragment">sortiert den Stream</dd>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
new Random().ints(3, 0, 10)
        .sorted()  <span class="fragment">=> [0, 1, 5]</span>
    </code></pre>
</dl>

----
<dl>
    <dt class="fragment"><code>map(function)</code></dt>
    <dd class="fragment">transformiert mit <code>function</code> alle Elemente</dd>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
Stream.of("Alfred", "Bernd", "Christina")
        .map(s -> s.charAt(0));  <span class="fragment">=> [A, B, C]</span>
    </code></pre>
    <dt class="fragment"><code>mapToXxx(function)</code></dt>
    <dd class="fragment">wie <code>map</code>, retourniert <code>XxxStream</code></dd>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
Stream.of("Alfred", "Bernd", "Christina")
        .mapToInt(String::length);   <span class="fragment">=> [6, 5, 9]</span>
    </code></pre>
    <dt class="fragment"><code>peek(consumer)</code></dt>
    <dd class="fragment">führt <code>consumer</code> aus, aber Elemente dürfen weiter</dd> 
    <dd class="fragment">nützlich zum Debuggen</dd>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
Stream.iterate(100, n -> n > 0, n -> n / 10)
        .peek(System.out::println);
    </code></pre>
</dl>

----
#### Nur für `XxxStream`
<dl style="width:85%">
    <dt class="fragment"><code>mapToObj(function)</code></dt>
    <dd class="fragment">wie <code>map</code>, retourniert <code>Stream&ltT></code></dd>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
DoubleStream.of(3.14, 1.41, 2.71)
        .mapToInt(d -> (int) d)         <span class="fragment">=> [3, 1, 2]</span>    
        .mapToObj(n -> "-".repeat(n))); <span class="fragment">=> [---, -, --]</span>
    </code></pre>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
"P0S".chars()   // IntStream [80, 48, 83]
        .mapToObj(Character::toString)  => [P, 0, S]
    </code></pre>
    <dt class="fragment"><code>boxed()</code></dt>
        <dd class="fragment">retourniert <code>Stream&ltT></code></dd>
        <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
IntStream.range(0,10)   // IntStream
        .boxed();       // Stream&ltInteger>
        </code></pre>
</dl>

----
#### Flatmap
```Java
Stream.of("3:2", "1:0", "0:2")  // Stream<String>
        .map(result -> {
            String[] splitted = result.split(":");
            return Arrays.stream(splitted);
        }); // Stream<Stream<String>> [[3, 2], [1, 0], [0, 2]]
```
<!-- .element: class="fragment" -->

Prinzipiell möglich, oft unerwünscht
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
Stream.of("3:2", "1:0", "0:2")  // Stream<String>
        .flatMap(result -> {
            String[] splitted = result.split(":");
            return Arrays.stream(splitted);
        }); // Stream<String> [3, 2, 1, 0, 0, 2]
```
<!-- .element: class="fragment" -->

----
#### Parallelisierung
```Java
IntStream.range(0, 3)  => [0, 1, 2]
        .parallel()     => [?, ?, ?]
```
<!-- .element: class="fragment" -->

<ul>
    <li class="fragment fade-in-then-semi-out">nutzt mehrere Cores der CPU</li>
    <li class="fragment fade-in-then-semi-out">schneller</li>
    <li class="fragment fade-in-then-semi-out">teilweise problematisch</li>
    <li class="fragment">stateful - Operationen synchronisieren Status, z.B:
        <ul>
            <li class="fragment"><code>sorted()</code></li>
            <li class="fragment"><code>distinct()</code></li>
        </ul>
    </li>
</ul>

---
#### Terminal
```Java
public interface Stream<T> extends BaseStream<T, Stream<T>> {

    long count();
    Optional<T> min(Comparator<? super T> comparator);
    Optional<T> max(Comparator<? super T> comparator);
    boolean anyMatch(Predicate<? super T> predicate);
    boolean allMatch(Predicate<? super T> predicate);
    boolean noneMatch(Predicate<? super T> predicate);
    Object[] toArray();
    ...
}
```
<!-- .element: class="fragment" -->

<dl style="width:100%">
    <dt class="fragment"><code>&ltA> A[] toArray(intFunction)</code></dt>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
String[] subjects = Stream.of("POS", "AM", "TINF")
        .toArray(String[]::new);    <span class="fragment">=> [POS, AM, TINF]</span>
    </code></pre>
</dl>

----
<dl style="width:100%">
    <dt class="fragment"><code>void forEach(consumer)</code></dt>
    <dd class="fragment">führt <code>consumer</code> für alle Elemente aus</dd>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
"POS".chars()                                   <span class="fragment">=> [80, 79, 83]</span>
        .mapToObj(i -> String.format("%d ", i)) <span class="fragment">=> [80 , 79 , 83 ]</span>
        .forEach(System.out::print);            <span class="fragment">=> 80 79 83 </span>
    </code></pre>
    <dt class="fragment"><code>Optional&ltT> findAny()</code> / <code>findFirst()</code></dt>
    <dd class="fragment">liefert <strong>ein</strong> Element; üblicherweise nach <code>filter</code></dd>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
            Optional&ltString> optionalSubject = Stream.of("POS", "AM", "TINF")
                    .filter(s -> s.length() > 2)
                    .findFirst();   <span class="fragment">=> [POS]</span>
    </code></pre>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
            Optional&ltString> optionalSubject = Stream.of("POS", "AM", "TINF")
                    .parallel()
                    .filter(s -> s.length() > 2)
                    .findAny();
    </code></pre>
</dl>

----
<dl>
    <dt class="fragment"><code>T reduce(T identity, accumulator)</code></dt>
    <dd class="fragment">Reduziert <code>Stream&ltT></code> auf <strong>ein</strong> Objekt</dd>
    <dd class="fragment"><code>identity</code> Startwert</dd>
    <dd class="fragment"><code>accumulator(T result, T element)</code> transformiert das Paar <code>result, element</code> in ein neues <code>result</code></dd>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
int product = IntStream.of(2, 3, 4)
        .reduce(1, (result, factor) -> result * factor);  <span class="fragment">=> 24</span>
    </code></pre>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
try (Stream&ltString> lines = Files.lines(path)) {
    String content = lines.reduce("",
            (result, line) -> result + "\n" + line);
}
        </code></pre>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
OptionalInt optional___ = random.ints(0, 100)
        .limit(10)
        .reduce((r, e) -> r < e ? r : e);   <span class="fragment">=> .min()</span>
    </code></pre>
</dl>

----
#### Nur für `XxxStream`
```Java
public interface IntStream extends BaseStream<Integer, IntStream> {

    int sum();
    OptionalInt min();
    OptionalInt max();
    OptionalDouble average();
    IntSummaryStatistics summaryStatistics();
}
```
<!-- .element: class="fragment" -->

```Java
public class IntSummaryStatistics implements IntConsumer {
    
    public final long getSum() { ... }
    public final int getMin() { ... }
    public final int getMax() { ... }
    public final double getAverage() { ... }
}
```
<!-- .element: class="fragment" -->

---
#### `.collect()`
```Java
<R> R collect(Supplier<R> supplier,
              BiConsumer<R, ? super T> accumulator,
              BiConsumer<R, R> combiner);
```
<!-- .element: class="fragment" -->
ähnlich wie `reduce`, nur Rückgabetyp beliebig
<!-- .element: class="fragment" -->

<dl>
    <dt class="fragment"><code>Supplier&lt;R> supplier</code></dt>
    <dd class="fragment">liefert das Returnobjekt</dd>
    <dt class="fragment"><code>BiConsumer&lt;R, ? super T> accumulator</code></dt>
    <dd class="fragment">wird für jedes Element des Streams ausgeführt</dd>
    <dt class="fragment"><code>BiConsumer&lt;R, R> combiner</code></dt>
    <dd class="fragment">für parallele Streams; jeder Einzelstream baut sein eigenes Returnobjekt</dd>
    <dd class="fragment"><code>combiner</code> fasst diese zusammen</dd>
</dl>

----
```Java
StringBuilder builder = IntStream.range(4, 10)
        .parallel()
        .collect(() -> new StringBuilder(),
                (result, i) -> result.append(i)
                        .append("th, "),
                (part1, part2) -> part1.append(part2));
```
<!-- .element: class="fragment" -->

```Plain Text
4th, 5th, 6th, 7th, 8th, 9th, 
```
<!-- .element: class="fragment" -->

```Java
StringJoiner joiner = Stream.of("POS", "AM", "TINF")
                .collect(() -> new StringJoiner(", "),
                        StringJoiner::add,
                        StringJoiner::merge);
```
<!-- .element: class="fragment" -->

```Plain Text
POS, AM, TINF
```
<!-- .element: class="fragment" -->

```Java
List<String> list = Stream.of("POS", "AM", "TINF")
        .parallel()
        .collect(ArrayList::new,
                Collection::add,
                Collection::addAll);
```
<!-- .element: class="fragment" -->

---
#### `Collectors`
erleichtert `collect`-Aufrufe
```Java
String joined = Stream.of("POS", "AM", "TINF")
        .collect(Collectors.joining(", "));
```
<!-- .element: class="fragment" -->

```Plain Text
POS, AM, TINF
```
<!-- .element: class="fragment" -->

```Java
List<String> list = Stream.of("POS", "AM", "TINF")
        .collect(Collectors.toList());
```
<!-- .element: class="fragment" -->

```Java
Set<String> set = Stream.of("POS", "AM", "TINF")
        .collect(Collectors.toSet());
```
<!-- .element: class="fragment" -->

```Java
SortedSet<String> sorted = Stream.of("POS", "AM", "TINF")
        .collect(Collectors.toCollection(TreeSet::new));
```
<!-- .element: class="fragment" -->

----
<dl>
    <dt class="fragment"><code>toMap(keyMapper, valueMapper)</code></dt>
    <dd class="fragment">Erzeugt eine <code>Map</code>; für jedes Element wird mit den <code>Mapper</code>-Funktionen ein Key/Value-Paar bestimmt</dd>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
Map&lt;Integer, String> map = Stream.of("POS", "AM", "TINF")
        .collect(Collectors.toMap(String::length, 
                s -> s)); <span class="fragment">=> {2=AM, 3=POS, 4=TINF}</span>
    </code></pre>
    <dt class="fragment"><code>toMap(..., mergeFunction)</code></dt>
    <dd class="fragment"><code>mergeFunction</code> bei doppelten Keys</dd>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
Map&lt;Integer, String> map = Stream.of("POS", "AM", "TINF", "SYP")
        .collect(Collectors.toMap(String::length, s -> s));
<span class="fragment exception">Exception in thread "main" java.lang.IllegalStateException: 
        Duplicate key 3 (attempted merging values POS and SYP)</span>
    </code></pre>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
Map&lt;Integer, String> map = Stream.of("POS", "AM", "TINF", "SYP")
        .collect(Collectors.toMap(String::length,
                s -> s,
                (old, e) -> e)); <span class="fragment">=> {2=AM, 3=SYP, 4=TINF}</span>
    </code></pre>
</dl>

----
```Java
Map<Integer, List<String>> multimap = Stream.of("POS", "AM", "D", "SYP")
                .collect(Collectors.toMap(String::length,
                        s -> new ArrayList<>(List.of(s)),
                        (part1, part2) -> {
                            part1.addAll(part2);
                            return part1;
                        },
                        TreeMap::new
                ));
```
<!-- .element: class="fragment stretch" -->

```Plaintext
{1=[D], 2=[AM], 3=[POS, SYP]}
```
<!-- .element: class="fragment stretch" -->

`groupingBy` ähnlich wie `toMap`
<!-- .element: class="fragment" -->

```Java
SortedMap<Integer, Long> grouped = IntStream.of(11, 20, 21, 30, 10, 12)
                .boxed()
                .collect(Collectors.groupingBy(i -> i % 10, // Keys
                        TreeMap::new,   
                        Collectors.counting()));    // Values; Collector!
```
<!-- .element: class="fragment stretch" -->

```Plaintext
{0=3, 1=2, 2=1}
```
<!-- .element: class="fragment stretch" -->

---
#### Best practice
<ul>
    <li class="fragment fade-in-then-semi-out">Variable: <code>Collection</code>/<code>Array</code></li>
    <li class="fragment fade-in-then-semi-out">Parameter: <code>Collection</code>/<code>Array</code></li>
    <li class="fragment">Returnwert: <code>Stream</code></li>
</ul>