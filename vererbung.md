# Vererbung

---
#### Motivation
<pre><code class="hljs Java" data-trim data-noescape>
Instrument[] orchestra = new Instrument[50];
orchestra[0] = new Cello();
orchestra[1] = new Violin();
...

for(Instrument i : orchestra)
    <span class="blue">i.play();</span>
</code></pre>

<ul>
    <li class="fragment fade-in-then-semi-out"><code>Cello</code> muss in <code>Instrument</code> gespeichert werden</li>
    <li class="fragment fade-in-then-semi-out">Compiler muss <code>i.play()</code> zusichern können</li>
    <li class="fragment"><em>Polymorphie</em></li>
</ul>

---
#### `extends`
```Java
public class Person {
    private String name;

    public Person(String name) {
        this.name = name;
    }

    public void work() {
        doJob();
    }
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
public class Student extends Person {
    ...
}
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
Person person = new Person("Chuck Norris");
Student student = new Student("Bruce Lee");
student.work();
```
<!-- .element: class="fragment" -->

----
#### Was wird vererbt?
<ul>
    <li class="fragment">Methoden</li>
        <ul>
            <li class="fragment fade-in-then-semi-out"><code>public</code></li>
            <li class="fragment fade-in-then-semi-out"><code>protected</code></li>
            <li class="fragment fade-in-then-semi-out"><code>package-private</code></li>
        </ul>
    <li class="fragment">Felder</li>
        <ul>
            <li class="fragment fade-in-then-semi-out"><strong>alle</strong></li>
            <li class="fragment fade-in-then-semi-out"><code>private</code> - kein Zugriff!</li>
         </ul>
    <li class="fragment"><strong>keine</strong> Konstruktoren</li>    
    <li class="fragment"><strong>keine</strong> <code>static</code> Member</li>    
</ul>

---
#### Konstruktion
Jeder Konstruktor ruft *implizit* durch `super()` einen Konstruktor der Superklasse auf  
  ⟹ nur **eine** Klasse extendbar
```Java
public class Student {}
```
```Java
public class Student {
    public Student() {
        super();
    }
}
```
<!-- .element: class="fragment" -->

note: Welchen Konstruktor ruft `new Person()` auf? -> `new Object()`

----
```Java
public class Person {
    private String name;

    public Person(String name) {
        this.name = name;
    }
```
<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
public class Student extends Person {
    private int nr;

    public Student(String name, int nr) {
        <span class="fragment">super(name);
        this.nr = nr;
    }</span>
</code></pre>

* Erste<!-- .element: class="fragment fade-in-then-semi-out" --> Zeile eines Konstruktors: `this(...);`/`super(... );`   
  Sonst nirgends!  🚫
* Implizit: <!-- .element: class="fragment fade-in-then-semi-out" -->`super();`

note: Wie ruft man `Person(String name)` auf?
----
![animal>dog>shepherd](assets/OOP/uml_animal_dog_shepherd.png)
```Java
new GermanShepherd();
    ---> new Dog();
        ---> new Animal();
            ---> new Object();
```
<!-- .element: class="fragment" -->

---
<h4><code class="yellow">@Override</code></h4>

Subklassen *dürfen* Methoden überschreiben
```Java
public class Person {
    public void work() {
        doJob();
    }
```
```Java
public class Student extends Person {
    @Override
    public void work() {
        study();
    }
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

----
#### Erlaubte Änderungen
```Java
Dog identifyDog(Dog dog) throws NoDogException
```

<div class="fragment">
Sichtbarkeit <em>generalisieren</em>
    <pre><code class="hljs Java" data-trim data-noescape>
public Dog identifyDog(Dog dog) throws NoDogException
protected Dog identifyDog(Dog dog) throws NoDogException
private Dog identifyDog(Dog dog) throws NoDogException<span class="fragment"> 🚫</span>
    </code></pre>
</div>

<div class="fragment">
Rückgabetyp <em>spezialisieren</em>
    <pre><code class="hljs Java" data-trim data-noescape>
GermanShepherd identifyDog(Dog dog) throws NoDogException
Animal identifyDog(Dog dog) throws NoDogException<span class="fragment"> 🚫</span>
    </code></pre>
</div>

<div class="fragment">
<em>checked</em> Exceptions <em>spezialisieren</em>
    <pre><code class="hljs Java" data-trim data-noescape>
Dog identifyDog(Dog dog)
Dog identifyDog(Dog dog) throws NullPointerException
Dog identifyDog(Dog dog) throws NoGermanShepherdException
Dog identifyDog(Dog dog) throws NoAnimalException<span class="fragment"> 🚫</span>
Dog identifyDog(Dog dog) throws IOException<span class="fragment"> 🚫</span>
    </code></pre>
</div>

----
#### `final`
<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
final int a = 3;
a = 0; <span class="fragment">🚫</span>
</code></pre>
<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
final Person person = new Person("Max");
person.setName("Karl");
person = new Person("Karl"); <span class="fragment">🚫</span>
</code></pre>
<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
final class String {
    ...
}
class CustomString738 extends String <span class="fragment">🚫</span>
</code></pre>
<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
class Person {
    final void work() {
    ...
}
class Student extends Person{
    @Override
    void work() { <span class="fragment">🚫</span>
        study();
    }
}
</code></pre>

---
#### `equals`

<code>==</code> vergleicht auf Objektgleichheit

```Java
Person person = new Person("name");
Person otherPerson = person;
person == otherPerson -> true
```
<!-- .element: class="fragment" -->

```Java
new Person("name") == new Person("name") -> false
```
<!-- .element: class="fragment" -->

<p class="fragment"><code >a.equals(b)</code> vergleicht auf Inhaltsgleichheit</p>

```Java
new Student("Karl", 3).equals(new Student("Karl", 3)) -> true
```
<!-- .element: class="fragment" -->

```Java
new Student("Max", 5).equals(new Student("Max", 7)) -> false
```
<!-- .element: class="fragment" -->

----

```Java
public class Object() {
    public boolean equals(Object obj) {
        return (this == obj);
    }
```

```Java
public class Dog extends Animal{
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        if (!super.equals(o))
            return false;
        Dog dog = (Dog) o;
        return weight == dog.weight &&
                Objects.equals(breed, dog.breed);
    }
```
<!-- .element: class="fragment" -->

---
#### Polymorphie
```Java
Person student = new Student("Max", 1);
student.work();
```

```Java
Compiletyp reference = new Laufzeittyp();
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

<dl>
    <dt class="fragment">Compiletyp</dt>
        <dd class="fragment fade-in-then-semi-out">Existenz</dd>
        <dd class="fragment fade-in-then-semi-out">Sichtbarkeit</dd>
        <dd class="fragment fade-in-then-semi-out">Returntyp</dd>
        <dd class="fragment fade-in-then-semi-out">Exceptions</dd>
    <dt class="fragment">Laufzeittyp</dt>
        <dd class="fragment fade-in-then-semi-out">Code <em>dieser</em> Klasse wird ausgeführt</dd>
        <dd class="fragment">Subtyp des Compiletyps</dd>
</dl>

---
#### Casten
Ein Cast ändert den **Compile**typ, kann aber zur Laufzeit eine `ClassCastException` verursachen
```Java
Dog dog = (Dog) "Dog"; 🚫
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
Animal animal = new ___();
Dog dog = (Dog) animal;
```
<!-- .element: class="fragment" -->

<dl>
    <dt class="fragment"><code>Animal</code>, <code>Cat</code>, <code>Bird</code></dt>
        <dd class="fragment c">Compiler: <code>Animal</code> könnte <code>Dog</code> enthalten ✔️</dd>
        <dd class="fragment fade-in-then-semi-out"><code>ClasscastException 💥</code></dd>
    <dt class="fragment"><code>Dog</code></dt>
        <dd class="fragment fade-in-then-semi-out">Compiler: <code>Animal</code> könnte <code>Dog</code> enthalten ✔️</dd>
        <dd class="fragment">Laufzeit ✔️</dd>
</dl>
note: wie Cast absichern?

----
#### `instanceof`/`.getClass`
<dl>
    <dt><code>instanceof</code></dt>
    <dd>Überprüft, ob Laufzeittyp (Sub)klasse von <code>X</code> 
    <pre class="fragment fade-in-then-semi-out"><code class="hljs Java" data-trim data-noescape>
    if (obj instanceof X) {
        X x = (X) obj;
        foo(x);
    }
    </code></pre>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
    if (obj instanceof X x) {
        foo(x);
    } 
    </code></pre>
    </dd>
    <dt><code class="fragment">.getClass</code></dt>
    <dd class="fragment">Überprüft, ob Laufzeittyp <code>X</code>
    <pre><code class="hljs Java" data-trim data-noescape>
    if (obj.getClass() == X.class)
    </code></pre>
    </dd>
</dl>
