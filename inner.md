# Nested/innere Klassen

---
#### Motivation
```Java
public class LinkedList {
    private Node first;
    
    public Object getFirst() {
        if (first == null)
            throw new NoSuchElementException();
        return first.getItem();
    }
    ...
```
```Java
public class Node {
    private Object item;
    private Node next;
    ... 
```
<ul>
    <li class="fragment">Klassen stark aneinander gekoppelt</li>
    <li class="fragment"><code>Node</code> wird nur in <code>LinkedList</code> verwendet</li>
</ul>

---
#### Nested/`static` inner
<pre><code class="hljs Java stretch" data-trim data-noescape>
public class Outer {
    private static int staticVar;
    private int instanceVar;
    private static void staticMethod() {}
    private void instanceMethod() {}
    
    <span class="fragment">private static class Nested {</span>
        <span class="fragment">private static int nestedStaticVar;
        private int nestedVar;
        private void foo() {
            staticVar++;        <span class="fragment">✔️</span>
            staticMethod();     <span class="fragment">✔️</span>
            instanceVar++;      <span class="fragment">🚫</span>
            instanceMethod();   <span class="fragment">🚫</span>
        }
    }</span>
    public static void main(String[] args) {
        <span class="fragment">Nested.nestedStaticVar++;</span>
        <span class="fragment">new Nested().nestedVar++;</span>
        <span class="fragment">new Outer.Nested().foo();</span>       
    }
}
</code></pre>

----
<pre><code class="hljs Java stretch" data-trim data-noescape>
public class LinkedList {
    Node first;

    public Object getFirst() {
        Node f = first;
        if (f == null)
            throw new NoSuchElementException();
        return f.item;
    }

    private static class Node {
        Object item;
        Node next;

        Node(Object item, Node next) {
            this.item = item;
            this.next = next;
        }
    }
}
</code></pre>
<ul>
    <li class="fragment">Einfacher Zugriff <span class="monospace">Outer -> Inner</span></li>
    <li class="fragment"><span class="monospace">Outer</span> möchte verstecken, dass <span class="monospace">Inner</span> existiert</li>
</ul>


---
#### inner
<pre><code class="hljs Java stretch" data-trim data-noescape>
public class Outer {
    ...
    private class Inner {
        private static int nestedStaticVar; <span class="fragment">🚫</span>
        private int innerVar;
    
        private void foo() {
            staticVar++;        <span class="fragment">✔️</span>
            staticMethod();     <span class="fragment">✔️</span>
            instanceVar++;      <span class="fragment">✔️</span>
            instanceMethod();   <span class="fragment">✔️</span>
        }
    }
    
    public static void main(String[] args) {
        new Outer.Inner();      <span class="fragment">🚫</span>
        <span class="fragment">Outer outer = new Outer();
        Outer.Inner inner = outer.new Inner();</span>
    }
}
</code></pre>
Einfacher Zugriff <span class="monospace">Outer <-> Inner</span>

---
#### Local
```Java
button.setOnAction(EventHandler handler);
```

```Java
@FunctionalInterface
public interface EventHandler extends EventListener {
    void handle(Event event);
}
```
<!-- .element: class="fragment" -->

```Java
public void initialize(URL location, ResourceBundle resources) {
    class CustomEventHandler implements EventHandler {

        @Override
        public void handle(Event event) {
            foo();
        }
    }
    Button button = new Button();
    button.setOnAction(new CustomEventHandler());
}
```
<!-- .element: class="fragment" -->

----
#### Closure
<pre><code class="hljs Java stretch" data-trim data-noescape>
public static void main(String[] args) {
    String s = "Closure?";
    class Local {
        void foo() {
            System.out.println(s); <span class="fragment">🚫</span>
        }
    }
    s += "Object, welche den aktuellen Zustand kapselt";
}
</code></pre>
<span class="fragment">Error: local variables referenced from an inner class must be final or effectively final</span>

----
#### 😱
```Java
Object foo() {
    String s = "wtf";
    class Local {
        void foo() {
            System.out.println(s);
        }
    }
    return new Local();
}
```
<ul>
    <li class="fragment">Referenz <code>s</code> wird am Stack angelegt</li>
    <li class="fragment">Objekt <code>"wtf"</code> muss erhalten bleiben</li>
    <li class="fragment">⟹ <code>Local</code> muss eine Referenz darauf haben</li>
</ul>

----
#### 😱 final 😱
<pre><code class="hljs Java stretch" data-trim data-noescape>
Object foo() {
    String s = "wtf";
    class Local {
        void foo() {
            System.out.println(s);
        }
    }
    Object result = new Local();
    Runnable mayHaveRun = () -> s = null; <span class="fragment">🚫</span>
    mayHaveRun.start();
    return result;
}
</code></pre>

---
#### Anonym
```Java
public void initialize(URL location, ResourceBundle resources) {
    Button button = new Button();
    button.setOnAction(new EventHandler() {
    
        @Override
        public void handle(Event event) {
            foo();
        }
    });
}
```

```Java
public void initialize(URL location, ResourceBundle resources) {
    Button button = new Button();
    button.setOnAction(ev -> foo());
}
```
<!-- .element: class="fragment" -->

---
#### Best-practice
<dl>
    <div class="fragment">
        <dt>Brauch ich nur <em>eine</em> Referenz?</dt>
        <dd>Anonyme innere Klasse</dd>
    </div>
    <div class="fragment">
        <dt>Outer -> Inner</dt>
        <dd>nested</dd>
    </div>
    <div class="fragment">
        <dt>Outer <-> Inner</dt>
        <dd><span class="fragment">ganz sicher?</span> <br>
        <span class="fragment">inner</span>
    </dd></div>
</dl>