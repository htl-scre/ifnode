# Concurrency Utilities

---
#### `Callable`
Probleme mit Runnable:
<!-- .element: class="fragment" -->
1. Keine Returnmöglichkeit
<!-- .element: class="fragment fade-in-then-semi-out" -->
1. Kein Werfen von Exceptions
<!-- .element: class="fragment fade-in-then-semi-out" -->
1. Abbruch schwierig
<!-- .element: class="fragment" -->

```Java
public interface Callable<V> {

    V call() throws Exception;
}
```
<!-- .element: class="fragment" -->

---
#### Motivation Threadpools
```Java
class Server {

    public static void main(String[] args) throws IOException {
        ServerSocket socket = new ServerSocket(9000);
        while (true) {
            final Socket s = socket.accept();
            new Thread(() -> doWork(s)).start();
        }
    }

    static void doWork(Socket s) {}
}
```
<!-- .element: class="fragment" -->

Für jeden Client mindestens folgende Arbeit:  
<!-- .element: class="fragment" -->

1. Thread erzeugen
<!-- .element: class="fragment fade-in-then-semi-out" -->
1. Thread ausführen
<!-- .element: class="fragment fade-in-then-semi-out" -->
1. Thread garbage collecten
<!-- .element: class="fragment" -->

---
#### `Executor`
```Java
public interface Executor {

    void execute(Runnable command);
}
```
<!-- .element: class="fragment" -->

```Java
Executor executor = ...
Runnable r = () -> foo();
executor.execute(r);
```
<!-- .element: class="fragment" -->

* <!-- .element: class="fragment" --> Task - Erzeugung getrennt von Task - Ausführung
* <!-- .element: class="fragment" --> Ausführung je nach Executor - Implementierung in


1. neuen Thread
<!-- .element: class="fragment fade-in-then-semi-out" -->
1. vorhandenen Thread
<!-- .element: class="fragment fade-in-then-semi-out" -->
1. ausführendem Thread
<!-- .element: class="fragment" -->

----
#### `ExecutorService`
```Java
public interface ExecutorService extends Executor
```
<!-- .element: class="fragment" -->

wichtige Methoden:
<!-- .element: class="fragment" -->
<dl>
    <div class="fragment fade-in-then-semi-out">
        <dt><code>Future&lt?> submit(Runnable task)</code></dt>
        <dd>Übernimmt den Task zur Ausführung</dd>
    </div>
    <div class="fragment">
        <dt><code>Future&ltT> submit(Callable&ltT> task)</code></dt>
        <dd>Übernimmt den Task zur Ausführung</dd>
    </div>
</dl>

----
#### `Future`
```Java
public interface Future<V> {

    boolean cancel(boolean mayInterruptIfRunning);
    boolean isCancelled();
    boolean isDone();
    V get() throws InterruptedException, ExecutionException;
    V get(long timeout, TimeUnit unit)
        throws InterruptedException, ExecutionException, TimeoutException;
}
```
<!-- .element: class="stretch fragment" -->

<dl class="fragment">
    <dt><code>V get()</code></dt>
    <dd>
    blockt, bis zugehörige <code>call()</code> / <code>run()</code> fertig
    liefert das Ergebnis von <code>call()</code> / <code>null</code></dd>
</dl>

----
#### Shutdown
<dl>
    <div class="fragment fade-in-then-semi-out">
        <dt><code>void shutdown()</code></dt>
        <dd>initiiert Shutdown; alte Tasks werden abgearbeitet, aber keine neuen akzeptiert</dd>
    </div>
    <div class="fragment">
        <dt><code>boolean awaitTermination(timeout)</code></dt>
        <dd>blockt nach <code>shutdown()</code> bis alle Tasks</dd>
        <dd>fertig <span class="monospace">-></span> <code>true</code> oder timeout <span class="monospace">-></span> <code>false</code></dd>
    </div>
</dl>

notes: `finalize()` tut nichts!

----
#### Ablauf
```Java
interface ArchiveSearcher { 
    String search(String target); 
}

ExecutorService executor = ...
ArchiveSearcher searcher = ...
```
<!-- .element: class="stretch fragment" -->

```Java
void showSearch(String target) throws InterruptedException {
    Callable<String> task = () -> searcher.search(target);
    Future<String> future = executor.submit(task);
    doOtherThings(); 
    try {
        displayText(future.get()); // Benutzen des Futures
    } catch (ExecutionException ex) { 
        cleanup(); 
    }
}
```
<!-- .element: class="stretch fragment" -->

```Java
executor.shutdown();
executor.awaitTermination(1, TimeUnit.SECONDS);
System.out.println("Work finished, you can close the program now");
```
<!-- .element: class="stretch fragment" -->

----
#### `Executors`
```Java
ExecutorService executor = Executors.newSingleThreadExecutor();
```
<!-- .element: class="fragment" -->

<p class="fragment">wichtige Methoden (returnen <code>ExecutorService</code>)</p>

<dl>
    <div class="fragment fade-in-then-semi-out">
        <dt><code>newSingleThreadExecutor()</code></dt>
        <dd>Executor mit <strong>einem</strong> Thread</dd>
        <dd>Bei Exceptions wird ein neuer erzeugt</dd>
    </div>
    <div class="fragment fade-in-then-semi-out">
        <dt><code>newFixedThreadPool(int nThreads)</code></dt>
        <dd>Executor mit <strong>n</strong> Threads</dd>
    </div>
    <div class="fragment">
        <dt><code>newCachedThreadPool()</code></dt>
        <dd>Erzeugt bei Bedarf neue Threads</dd>
        <dd>Unbenutzte Threads werden nach 60s gelöscht</dd>
    </div>
</dl>

---
#### Lock
```Java
public interface Lock {

    void lock();
    void lockInterruptibly() throws InterruptedException;
    boolean tryLock();
    boolean tryLock(long time, TimeUnit unit) throws InterruptedException;
    void unlock();
    Condition newCondition();
}
```
<!-- .element: class="fragment stretch" -->

<dl>
    <div class="fragment fade-in-then-semi-out">
        <dt><code>lockInterruptibly</code></dt>
        <dd>wie <code>lock()</code>, aber interruptbar <span class="monospace">-></span> <code>InterruptedException</code></dd>
    </div>
    <div class="fragment">
        <dt><code>tryLock</code></dt>
        <dd><strong>blockt nicht</strong></dd>
        <dd>returnt ob Lock erhalten wurde</dd>
    </div>
</dl>

---
```Java
static int count = 0;

public static void main(String[] args) throws InterruptedException {
    ExecutorService executor = Executors.newFixedThreadPool(10);
    for (int i = 0; i < 10_000; i++)
        executor.submit(ReentrantLockDemo::increment);
    executor.shutdown();
    executor.awaitTermination(1, TimeUnit.DAYS);
    System.out.println(count);
}

static void increment() {
    count++;
}
```
<!-- .element: class="fragment stretch" -->

notes: Mehrere Threads greifen auf `count` zu. Synchronisieren, locken oder atomic

----

```Java
private static final String lock = "Just for locking";

static void increment() {
    synchronized (lock) {
        count++;
    }
}
```
<!-- .element: class="fragment" -->

```Java
static Lock lock = new ReentrantLock();

static void increment() {
    lock.lock();
    try {
        count++;
    } finally {
        lock.unlock();
    }
}
```
<!-- .element: class="fragment" -->

---
#### `ReentrantLock`
```Java
ExecutorService executor = Executors.newFixedThreadPool(2);
ReentrantLock lock = new ReentrantLock();
```
<!-- .element: class="fragment stretch" -->

```Java
executor.submit(() -> {
    lock.lock();
    try {
        sleep(1);
    } finally {
        lock.unlock();
    }});
```
<!-- .element: class="fragment stretch" -->

```Java
executor.submit(() -> {
    System.out.println("Locked: " + lock.isLocked());
    System.out.println("Held by me: " + lock.isHeldByCurrentThread());
    boolean locked = lock.tryLock();
    System.out.println("Lock acquired: " + locked);
});
executor.shutdown();
```
<!-- .element: class="fragment stretch" -->

```plain
Locked: true
Held by me: false
Lock acquired: false
```
<!-- .element: class="fragment stretch" -->

----
#### `ReadWriteLock`
Stellt zwei `Lock`-Instanzen zur Verfügung
<!-- .element: class="fragment" --> 


<dl>
    <div class="fragment">
    <dt><code>readLock</code></dt>
    <dd>Kann von beliebig vielen Threads gehalten werden</dd>
    </div>
    <div class="fragment">
        <dt><code>writeLock</code></dt>
        <dd class="fragment fade-in-then-semi-out">Kann nur von <strong>einem</strong> Thread gehalten werden</dd>
        <dd class="fragment fade-in-then-semi-out">Blockt bis laufende Lesevorgänge beendet sind</dd>
        <dd class="fragment">Während das <code>writeLock</code> gehalten wird, werden keine <code>readLocks</code> vergeben</dd>
    </div>
</dl>

----
```Java
ExecutorService executor = Executors.newFixedThreadPool(3);
Map<String, String> map = new HashMap<>();
ReadWriteLock lock = new ReentrantReadWriteLock();
```
<!-- .element: class="fragment" --> 

```Java
executor.submit(() -> {
    lock.writeLock().lock();
    try {
        sleep(1);
        map.put("foo", "bar");
    } finally {
        lock.writeLock().unlock();
}});
```
<!-- .element: class="fragment" --> 

```Java
Runnable readTask = () -> {
    lock.readLock().lock();
    try {
        System.out.println(map.get("foo"));
    } finally {
        lock.readLock().unlock();
}};
```
<!-- .element: class="fragment" --> 

```Java
executor.submit(readTask);
executor.submit(readTask);      // beide readTasks printen
executor.shutdown();
```
<!-- .element: class="fragment" --> 

notes: Garantierter Output? nein! Wenn einer der `readTask`s schneller ist, kommt `null`

---
#### Semaphore
Ein *Semaphore* schützt einen kritischen Bereich, den maximal *n* Threads gleichzeitig betreten dürfen.  
<!-- .element: class="fragment" --> 
Ein *Lock* / *Monitor* ist ein binärer Semaphore.
<!-- .element: class="fragment" --> 

----
```Java
ExecutorService executor = Executors.newFixedThreadPool(5);
Semaphore semaphore = new Semaphore(5); // 5 Pissoirs
```
<!-- .element: class="fragment" --> 

```Java
Runnable goToToilet = () -> {
    try {
        semaphore.acquire();
        System.out.println("Semaphore acquired");
        pee();
    } catch (InterruptedException e) {
        e.printStackTrace();
    } finally {
        semaphore.release();
}};
```
<!-- .element: class="fragment" --> 

---
#### Condition
```Java
synchronized(elevator) {
    while(burning)
        elevator.wait();
}
```
<!-- .element: class="fragment" --> 

```Java
synchronized(elevator) {
    extinguishFire();
    elevator.notifyAll();
}
```
<!-- .element: class="fragment" --> 

----

```Java
Lock lock = new ReentrantLock();
Condition fireExtinguished = lock.newCondition();
```
<!-- .element: class="fragment" --> 

```Java
lock.lock();        // synchronized {
try {
    while(burning)
        fireExtinguished.await();   // elevator.wait()
} finally {
    lock.unlock();  // synchronized }
}
```
<!-- .element: class="fragment" --> 

```Java
lock.lock();        // synchronized {
try {
    extinguishFire();
    fireExtinguished.signalAll();   // elevator.notify()
} finally {
    lock.unlock();  // synchronized }
}
```
<!-- .element: class="fragment" --> 

Jede Condition hat eine Referenz auf das dazugehörige Lock
<!-- .element: class="fragment" --> 

---
#### Fork / Join
<ul>
    <li class="fragment fade-in-then-semi-out">Implementierung von <code>ExecutorService</code></li>
    <li class="fragment fade-in-then-semi-out">Task wird rekursiv in Subtasks aufgesplittet (<em>fork</em>)</li>
    <li class="fragment fade-in-then-semi-out">Ist die Workload klein genug, wird die Task abgearbeitet</li>
    <li class="fragment">Dann werden die Teilergebnisse vereinigt (<em>join</em>)</li>
</ul>

----
#### Ablauf
```Java
public class MyRecursiveTask extends RecursiveTask<ReturnType> {

    @Override
    protected ReturnType compute() {
        if (my portion of the work is small enough)
          doTheWork();
        else {
          split my work into 2+ pieces
          fork most pieces
          invoke one
          combine results
        }
    }
}
```
<!-- .element: class="fragment" --> 
`RecursiveAction` falls kein Returnwert nötig
<!-- .element: class="fragment" --> 

----
```Java
public class NullCounterTask extends RecursiveTask<Integer> {

    private final List<?> list;
    private final int threshold;

    public NullCounterTask(Collection<?> coll, int threshold) {
        this.list = new ArrayList<>(coll);
        this.threshold = threshold;
    }
```
<!-- .element: class="stretch" --> 

```Java
@Override
protected Integer compute() {
    if (sizeBelowThreshold())
        return countNulls();
    else {
        var cutIndex = list.size() / 2;
        var part1 = list.subList(0, cutIndex);
        var part2 = list.subList(cutIndex, list.size());
        var forkedTask = new NullCounterTask(part1, threshold).fork();
        int nullsInPart2 = new NullCounterTask(part2, threshold).invoke();
        return forkedTask.get() + nullsInPart2;
    }
}
```
<!-- .element: class="stretch" --> 

note: Fehlerbehandlung weggelassen

---
#### Mehr
* `CyclicBarrier`
* `Phaser`
* `CountDownLatch`
* `Concurrent Collections`