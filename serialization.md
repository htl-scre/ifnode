# Serialisierung

---
#### Motivation

```Java
public class Student {

    private int id;
    private String name;
    private School school;
    private Address address;
    
    public Student(byte[]) { ... }
    
    public byte[] serialize() { ... }
```
<!-- .element: class="fragment stretch" -->

----
Implementierung
```Java
public byte[] serialize() throws IOException {
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
         DataOutputStream output = new DataOutputStream(baos)) {
        output.writeInt(id);
        output.writeUTF(name);
        output.write(school.serialize());
        output.write(address.serialize());
        return baos.toByteArray();
    }
}
```
<!-- .element: class="fragment" -->

```Java
public Student(byte[] data) throws IOException {
    try (DataInputStream dis = new DataInputStream(
            new ByteArrayInputStream(data))) {
        id = dis.readInt();
        name = dis.readUTF();
        school = new School(dis.readNBytes(???));
        address = new Address(dis.readNBytes(???));
    }
}
```
<!-- .element: class="fragment" -->

note: Mögliche Lösung: zu jedem Objekt vorher Byteanzahl schreiben

---
#### `ObjectOutputStream`
```Java
School htl = new School("HTL");
Student student = new Student(42, "Bob", htl);

try (ObjectOutputStream output = new ObjectOutputStream(
        new FileOutputStream("student.dat"))) {
    output.writeObject(student);
}
```
<!-- .element: class="fragment" -->

```Plain Text
Exception in thread "main" java.io.NotSerializableException: 
        serializing.domain.Student
```
<!-- .element: class="fragment exception" -->

----
#### `Serializable`
```Java
public interface Serializable { }
```
<!-- .element: class="fragment" -->
*Marker-Interface*, markiert Klasse als serialisierbar
<!-- .element: class="fragment" -->

```Java
public class Student implements Serializable
```
<!-- .element: class="fragment" -->

```Java
School htl = new School("HTL");
Student student = new Student(42, "Bob", htl);

try (ObjectOutputStream output = new ObjectOutputStream(
        new FileOutputStream("student.dat"))) {
    output.writeObject(student);
}
```
<!-- .element: class="fragment" -->

```Plain Text
Exception in thread "main" java.io.NotSerializableException: 
        serializing.domain.School
```
<!-- .element: class="fragment exception" -->

----
```Java
public class School implements Serializable
```
<!-- .element: class="fragment" -->

```Java
School htl = new School("HTL");
Student student = new Student(42, "Bob", htl);

try (ObjectOutputStream output = new ObjectOutputStream(
        new FileOutputStream("student.dat"))) {
    output.writeObject(student);
}
```
<!-- .element: class="fragment" -->

```Java
try (ObjectInputStream input = new ObjectInputStream(
        new FileInputStream("student.dat"))) {
    Student deserialized = (Student) input.readObject();
}
```
<!-- .element: class="fragment" -->

---
## Customization

---
#### `transient`
```Java
public class Student implements Serializable {

    private int id;
    private String name;
    private transient List&lt;Grade> grades;
    ...
       
}
```
<!-- .element: class="fragment" -->

Beim Serialisieren nicht berücksichtigt:
<!-- .element: class="fragment" -->
<ul>
    <li class="fragment">statische Felder</li>
    <li class="fragment">transiente Felder</li>
</ul>

---
#### `writeObject`/`readObject`
```Java
public class Student implements Serializable {

    private int id;
    private String name;
    private School school;
    
    @Serial
    private void writeObject(ObjectOutputStream output)
            throws IOException {
        output.writeByte(42);
        output.writeUTF("Chuck Norris");
    }

    @Serial
    private void readObject(ObjectInputStream input)
            throws IOException, ClassNotFoundException {
        id = input.readByte();
        name = input.readUTF();
    }
}
```
<!-- .element: class="fragment stretch" -->

----
```Java
School htl = new School("HTL");
Student student = new Student(17, "Bob", htl);

try (ObjectOutputStream output = new ObjectOutputStream(
        new FileOutputStream("student.dat"))) {
    output.writeObject(student);
}

try (ObjectInputStream input = new ObjectInputStream(
        new FileInputStream("student.dat"))) {
    Student deserialized = (Student) input.readObject();
}
```
<!-- .element: class="fragment" -->

```Plain Text
Student{id=42, name='Chuck Norris', school=null}
```
<!-- .element: class="fragment" -->

komplette Kontrolle über Serialisierung
<!-- .element: class="fragment" -->

---
#### `serialVersionUID`
<ul>
    <li class="fragment">Objekt wird serialisiert</li>
    <li class="fragment">Klasse wird verändert</li>
    <li class="fragment">Objekt wird deserialisiert
        <ul>
            <li class="fragment">✔️ wenn <code>serialVersionUID</code> ident</li>
            <li class="fragment">💥   sonst</li>
        </ul>
    </li>
    <li class="fragment"><code>serialVersionUID</code> wird bei Bedarf vom Compiler automatisch generiert 👍</li>
</ul>

```Java
public class Student implements Serializable {

    @Serial
    private static final long serialVersionUID = 42L;
    ...
```
<!-- .element: class="fragment" -->

note: 
* Breaking change -> UID ändern
* Gefahr des Vergessens