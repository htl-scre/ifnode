<style>
.bordered {
    text-align:left;
}
</style>

#### HTML
* keine Programmiersprache
<!-- .element: class="fragment fade-in-then-semi-out" -->
* HyperText Markup Language
<!-- .element: class="fragment" -->

---
#### Elemente
![html Element](./assets/html/tags.png)

----
#### Block/inline
* Block-Elemente beginnen eine neue Zeile
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Inline-Elemente formatieren meist Text
<!-- .element: class="fragment fade-in-then-semi-out" -->

```html
<em>HTL<strong>St.</strong>Pölten</em>
<p>Abteilung</p><p>Informatik</p><p>2XHIF</p>
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs plain" data-trim data-noescape>
<em>HTL<strong>St.</strong>Pölten</em>
Abteilung
Informatik
2XHIF
</code></pre>

----
#### Leere Elemente
* besitzen kein closing-Tag
<!-- .element: class="fragment fade-in-then-semi-out" -->
* fügen meist Content ein
<!-- .element: class="fragment fade-in-then-semi-out" -->

<div class="fragment">
<pre><code class="hljs html" data-trim>
<img src="https://www.htlstp.ac.at/logo">
</code></pre>
<img src="https://www.htlstp.ac.at/logo" alt="logo htl stp">
</div>

---
#### Attribute
![attribute](./assets/html/attributes.png)

```html
<tag attr1="val1" attr2="val2 val3" attr3='val4'>content</tag>
```
<!-- .element: class="fragment" -->

```html
<a title="HTL St. Pölten"
    href="https://www.htlstp.ac.at/" 
    target="_blank">HTL</a>
```
<!-- .element: class="fragment" -->

<a title="HTL St. Pölten"
    href="https://www.htlstp.ac.at/" 
    target="_blank">HTL</a>
<!-- .element: class="fragment" -->

---
#### Hello World
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Tooltiptext, Bookmarktext</title>
  </head>
  <body>
    <p>Hello World</p>
  </body>
</html>
```

---
## `<head>`

----
#### `<title>/<hx>`
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Title</title>
  </head>
  <body>
    <h1>Heading</h1>
    <h2>Subheading</h2>
  </body>
</html>
```

----
#### `<meta>`
```html
<head>
    <meta charset="UTF-8">
    <meta name="author" content="Christoph Schreiber">
    <meta name="description" 
        content="Diese Slides sollen 
                 den Unterricht vereinfachen.">
</head>
```

<dl>
    <div class="fragment">
        <dt><code>charset</code></dt>
        <dd>definiert den verwendeten Zeichensatz</dd>
    </div>
    <div class="fragment">
        <dt><code>description</code></dt>
        <dd>dient der Search Engine Optimization (SEO)</dd>
    </div>
</dl>

note: orf googlen und Google-Resultat mit description vergleichen

----
#### Icon
```html
<head>
    <link rel="shortcut icon" 
        href="my-image.png" 
        type="image/x-icon">
</head>
```
<!-- .element: class="stretch" -->

* Icon für Browsertab/Bookmarks

----
#### CSS/JavaScript

<pre class="fragment"><code class="hljs html" data-trim>
<head>
    <link rel="stylesheet" href="my-css-file.css">
    <script src="my-js-file.js" defer></script>
</head>
</code></pre>

<dl>
    <div class="fragment">
        <dt><code>defer</code></dt>
        <dd>Script wird erst <strong>nach</strong> dem Laden des Contents ausgeführt</dd>
    </div>
</dl>

---
#### `<p>` - Paragraph
* Block
<!-- .element: class="fragment fade-in-then-semi-out" -->

```html
<p>eins</p>
<p>zwei</p>
```
<!-- .element: class="fragment" -->
<div class="fragment">
    <p class="bordered">eins</p>
    <p class="bordered">zwei</p>
</div>

----
#### `<hX>` - Heading
* Block
<!-- .element: class="fragment fade-in-then-semi-out" -->

```html
<h1>h1</h1>
<h3>h3</h3>
<h5>h5</h5>
```
<!-- .element: class="fragment" -->
<div class="fragment">
    <h1 class="bordered" style="text-transform: none">h1</h1>
    <h3 class="bordered" style="text-transform: none">h3</h3>
    <h5 class="bordered" style="text-transform: none">h5</h5>
</div>

----
#### `<em> / <strong>`
<ul>
    <li class="fragment fade-in-then-semi-out">Akzentuieren einzelner Textteile</li>
    <li class="fragment fade-in-then-semi-out"><em>em</em> / <strong>strong</strong></li>
    <li class="fragment fade-in-then-semi-out">Inline</li>
</ul>

```html
<p>
    if <em>htl</em> then <strong>if</strong>
</p>
```
<!-- .element: class="fragment" -->
<p class="fragment">if <em>htl</em> then <strong>if</strong></p>

----
#### `<span>` 
* Akzentuieren einzelner Textteile mit CSS
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Inline
<!-- .element: class="fragment fade-in-then-semi-out" -->

```html
<p>
    span <span style="color: darkred">is worthless</span> without css
</p>
```
<!-- .element: class="fragment stretch" -->
<p class="fragment">
    span <span style="color: darkred">is worthless</span> without css
</p>

----
#### `<br>, <hr>`
```html
So gehts nicht weiter <br>
Ganz <br>
sicher
<hr>
nicht   
```

So gehts nicht weiter <br>
Ganz <br>
sicher
<hr>
nicht  

---
#### `<ul>` - Unordered List
```html
<ul>
    <li>ListItem</li>
    <li>ListItem</li>
    <li>ListItem</li>
</ul>
```
<!-- .element: class="fragment" -->

<ul class="fragment">
    <li>ListItem</li>
    <li>ListItem</li>
    <li>ListItem</li>
</ul>

----
#### `<ol>` - Ordered List
```html
<ol>
    <li>ListItem</li>
    <li>ListItem</li>
    <li>ListItem</li>
</ol>
```
<!-- .element: class="fragment" -->

<ol class="fragment">
    <li>ListItem</li>
    <li>ListItem</li>
    <li>ListItem</li>
</ol>

----
#### `<dl>` - Definition List
```html
<dl>
    <dt>DefinitionTerm</dt>
    <dd>DefinitionDescription</dd>
    <dt>DefinitionTerm</dt>
    <dd>DefinitionDescription</dd>
</dl>
```
<!-- .element: class="fragment" -->

<dl class="fragment">
    <dt>DefinitionTerm</dt>
    <dd>DefinitionDescription</dd>
    <dt>DefinitionTerm</dt>
    <dd>DefinitionDescription</dd>
</dl>

---
#### `<a>` - Anchor/Link
```html
<p>Would you like to know 
    <a target = "_blank" href="https://www.htlstp.ac.at">more</a></p>
```
<!-- .element: class="fragment stretch" -->

<p class="fragment">Would you like to know 
    <a target = "_blank" href="https://www.htlstp.ac.at">more</a></p>

<dl>
    <div class="fragment fade-in-then-semi-out">
        <dt><code>href</code></dt>
        <dd>HypertextReference - Ziel des Links z.B.:
            <ul class="monospace">
                <li>https://www.htlstp.ac.at</li>
                <li>mailto:christoph.schreiber@htlstp.ac.at</li>
                <li>tel:0123456789</li>
            </ul>
        </dd>
    </div>
    <div class="fragment">
        <dt><code>target</code></dt>
        <dd>ohne <span class="monospace">-></span> Link wird in aktuellem Tab/Fenster geöffnet</dd>
        <dd><span class="monospace">_blank -></span> Link wird in neuem Tab/Fenster geöffnet</dd>
    </div>
</dl>

---
#### `<img>`
```html
<img src="https://www.htlstp.ac.at/logo" alt="logo htl stp">
```
<!-- .element: class="fragment" -->

<dl>
    <div class="fragment fade-in-then-semi-out">
        <dt><code>src</code></dt>
        <dd>Quelle des Bildes</dd>
    </div>
    <div class="fragment">
        <dt><code>alt</code></dt>
        <dd>Alternativtext</dd>
    </div>
</dl>