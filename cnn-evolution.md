# CNN Evolution

![](./assets/cnn-evolution/cnn.webp)

---

#### [LeNet - 1998](https://proceedings.neurips.cc/paper/1989/file/53c3bce66e43be4f209556518c2fcb54-Paper.pdf)

![](./assets/cnn-evolution/lenet.svg)
<!-- .element: class="fragment fade-in" -->

* Convolution
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Average Pooling(kernel_size=2, stride=2)
<!-- .element: class="fragment fade-in" -->
* Dense
<!-- .element: class="fragment fade-in-then-semi-out" -->

----
![](./assets/cnn-evolution/features.png)
<!-- .element: class="fragment fade-in" -->

* Dimensionen werden kleiner
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Feature maps mehr
<!-- .element: class="fragment fade-in" -->


---

#### [AlexNet - 2012](https://papers.nips.cc/paper_files/paper/2012/file/c399862d3b9d6b76c8436e924a68c45b-Paper.pdf)

![](./assets/cnn-evolution/alexnet.svg)
<!-- .element: class="fragment fade-in" style="height:40vh" -->

* Convolution(stride)
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Max Pooling
<!-- .element: class="fragment fade-in-then-semi-out" -->
* tanh -> ReLU
<!-- .element: class="fragment fade-in" -->

----

#### Padding

![](./assets/cnn-evolution/padding.svg)
<!-- .element: class="fragment fade-in" -->

```Python
nn.Conv2d(kernel_size=2, stride=1, padding=1)
```

<!-- .element: class="fragment fade-in" -->

hält Dimensionen konstant
<!-- .element: class="fragment fade-in" -->

---

#### [VGGNet, 2014](https://arxiv.org/pdf/1409.1556)

![](./assets/cnn-evolution/vgg.svg)
<!-- .element: class="fragment fade-in" style="height:40vh" -->

* Blocks
<!-- .element: class="fragment fade-in-then-semi-out" -->
* $3 \times 3$ Convolution
<!-- .element: class="fragment fade-in" -->

----

#### `kernel_size--`

```Python
nn.Conv2d(kernel_size=5, stride=1, padding=0)
```

<!-- .element: class="fragment fade-in" -->
$5\times5 \rightarrow Conv_{5\times5} ⟹ 1\times1$
<!-- .element: class="fragment fade-in" -->
$5 \times 5 = 25$ Parameter
<!-- .element: class="fragment fade-in" -->

```Python
nn.Conv2d(kernel_size=3, stride=1, padding=0)
nn.Conv2d(kernel_size=3, stride=1, padding=0)
```

<!-- .element: class="fragment fade-in" -->
$5\times5 \rightarrow Conv_{3\times3} ⟹ 3\times3$
<!-- .element: class="fragment fade-in" -->
$3\times3 \rightarrow Conv_{3\times3} ⟹ 1\times1$
<!-- .element: class="fragment fade-in" -->
$3 \times 3 + 3 \times 3 = 18$ Parameter
<!-- .element: class="fragment fade-in" -->

---

#### [NiN, 2014](https://arxiv.org/pdf/1312.4400)

![](./assets/cnn-evolution/nin.svg)
<!-- .element: class="stretch fragment fade-in" -->

----

#### NiN Block

![](./assets/cnn-evolution/nin-block.png)
<!-- .element: class="fragment fade-in" style="height:25vh" -->

![](./assets/cnn-evolution/nin-block.webp)
<!-- .element: class="fragment fade-in" style="height:25vh"-->

1x1 Convs fungieren als MLP
<!-- .element: class="fade-in fragment" -->

----

#### Fully Convoluted

![](./assets/cnn-evolution/gap.webp)
<!-- .element: class="fade-in fragment" -->

```
nin_block(num_classes, kernel_size=3, strides=1, padding=1),
nn.AdaptiveAvgPool2d((1, 1)) # output 1x1 je feature map
```

<!-- .element: class="fade-in fragment" -->

---
<h4><a href="https://arxiv.org/pdf/1512.03385"><code>GoogLeNet, 2014</code></a></h4>

![](./assets/cnn-evolution/google-net.svg)
<!-- .element: class="fade-in fragment" -->

welche Convolution? ja!
<!-- .element: class="fade-in fragment" -->

----

#### Inception Block

![](./assets/cnn-evolution/inception.svg)
<!-- .element: class="fade-in fragment" -->
alle Channel werden concatenated
<!-- .element: class="fade-in fragment" -->

---

#### [ResNet, 2015](https://arxiv.org/pdf/1512.03385)

![](./assets/cnn-evolution/resnet.webp)
<!-- .element: class="fade-in fragment" -->

----

#### Deep Learning

> When deeper networks are able to start converging, a
> degradation problem has been exposed: with the network
> depth increasing, accuracy gets saturated and then degrades rapidly.  
> Unexpectedly, such degradation is not caused by overfitting, and adding
> more layers to a suitably deep model leads to higher training error.  
<!-- .element: class="fade-in fragment stretch" -->

----

#### Warum?

![](./assets/cnn-evolution/acc-saturation.png)
<!-- .element: class="fade-in fragment" data-fragment-index="0"-->

<dl>
    <dt class="fragment fade-in" data-fragment-index="1">Vanishing/Exploding gradients</dt>
    <dd class="fragment fade-in" data-fragment-index="1">Gradients -> 0/∞</dd>
    <dd class="fragment fade-in">✅ ReLU</dd>
    <dd class="fragment fade-in">✅ BatchNorm</dd>
</dl>

----

#### Residual Connections

![](./assets/cnn-evolution/residual-block.svg)
<!-- .element: class="fade-in fragment" -->

![](./assets/cnn-evolution/block.png)
<!-- .element:  class="fragment" style="height:20vh"-->

<!-- .element:  class="fragment fade-in"-->

note: residual, weil g(x) = f(x) - x zu lernen ist

----
* $L = f * g$
* $\frac{\partial L}{\partial f} = g = 42$
* $f = a + b$
* $\frac{\partial f}{\partial a} = 1$ *local gradient*
  $\implies \frac{\partial L}{\partial a} = \frac{\partial L}{\partial f} \cdot \frac{\partial f}{\partial a} = 42 \cdot 1 = 42$ *global gradient*  
  $\implies \frac{\partial L}{\partial b} = 42$  
  $\implies + $ verteilt den upstream gradient

----
#### ‐‐‐‐‐‐‐‐‐‐‐

![](./assets/cnn-evolution/residual-block-conv.svg)
<!-- .element:  class="fragment fade-in"-->
1x1 Conv falls Channels nicht passen
<!-- .element:  class="fragment fade-in"-->

----
![](./assets/cnn-evolution/resnet-performance.png)
<!-- .element:  class="fragment fade-in"-->

Effekt erst tiefen Netzwerken
<!-- .element:  class="fragment fade-in"-->

----
#### [The connectome of an insect brain, 2023](https://www.science.org/doi/10.1126/science.add9330)

![](./assets/cnn-evolution/feedforward-pathways.png)
<!-- .element:  class="r-stretch"-->

---

#### [Vision Transformer, 2021](https://arxiv.org/pdf/2010.11929)

![](./assets/cnn-evolution/vit.png)
<!-- .element:  class="fragment fade-in"-->

`[class]` enthält generelle Daten
<!-- .element:  class="fragment fade-in"-->

---

#### Transfer Learning

```Elixir
def entries(todo_list, date) do
  todo_list.entries
  |> Map.values()
  |> Enum.filter(fn entry -> entry.date == date end)
end
```

<!-- .element:  class="fragment fade-in-then-semi-out"-->

* Vorhandenes Wissen neu anwenden

<!-- .element:  class="fragment fade-in-then-semi-out"-->

* Implementierung?

<!-- .element:  class="fragment fade-in"-->

----
![](./assets/cnn-evolution/transfer-learning.ppm)

----

```python
model = resnet34()
```

<!-- .element:  class="fragment fade-in stretch"-->

```text
ResNet(
  (conv1): Conv2d(3, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
  (bn1): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
  (relu): ReLU(inplace=True)
  ...
  (avgpool): AdaptiveAvgPool2d(output_size=(1, 1))
  (fc): Linear(in_features=512, out_features=1000, bias=True)
)
```

<!-- .element:  class="fragment fade-in stretch"-->

```
for param in model.parameters():
    param.requires_grad = False

model.fc = nn.Sequential([...])

optimizer = Adam(model.fc.parameters(), lr=1e-3)
```

<!-- .element:  class="fragment fade-in stretch"-->