![javascript-logo](./assets/javascript/logo.jpg)

---
#### Features
* ursprünglich für interaktive Websites entwickelt
  <!-- .element: class="fragment fade-in-then-semi-out" -->
* Browser besitzen JS-Engine, welche Script ausführt
  <!-- .element: class="fragment fade-in-then-semi-out" -->
* Node.js ⟹ JS ohne Browser ⟹ Server
  <!-- .element: class="fragment fade-in-then-semi-out" -->
* React Native ⟹ JS mobile Apps
  <!-- .element: class="fragment fade-in-then-semi-out" -->
* Electron ⟹ JS Desktopapps
  <!-- .element: class="fragment fade-in-then-semi-out" -->
* JS vor 2015 << JS2015+
  <!-- .element: class="fragment" -->

----
#### Engine
![javascript-engine](./assets/javascript/js-engine-pipeline.svg)

----
#### node.js
![node.js-engine](./assets/javascript/node-js.jpg)

---
#### `use strict`
```Javascript
'use strict';
"use strict";
```
  <!-- .element: class="fragment" -->

<ul>
    <li class="fragment fade-in-then-semi-out">sollte die erste Zeile fast jeder .js-Datei sein</li>
    <li class="fragment fade-in-then-semi-out">zwingt die Engine zu modernen Modus</li>
    <li class="fragment fade-in-then-semi-out">hilft, blöde Fehler zu vermeiden</li>
    <li class="fragment"><code>module</code>- und <code>class</code>-Deklarationen implizieren <code>'use strict'</code></li>
</ul>

---
#### Variablen
<dl>
    <dt class="fragment"><code>let</code></dt>
    <dd class="fragment">Standardzuweisung, Variable veränderbar</dd>
    <dd class="fragment"><pre><code class="hljs Javascript" data-trim data-noescape>
        let x = 42;
        x = 'Code smell';
    </code></pre></dd>
    <dt class="fragment"><code>const</code></dt>
    <dd class="fragment">Variable unveränderbar, <code>final</code></dd>
    <dd class="fragment"><pre><code class="hljs Javascript" data-trim data-noescape>
        const 
        let template = `${name} seit ${year} top!`;
    </code></pre></dd>
    <dt class="fragment"><code>var</code></dt>
    <dd class="fragment">👎 alt, nicht empfohlen 👎</dd>
</dl>

---
#### Typen
<dl>
    <dt class="fragment">number</dt>
    <dd class="fragment"><pre><code class="hljs Javascript" data-trim data-noescape>
        let year = 2015;
        let pi = 3.1415926;
    </code></pre></dd>
    <dt class="fragment">string</dt>
    <dd class="fragment"><pre><code class="hljs Javascript" data-trim data-noescape>
        let name = 'JavaScript'; // "Javascript"
        let template = `${name} seit ${year} top!`;
    </code></pre></dd>
    <dt class="fragment">boolean</dt>
    <dd class="fragment"><pre><code class="hljs Javascript" data-trim data-noescape>
        let important = true;
    </code></pre></dd>
    <dt class="fragment">null</dt>
    <dd class="fragment"><pre><code class="hljs Javascript" data-trim data-noescape>
        let nothing = null;
    </code></pre></dd>
</dl>

----
<dl>
    <dt class="fragment">undefined</dt>
    <dd class="fragment"><pre><code class="hljs Javascript" data-trim data-noescape>
        let unassigned;
        console.log(unassigned); // -> undefined
    </code></pre></dd>
    <dt class="fragment">object</dt>
    <dd class="fragment"><pre><code class="hljs Javascript" data-trim data-noescape>
        let webDev = {
            content: 'html',
            style: 'css',
            action: 'js',
            since: 1995,
            oop: true
        };
    </code></pre></dd>
</dl>

---
#### Typumwandlung
```Javascript
Number(null)        -> 0
Number(undefined)   -> NaN
Number(true)        -> 1
Number(false)       -> 0
Number("")          -> 0
Number("text")      -> NaN
Number("3.1415")    -> 3.1415
"2" / "3"           -> 0.6666666666666666
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

Mathematische Operationen konvertieren implizit
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Javascript
Boolean(0, '', null, undefined, NaN) -> false
Boolean(others) -> true
```
<!-- .element: class="fragment" -->

---
#### Operationen
wie Java/C
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Javascript
2 ** 3  -> 8

let n = 5;
n %= 2  -> 1
n++     -> 2
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Javascript
0 == false // true
0 === false // false
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

`===` checkt ohne Typumwandlung 👍
<!-- .element: class="fragment" -->

---
#### `if`/`else`/`?`
wie Java/C
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Javascript
if (truthy) {
    //
} else if (!something && other || thing){
    
} else {
    
}

let even = n % 2 == 0 ? true : false;
```
<!-- .element: class="fragment" -->

----
#### `??`

```Javascript
let result;
if (optional != null && optional != undefined)
    result = optional;
else
    result = defaultValue;
```
<!-- .element: class="fragment" -->

```Javascript
let result = optional ?? defaultValue;
```
<!-- .element: class="fragment" -->

```Javascript
let name = lastName ?? firstName ?? nickName ?? "Anonymous";
```
<!-- .element: class="fragment" -->

----
#### switch/case
wie oldschool Java/C
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Javascript
let result;
switch(grade) {
    case 5:
        result = 'Nicht genügend';
        break;
    case 4:
        result = 'Genügend';
        break;
    case 3:
        result = 'Befriedigend';
    // ...    
    default:
        throw new RangeError('Invalid grade: ' + grade);
}
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

⚠️ Fallthrough ⚠️
<!-- .element: class="fragment" -->

---
#### Schleifen
```Javascript
while (n) {
    console.log(n--);
}
```
<!-- .element: class="fragment fade-in-then-semi-out" -->
```Javascript
for (let i = 0; i < n; i++) {
    console.log(i);
}
```
<!-- .element: class="fragment" -->

---
#### Funktionen
```Javascript
function lengthUntilFirstLineEnding(string, ending = '\n') {
    for(let i=0; i < string.length; i++)
        if (string.substring(i, i + ending.length) === ending)
            return i;
    return Infinity;
}
```
<!-- .element: class="fragment" -->

----
#### Callbacks
Parameter einer Funktion können Funktionen sein
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Javascript
function ifPresent(object, consumer) {
    if (object)
        consumer(object);
}
```
<!-- .element: class="fragment" -->

```Javascript
function log(object) {
    console.log(object);
}
```
<!-- .element: class="fragment" -->

```Javascript
ifPresent(42, log);
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Javascript
ifPresent({name: 'callback'},
        function (object) {
        console.log(object);
    });
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Javascript
ifPresent({name: 'callback'},
        object => console.log('Present: ', object));
```
<!-- .element: class="fragment" -->

---
#### OOP
Initialisierung wie dictionary in Python
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Javascript
let person = {   
    name: 'Bob', 
    age: 42       
};
person.profession = 'Developer';
```
<!-- .element: class="fragment" -->

```Javascript
person.profession -> 'Developer'
person['age'] -> 42
```
<!-- .element: class="fragment" -->

```Javascript
delete person.name
person.name -> undefined
```
<!-- .element: class="fragment" -->

----
#### Konstruktoren/Factory-Methoden
```Javascript
function createUser(name, email) {
    return {
        name,   // name: name
        email,  // email: email
        role: 'user'
    };
}
```
<!-- .element: class="fragment" -->

```Javascript
function User(name, email, role='user') {
    this.name = name;
    this.email = email;
    this.role = role;
}

new User("Jeff", "jeff@htlstp.at");
new User("Cäsar", "cesar@htlstp.at", 'admin');
```
<!-- .element: class="fragment" -->

----
#### Methoden
Werden wie Felder definiert
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Javascript
let calculator = {
    left: 5,
    right: 3,
    add() {
        return this.left + this.right;
    },
    divide() {
        if (this.right === 0)
            throw new RangeError('Invalid divisor: ' + right);
        return this.left / this.right;
    }
};
```
<!-- .element: class="fragment" -->

----
#### `for in`
iteriert über member eines objects
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Javascript
let webDev = {
    content: 'html',
    style: 'css',
    action: 'js',
    since: 1995,
    oop: true,
    log() {
        console.log(this);
    }
};
```
<!-- .element: class="fragment" -->

```Javascript
for (let property in webDev)
    console.log(`${property}\t${webDev[property]}`)
```
<!-- .element: class="fragment" -->

```Plaintext
content html
style   css
action  js
...
```
<!-- .element: class="fragment" -->

---
#### Arrays
Sammlung beliebiger Daten
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Javascript
array = [42, obj => console.log(obj), 3.14, 'HTL'];

array[1](array);   -> [ 42, [Function (anonymous)], 3.14, 'HTL' ]
```
<!-- .element: class="fragment" -->

```Javascript
console.log(array.pop());   -> HTL
console.log(array.length);  -> 3
```
<!-- .element: class="fragment" -->

```Javascript
array.push('new');
console.log(array); -> [ 42, [Function (anonymous)], 3.14, 'new' ]
```
<!-- .element: class="fragment" -->