![logo](./assets/graphQL/logo.png)

---
#### Motivation

```Java
public record Customer(
        String name,
        String email,
        Address address,
        List<Order> orders,
        String avatarUrl) { }
```
<!-- .element: class="fragment fade-in" -->

<dl>
    <div class="fragment fade-in">
        <dt>/home</dt>
        <dd>name, avatarUrl</dd>
    </div>
    <div class="fragment fade-in">
        <dt>/orders</dt>
        <dd>orders</dd>
    </div>
    <div class="fragment fade-in">
        <dt>/profile</dt>
        <dd>alles</dd>
    </div>
</dl>

----
#### Lösungen
<dl>
    <dt class="fragment fade-in"><code>/customers/{id}</code> lädt alles</dt>
    <dd class="fragment fade-in-then-semi-out">Extratraffic (mobile!)</dd>
    <dt class="fragment fade-in">3 Endpoints</dt>
    <dd class="fragment fade-in-then-semi-out">Skalierung 😕</dd>
    <dd class="fragment fade-in">Backend restricted Frontend</dd>
</dl>

---
#### GraphQL

```graphql
{
    customer(id: 42) {
        name
        avatarUrl
    }
}
```
<!-- .element: class="fragment fade-in" -->

* Client spezifiziert zu liefernde Daten
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Server schickt nur Benötigtes
<!-- .element: class="fragment fade-in-then-semi-out" -->
* wird zu POST-Request
<!-- .element: class="fragment fade-in-then-semi-out" -->
* 2015 von Meta veröffentlicht
<!-- .element: class="fragment fade-in" -->

----
#### `magic--`
```graphql
{
  students {
    id
    name
  }
}
```
<!-- .element: class="fragment fade-in" -->

```http request
POST http://localhost:8080/graphql
Content-Type: application/json

{
    "query": "{students {id name} }"
}
```
<!-- .element: class="fragment fade-in" -->

---
#### Schema

```graphql
type Student {
    id: String
    name : String!
    schoolClass: SchoolClass!
}

type SchoolClass {
    title: String!
    students: [Student!]!
}
```
<!-- .element: class="fragment fade-in" -->

```graphql
type Query {
  students: [Student!]!
  withClass(schoolClass: String!): [Student!]!
  student(id: String!): Student
}
```
<!-- .element: class="fragment fade-in" -->

note: 
* ! => not null
* []! => garantiert kommt Liste retour
* [!] => vielleicht Liste, aber sicher nicht null drin

----
#### Queries

```graphql
query {
    students {
        id
        name
    }
}
```
<!-- .element: class="fragment fade-in" -->

```graphql
{
    withClass(schoolClass: "5AHIF") {
        id
        name
        schoolClass{
            title
        }
    }
}
```
<!-- .element: class="fragment fade-in" -->

----
#### Mutations
```graphql
type Mutation {
    createStudent(student: StudentInput!): Student
}

input StudentInput {
    name : String!
    classTitle: String!
}
```
<!-- .element: class="fragment fade-in" -->

```graphql
mutation {
    createStudent(student: { name: "new", classTitle: "5AHIF" }) {
        id
    }
}
```
<!-- .element: class="fragment fade-in" -->

---
#### Controller
```graphql
type Query {
    students: [Student!]!
    student(id: String!): Student
}

type Mutation {
    createStudent(student: StudentInput!): Student
}
```
<!-- .element: class="fragment fade-in stretch" -->

```Java
@Controller
public class GraphQLController {

    @QueryMapping
    public List<Student> students() { ... }

    @QueryMapping
    public Student student(@Argument String id) { ... }

    @MutationMapping
    public Student createStudent(
            @Valid @Argument("student") StudentInput studentInput) { ... }
}
```
<!-- .element: class="fragment fade-in stretch" -->

---
#### Error Handling

```Java
@Component
public class ExceptionResolver extends DataFetcherExceptionResolverAdapter {

    @Override
    protected List<GraphQLError> resolveToMultipleErrors(Throwable ex, DataFetchingEnvironment env) {
        var errors = new ArrayList<GraphQLError>();
        if (ex instanceof NoSuchElementException)
            errors.add(buildGraphQLError(ErrorType.NOT_FOUND, ex, env));
        if (ex instanceof BindException)
            errors.add(buildGraphQLError(ErrorType.BAD_REQUEST, ex, env));
        return errors;
    }

    private static GraphQLError buildGraphQLError(ErrorType type, Throwable ex, DataFetchingEnvironment env) {
        return GraphqlErrorBuilder.newError()
                .errorType(type)
                .message(ex.getMessage())
                .path(env.getExecutionStepInfo().getPath())
                .location(env.getField().getSourceLocation())
                .build();
    }
}
```
<!-- .element: class="fragment fade-in stretch" -->
