<style>
#css-react:hover {
    color: dodgerblue;
}
@media only screen and (max-height: 400px) {
  li {
    float: left;
    margin: 1em;
  }
}
</style>

## Das Web

---
#### Geschichte
* Erfunden am CERN ~1990
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Idee: Informationen mit Hyperlinks verknüpfen
<!-- .element: class="fragment fade-in-then-semi-out" -->
* <!-- .element: class="fragment fade-in-then-semi-out" --> Resourcen eindeutig identifizierbar über URI/URL, z.B. https://www.htlstp.ac.at
* Übertragungsprotokoll: TCP/IP
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Kommunikationsprotokoll: HTTP
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Sprache: HTML
<!-- .element: class="fragment" -->

note: TCP/IP vom DoD, Internet ist das Übertragungsmedium
----
![key layers](./assets/the-web/Internet_Key_Layers.png)

note: 
* HyperCard - Internet auf Karteikarten, Apple
* Archie - FTP search engine
* Houdini - Desktop Programm a la Windows
* SAGE - USA Raderkoordinationssystem 1950-1980
* Mosaic - ging in Internet Explorer auf

---
![browsing](./assets/the-web/what-happens-when-you-browse.png)

---
#### HTML - Content
![html = Rohbau](./assets/the-web/html-css-js-house.jpg)
<!-- .element: class="fragment" -->

* Text
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Graphiken/Videos/Audio
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Links
<!-- .element: class="fragment" -->

----
#### CSS - Styling
<ul id="css-responsive">
    <li class="fragment fade-in-then-semi-out" style="color: dodgerblue">Farbe</li>
    <li class="fragment fade-in-then-semi-out" style="font-family: 'Comic Sans MS'">Schriftart</li>
    <li class="fragment fade-in-then-semi-out" style="background: dodgerblue;
                                border-radius: 15px;
                                padding: 0.5em 1em;
                                text-decoration: blink;">
                                Hintergrund</li>
    <li class="fragment fade-in-then-semi-out" style=":hover">Layout</li>
    <li class="fragment fade-in-then-semi-out" id="css-react">GUI Reaktionen</li>
    <li class="fragment">Responsive Design</li>
</ul>

----
#### ECMAScript - Funktionalität
<img alt="unlit bulb" id="image" 
    style="border-radius:10px; border:0px;"
    onclick="const image = document.getElementById('image'); 
    if (image.src.match('bulb-on')) 
        image.src = './assets/the-web/bulb-off.jpg'; 
    else
        image.src = './assets/the-web/bulb-on.jpg'; " src="./assets/the-web/bulb-off.jpg"> 

---
#### Dynamische Seiten
![dynamic pages](./assets/the-web/Scheme_dynamic_page.svg)