# JDBC
![JDBC](./assets/JDBC/jdbc.png)

---
#### Architektur
![Architktur](./assets/JDBC/jdbc-architecture.jpg)
<!-- .element: class="fragment" --> 

----
#### Ablauf
<ol>
    <li class="fragment fade-in-then-semi-out"><code>DriverManager</code> erzeugt <code>Connection</code></li>
    <li class="fragment fade-in-then-semi-out"><code>Connection</code> erzeugt <code>Statement</code></li>
    <li class="fragment fade-in-then-semi-out"><code>Statement</code> wird auf Db executed</li>
    <li class="fragment fade-in-then-semi-out">Db returnt <code>ResultSet</code></li>
    <li class="fragment fade-in-then-semi-out"><code>ResultSet</code> verarbeiten</li>
    <li class="fragment fade-in-then-semi-out"><code>Statement</code> closen</li>
    <li class="fragment"><code>Connection</code> closen</li>
</ol>

---
#### `getConnection`
```Java
var connection = DriverManager.getConnection("jdbc:h2:mem:");
```
<!-- .element: class="fragment fade-in-then-semi-out" -->
 
```Java
var connection = DriverManager.getConnection(
        "jdbc:postgresql://ifpostgres02:5432/db",
        "unterricht",
        "unterricht");
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
var properties = new Properties();
properties.put("user", "scre");
properties.put("password", "secure");

var connection = DriverManager.getConnection(
        "jdbc:mysql://host/db", 
        properties);
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```
var dataSource = new JdbcDataSource();
dataSource.setURL("jdbc:...");
return dataSource.getConnection("user", "pw");
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

CI: *Secret-Manager* setzt während *Build* Daten
<!-- .element: class="fragment" -->

note: `getConnection` benutzt `Reflection`, um den `ClassLoader` der Callerklasse zu finden
---
#### `Connection`
```Java
public interface Connection  extends Wrapper, AutoCloseable {

    // allgemeine Infos
    DatabaseMetaData getMetaData() throws SQLException;
    
    // Kommunikation mit Db
    Statement createStatement() throws SQLException;
    PreparedStatement prepareStatement(String sql) throws SQLException;
    CallableStatement prepareCall(String sql) throws SQLException;
    
    // Transaktionsmanagement
    void setAutoCommit(boolean autoCommit) throws SQLException;
    void commit() throws SQLException;
    void rollback() throws SQLException;
    Savepoint setSavepoint() throws SQLException;
    void rollback(Savepoint savepoint) throws SQLException;

    ...
}
```
<!-- .element: class="fragment stretch" -->

note:
* Noch viel mehr Methoden:
    * Type Forward/Backward
    * Concurrency   
    * Holdability   Cursor behalten
* Welche *Implementierung* von `getConnection` returnt wird, hängt vom `Driver` ab

---
#### `Statement`
```Java
public interface Statement extends Wrapper, AutoCloseable {

    ResultSet executeQuery(String sql) throws SQLException; // SELECT
    int executeUpdate(String sql) throws SQLException;      
         // INSERT, UPDATE, DELETE, DDL
    ...
```
<!-- .element: class="fragment stretch" -->

```Java
try (var connection = DriverManager.getConnection("jdbc:h2:mem:")) {
    var statement = connection.createStatement();
    statement.executeUpdate("""
            CREATE TABLE students(
                id INTEGER,
                first_name VARCHAR(50) NOT NULL,
                constraint students_pk
                		primary key (id));
            """);
}
```
<!-- .element: class="fragment stretch" -->

note: 
* DDL = data definition language
* auch andere Methoden, aber nicht empfohlen

----
#### Problem
```Java
public Stream<String> getStudentNamesLikeName(String name) 
        throws SQLException {
    try (var statement = connection.createStatement()) {
        var resultSet = statement.executeQuery("""
                SELECT first_name 
                FROM students 
                WHERE first_name LIKE '%%%s%%'"""
                .formatted(name));
        var builder = Stream.<String>builder();
        while (resultSet.next())
            builder.accept(resultSet.getString("first_name"));
        return builder.build();
    }
}
```
<!-- .element: class="fragment" -->

```Java
getStudentIdsWhereStudentsLikeName("fred") => [Alfred, Manfred]
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
getStudentNamesLikeName("' OR '1'='1") => [Alfred, Bob, Carl, ...]
```
<!-- .element: class="fragment" -->

⚠️ `Statement` nur ohne Parameter! ⚠️
<!-- .element: class="fragment" -->

note: Stream nicht lazy

----
#### `PreparedStatement`
Besser als `Statement` 
* bereits vorkompiliert auf Db ⟹ effizienter
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Parameter übergeben
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
var sql = "INSERT INTO students (id, first_name) VALUES (?, ?)";
try (var statement = connection.prepareStatement(sql)) {
    statement.setInt(1, student.getId());
    statement.setString(2, student.getName());
    statement.executeUpdate();
}
```
<!-- .element: class="fragment" -->

<ul>
    <li class="fragment">SQL Parameter als <code>?</code></li>
    <li class="fragment">Setzen durch <code>setXXX()</code></li>
    <li class="fragment">kein <code>' '</code> bei Strings notwendig</li>
</ul>

---
#### `ResultSet`
<ul>
    <li class="fragment fade-in-then-semi-out">wird von <code>statement.executeQuery</code> returnt</li>
    <li class="fragment fade-in-then-semi-out">ähnlich <code>Iterator</code> Bewegen eines Cursors</li>
    <li class="fragment fade-in-then-semi-out">Cursor zu Beginn <strong>vor</strong> erstem Record</li>
    <li class="fragment fade-in-then-semi-out">wird mit dem <code>Statement</code> geschlossen</li>
</ul>

note: es gibt auch andere ResultSet-Typen; nicht empfohlen:
* TYPE_SCROLL_SENSITIVE -> `previous()`
* CONCUR_UPDATABLE -> kann in Db schreiben

----
#### Methoden
<dl>
    <dt class="fragment"><code>boolean next()</code></dt>
    <dd class="fragment">Cursor weiterbewegen</dd>
    <dd class="fragment"><code>false</code> ⟹ kein Record mehr</dd>
    <dt class="fragment"><code>Xxx getXxx(String columnLabel)</code></dt>
    <dd class="fragment">liest Information aus aktuellem Record</dd>
    <dt class="fragment"><code>boolean wasNull()</code></dt>
    <dd class="fragment">nach <code>getXxx</code> zu callen</dd>
    <dd class="fragment"><code>true</code> ⟹ NULL in Db</dd>
    <pre class="fragment"><code class="hljs Java" data-trim data-noescape>
int n = resultSet.getInt("contains_nulls_and_0s");
// n = 0 entweder weil Column 0 oder NULL
if (resultSet.wasNull())
    ...
    </code></pre>
</dl>

----
```Java
public Stream<Student> findAll() throws SQLException {
        var sql = """
                SELECT id, first_name 
                FROM students
                """;
    try (var statement = connection.prepareStatement(sql)) {
        var resultSet = statement.executeQuery();
        var builder = Stream.<Student>builder();
        while (resultSet.next()) {
            var id = resultSet.getInt("id");
            var firstName = resultSet.getString("first_name");
            builder.add(new Student(id, firstName));
        }
        return builder.build();
    }
}
```
<!-- .element: class="fragment stretch" -->

note: Textblocks ⚠️ Blank am Ende

----
#### `NULL`
```Java
public class Student {

    private int id;
    private String firstName;
    private Boolean mayContactParents; // true, false, null
```
<!-- .element: class="fragment stretch" -->
 
```Java
public Stream<Student> findAllWithParentalPermission(Boolean permission) 
        throws SQLException {
        var sql = """
                SELECT id, first_name 
                FROM students 
                WHERE may_contact_parents = ? 
                """;
        try (var statement = connection.prepareStatement(sql)) {
            statement.setBoolean(1, permission);
        ...
```
<!-- .element: class="fragment stretch" -->

```Java
java.lang.NullPointerException
```
<!-- .element: class="fragment exception stretch" -->

```Java
if (permission == null)
    statement.setNull(1, Types.NULL);   ==> leerer Stream
```
<!-- .element: class="fragment stretch" -->

----
#### 😡 SQL 😡
<ul>
    <li class="fragment fade-in-then-semi-out"><code>= NULL</code> ist immer <code>false</code></li>
    <li class="fragment fade-in-then-semi-out"><code>IS NULL</code> 🤔</li>
</ul>

```Java
public Stream<Student> findAllWithParentalPermission(Boolean permission) 
        throws SQLException {
    var query = permission == null ? "IS NULL" : "= ?";
    var sql = """
            SELECT id, first_name 
            FROM students 
            WHERE may_contact_parents 
            """ + query;
    try (var statement = connection.prepareStatement(sql)) {
        if (permission != null)
            statement.setBoolean(1, permission);
        var resultSet = statement.executeQuery();
        ...
```
<!-- .element: class="fragment stretch" -->

---
#### `auto_increment`
```Java
public void save(User user) throws SQLException {
    var sql = """
            INSERT INTO users (login_name) 
            VALUES (?)
            """;
    try (var statement = connection.prepareStatement(sql,
            Statement.RETURN_GENERATED_KEYS)) {
        statement.setString(1, user.getName());
        statement.executeUpdate();
        ResultSet generatedKeys = statement.getGeneratedKeys();
        if (generatedKeys.next())
            user.setId(generatedKeys.getLong(1));
        else
            throw new SQLException("Saving user failed.");
    }
}
```
<!-- .element: class="fragment stretch" -->

---
#### Entities
```Java
public class Entity {

    private final Id id;    // Integer, Long, UID, String, ...
    private final Stuff stuff;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entity other = (Entity) o;
        return id == null ? false : id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return id == null ? 42 : id.hashCode();
    }
    
    public Optional<Id> getId() {
        return Optional.ofNullable(id);
    }
}
```
<!-- .element: class="fragment stretch" -->

---
#### Transactions
ermöglicht *Rollback* von Änderungen
<!-- .element: class="fragment" -->

```Java
connection.setAutoCommit(false);
try {
    thisMightNotWork();
    maybeViolatesConstraints();
    connection.commit();
} catch (SQLException e) {
    connection.rollback();
} finally {
    connection.setAutoCommit(true);
}
```
<!-- .element: class="fragment" -->

----
#### ACID
Transaction, wenn
<!-- .element: class="fragment" -->
<ul class="first-letter-large">
    <li class="fragment fade-in-then-semi-out">Atomic Operation notwendig</li>
    <li class="fragment fade-in-then-semi-out">Consistency der Db fragwürdig</li>
    <li class="fragment fade-in-then-semi-out">Isolation(Concurrency) notwendig</li>
    <li class="fragment fade-in-then-semi-out">Durability(Katastrophen) notwendig</li>
</ul>

----
#### `Savepoint`
```Java
connection.setAutoCommit(false);
Savepoint savepoint = null;
try (var statement = connection.createStatement()) {
    savepoint = connection.setSavepoint();
    statement.executeUpdate(sql[0]);
    statement.executeUpdate(sql[1]);

    savepoint = connection.setSavepoint();
    statement.executeUpdate(sql[2]);
    connection.commit();
} catch (SQLException e) {
    connection.rollback(savepoint);
} finally {
    connection.setAutoCommit(true);
}
```
<!-- .element: class="fragment stretch" -->

----
#### Batch processing
```Java
public void depositMoney(int amount, Account source, Account destination) 
        throws SQLException {
    connection.setAutoCommit(false);
    var sql = """
            UPDATE accounts 
            SET balance = balance + ? 
            WHERE account_num = ? """;
    try (var statement = connection.prepareStatement(sql)) {
        statement.setInt(1, amount);
        statement.setInt(2, destination.getNumber());
        statement.addBatch();
        statement.setInt(1, -amount);
        statement.setInt(2, source.getNumber());
        statement.addBatch();
        int[] result = statement.executeBatch();    // [updateCounts]
        connection.commit();
    } catch (SQLException e) {
        connection.rollback();
    } finally {
        connection.setAutoCommit(true);
    }
}
```
<!-- .element: class="fragment stretch" -->