![logo](./assets/dart/logo.png)

---
#### Dart?

* [DartPad](https://dartpad.dev/)
<!-- .element: class="fragment" -->
* Object-oriented
<!-- .element: class="fragment" -->
* null-safe
<!-- .element: class="fragment" -->
* JIT compiled beim Debuggen
<!-- .element: class="fragment" -->
* AOT compiled beim Release
<!-- .element: class="fragment" -->

---
#### Basics
```Dart
int x = 3;
print('x is $x');   // x is 3
assert('${1 + 1}' == '2');
String text = 'same old';   // oder ""
var firstCharacter = text[0];
```
<!-- .element: class="fragment" -->

```Dart
int.parse('42') // -> 42
3.14159.toStringAsFixed(2) // -> '3.14'
```
<!-- .element: class="fragment" -->

```Dart
var list = [1, 2, 3];
List<int> numbers = [...list, 4];
```
<!-- .element: class="fragment" -->

```Dart
var set = {0};
set.add(0);
assert(set.length == 1);
```
<!-- .element: class="fragment" -->

```Dart
var frameworkByDeveloper = {
        'Flutter': 'Google', 
        'React Native': 'Meta'
        };
assert(frameworkByDeveloper['Flutter'] == 'Google');
assert(frameworkByDeveloper['Ionic'] == null);
```
<!-- .element: class="fragment" -->

note:
`assert` nur im DebugMode

----
#### Null Safety
```Dart
String txt = null;          // 🚫
String? txt = null;         // Type? ist nullable
var firstChar = txt[0];     // 🚫
var firstChar = txt![0];    // ! -> es IST nicht null
```
<!-- .element: class="fragment" -->

---
#### Functions

```Dart
bool isEven(int n) {
    return n % 2 == 0;
}
```
<!-- .element: class="fragment" -->

```
bool isOdd(int n) => n % 2 != 0;
```
<!-- .element: class="fragment" -->

```Dart
Map<int, String>? _onlyForInternalUse(noType) {
    return null;
}

assert(_onlyForInternalUse(42) == null);
```
<!-- .element: class="fragment" -->

----
#### Named/Optional Parameters
```
void format(String text, {String direction='ltr', bool? bold}) 

format('text', bold: null);
format('text');
format('text', bold: false, direction:'rtl');
```
<!-- .element: class="fragment" -->

```Dart
String substring(int start, [int? end])
```
<!-- .element: class="fragment" -->

XOR
<!-- .element: class="fragment" -->

```
void foo(int i, {String? txt}, [bool b]);   // 🚫
```
<!-- .element: class="fragment" -->

---
#### Operatoren
```Dart
assert(12 / 10 == 1.2);
assert(12 ~/ 10 == 1);
```
<!-- .element: class="fragment" -->

```Dart
a ?? b == (a != null ? a : b)
```
<!-- .element: class="fragment" -->

```Dart
Object? x = null;
42 ?? 'only if null';  // -> 42
x ?? 42;               // -> 42
```
<!-- .element: class="fragment" -->

```Dart
o?.x == (o != null ? o.x : null)
```
<!-- .element: class="fragment" -->

```Dart
userObject?.userName?.toString()
```
<!-- .element: class="fragment" -->

----
#### Loops
```Dart
var evens = [0, 2, 4];
for (final n in evens) {
  print(n);
}
```
<!-- .element: class="fragment" -->

```Dart
evens.forEach((n) => print(n));     // Dart: avoid
```
<!-- .element: class="fragment" -->

```Dart
evens.forEach(print);
```
<!-- .element: class="fragment" -->

---
#### `λ`
```Dart
List.generate(10, (i) => i + 1)
    .where((i) => i % 2 == 1)
    .take(3)
    .map((i) => '-' * i)
    .forEach(print);
```

---
#### OOP
```Dart
class Person {
  String name = 'required';
  int? _age;  // not required
  
  Person(this.name, this._age);
  
  set age(int age) {
    if (age < 0) 
      throw 'Invalid age: $age';
    _age = age;
  }
  
  @override
  bool operator == (Object other) {
      identical(this, other) || other is Person &&
              runtimeType == other.runtimeType &&
              name == other.name && _age == other._age;
  }
  
  @override
  String toString() {
    return '$name, $_age';
  }

}
```
<!-- .element: class="fragment stretch" -->

----
#### Inheritance

```Dart
class Animal {
    final String name;  // impliziter getter

    void live() => print('living');

    Animal(this.name);
}
```
<!-- .element: class="fragment" -->

```Dart
class Dog extends Animal{
    Dog(String name) : super(name);
}
```
<!-- .element: class="fragment" -->

```Dart
class Ant implements Animal {
    @override
    String get name => 'Ant';
    
    @override
    void live() {}
}
```
<!-- .element: class="fragment" -->

----
#### Mixins

```Dart
abstract class Flyer {
    void fly() => print('flying');
    // void abstractMethod();
}
```
<!-- .element: class="fragment" -->

```Dart
class Duck extends Animal implements Flyer {
    Duck(String name) : super(name);

    @override
    void fly() {
        // Can't extend two classes
        // Have to implement interface (🤮 default 🤮)
        print('flying');
    }
}
```
<!-- .element: class="fragment" -->

```Dart
class Eagle extends Animal with Flyer {
    Eagle(String name) : super(name);
}
```
<!-- .element: class="fragment" -->

----
![animals](assets/dart/animals.png)
<!-- .element: class="fragment" -->

----
![animals](assets/dart/animals-with-mixins.png)
<!-- .element: class="fragment" -->

note: 
Mixins 'stacken' Funktionalität
[MixIns](https://medium.com/flutter-community/dart-what-are-mixins-3a72344011f3)

---
#### `final` vs `const`
```Dart
class Point {
  static final Point origin = const Point(0, 0);
  final int x;
  final int y;
  const Point(this.x, this.y);
  Point.clone(Point other) : this(other.x, other.y);    // named Constructor
}
```
<!-- .element: class="fragment" -->

```Dart
  var point = Point(0, 0);
  assert(identical(point, Point.origin));   💥
```
<!-- .element: class="fragment" -->

```Dart
  const point = Point(0, 0);
  assert(identical(point, Point.origin));   ✔️
```
<!-- .element: class="fragment" -->

`final` ⟹ Runtime constant
<!-- .element: class="fragment" -->

`const` ⟹ Compile constant
<!-- .element: class="fragment" -->

----
```Dart
const text = const ['Lorem'];
text.add('ipsum');
```
<!-- .element: class="fragment" -->

```Dart
var text = const ['Lorem'];
text.add('ipsum');
```
<!-- .element: class="fragment" -->

```Dart
text = ['Something', 'different'];
```
<!-- .element: class="fragment" -->
