## Data Science &  
## Machine Learning

---
#### Data Science
![venn-diagram](./assets/dsai/ds_venn_en.png)
<!-- .element: class="r-stretch" -->

---
#### Artificial Intelligence

* nimmt seine Umgebung wahr
<!-- .element: class="fragment fade-in-then-semi-out" -->
* führt intelligente Aktionen durch, um ein Ziel zu erreichen
<!-- .element: class="fragment fade-in-then-semi-out" -->
* lernt
<!-- .element: class="fragment fade-in-then-semi-out" -->
* plant
<!-- .element: class="fragment fade-in-then-semi-out" -->
* folgert
<!-- .element: class="fragment fade-in" -->

----
#### Subfields
![ai-subfields](./assets/dsai/ai-subfields.ppm)
<!-- .element: class="fragment fade-in" -->

note: 
* rot: von DL assimiliert

----
#### Geschichte
![deep-learning](./assets/dsai/ai-ml-dl.png)
<!-- .element: class="fragment fade-in" -->

note:
* AI Winter I: 1973-1980
* AI Winter II: 1993-2006
----
#### (weak) AI vs (strong) AGI
<dl>
    <dt class="fragment fade-in">Weak AI</dt>
    <dd class="fragment fade-in-then-semi-out">Kann EINE Sache gut</dd>
    <dd class="fragment fade-in-then-semi-out">Spamfilter</dd>
    <dd class="fragment fade-in-then-semi-out">Machine Translation</dd>
    <dd class="fragment fade-in-then-semi-out">Selbstfahrende Autos</dd>
    <dt class="fragment fade-in">Strong AI/AGI</dt>
    <dd class="fragment fade-in-then-semi-out">Kann alles mindestens auf Menschenniveau</dd>
    <dd class="fragment fade-in-then-semi-out">Terminator, Data</dd>
    <dd class="fragment fade-in">ETA: 2045/nie</dd>
</dl>

---
#### Machine Learning
![traditional-vs-ml](./assets/dsai/traditional-vs-ml.png)
<!-- .element: class="r-stretch" -->

----
#### Subfields
![ml-subfields](./assets/dsai/machine-learning-disciplines.jpg)
<!-- .element: class="r-stretch" -->

----
#### (Un)supervised
![supervised-vs-unsupervised](./assets/dsai/supervised-vs-unsupervised.png)
<!-- .element: class="r-stretch" -->

----
#### Reinforcement
![reinforcement](./assets/dsai/reinforcement.png)
<!-- .element: class="r-stretch" -->