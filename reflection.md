# Reflection

---
#### Motivation

```Java
@NoArgsConstructor
@Getter
class User {
    private String name;
    private String email;
}
```
<!-- .element: class="fragment" -->

```Java
var user = entitiyManager.find(User.class, 42);
System.out.println(user.getName()); -> "Max"
```
<!-- .element: class="fragment" -->

Wie?
<!-- .element: class="fragment" -->

---
#### `Class`

```Java
Class<User> clazz = User.class;
clazz = user.getClass();
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
class Class {
    String getName()       // -> at.htlstp.domain.User
    String getSimpleName() // -> User
    Field[] getDeclaredFields()
    Constructor[] getDeclaredConstructors()
    Method[] getDeclaredMethods()
    Field getField(String name)
    Constructor getConstructor(Class<?>... parameterTypes)
    Method getMethod(String name, Class<?>... parameterTypes)
    A getAnnotation(Class<A> annotationClass)
    ...
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

<ul>
    <li class="fragment fade-in-then-semi-out"><code>getMethods()</code> -> alle public Methoden</li>
    <li class="fragment fade-in-then-semi-out"><code>getDeclaredMethods()</code> -> alle Methoden</li>
    <li class="fragment fade-in-then-semi-out"><code>Constructor</code>, <code>Field</code>, <code>Method</code> analog</li>
</ul>

---
#### Annotations
```Java
public @interface Entity {
   String name default "";
   String table();
   String[] constraints();
}
```
<!-- .element: class="fragment fade-in" -->

```Java
@Entity(table = "users", constraints = {"UNIQUE(name)"})
class User {}
```
<!-- .element: class="fragment fade-in" -->

```Java
Class clazz = User.class
Entity entity = clazz.getAnnotation(Entity.class);
String table = null;
if (entity.table().equals(""))
    table = clazz.getSimpleName();
else
    table = entity.table();
connection.prepareStatement("create table ? ...")
        .setString(1, table);
```
<!-- .element: class="fragment fade-in" -->