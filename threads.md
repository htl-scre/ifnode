# Threads

---
#### Motivation
* langwierige Operationen
<!-- .element: class="fragment fade-in-then-semi-out" -->
* warten auf IO
<!-- .element: class="fragment fade-in-then-semi-out" -->
* modellieren paralleler Abläufe
<!-- .element: class="fragment fade-in" -->

----
#### Warten auf IO
<dl>
    <div class="fragment fade-in-then-semi-out">
        <dt>Szenario</dt>
        <dd>Mehrere Clients connecten zu einem Server, um Daten zu empfangen. Ohne <em>Concurrency</em> 
            könnte sich gleichzeitig nur ein einziger Client verbinden.</dd>
    </div>
    <div class="fragment fade-in">
        <dt>Lösung</dt>
        <dd>Jedem Client wird ein Thread zugeteilt, welcher sich um die Kommunikation kümmert.</dd>
    </div>
</dl>

---
##### Einbettung im OS
* moderne Prozessoren verfügen über mehrere CPUs/Cores
<!-- .element: class="fragment fade-in-then-semi-out" -->
* ein<!-- .element: class="fragment fade-in-then-semi-out" --> *Prozess* kann Threads erzeugen
* der<!-- .element: class="fragment fade-in-then-semi-out" --> OS-*Scheduler* verwaltet, welcher Prozess bzw. Thread wann arbeiten darf
* ein<!-- .element: class="fragment" --> Thread kann **jederzeit** vom OS pausiert werden ⚠️

----
#### Time Slicing
![time-slicing](assets/Threads/time-slicing.png)
Threads > Prozessoren
<!-- .element: class="fragment fade-in-then-semi-out" -->

note: Physische Prozessoren werden oft in logische Prozessoren aufgeteilt -> Cores

----
#### Hyperthreading
![hyperthreading](assets/Threads/hyperthreading.jfif)

---
#### Erzeugung
```Java
Thread thread = new Thread();
thread.start();
```

Festlegung des auszuführenden Codes
<ul>
<li class="fragment fade-in-then-semi-out"><code>Thread</code> extenden</li>
<li class="fragment"><code>Runnable</code> implementieren</li>
</ul>

----
#### `Thread` extenden
```Java
public class MyThread extends Thread {
	@Override
	public void run(){
		System.out.println("MyThread running");
	}
}
```

```Java
MyThread myThread = new MyThread();
myThread.start();
```
<!-- .element: class="fragment fade-in" -->

----
#### `Runnable` implementieren
```Java
public class MyRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("MyRunnable running");
    }
}
```

```Java
Runnable runnable = new MyRunnable();
new Thread(runnable).start();
```
<!-- .element: class="fragment fade-in" -->

```Java
runnable = () -> foo();
new Thread(runnable, "name").start();
```
<!-- .element: class="fragment fade-in" -->

<div>
👍
<ul>
<li>Interfaces sind dazu da, Verhalten zu deklarieren</li>
<li>Möglichkeit, andere Klassen zu extenden</li>
</ul>
</div>
<!-- .element: class="fragment" -->

---
#### Beispiel
```Java
public class ThreadExample {
    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName());
        for (int i = 0; i < 4; i++) {
            new Thread("" + i) {
                @Override
                public void run() {
                    System.out.println("Thread: " + getName() + " running");
                }
            }.start();
        }
    }
}
```

```
main
Thread: 1 running
Thread: 2 running
Thread: 0 running
Thread: 3 running
```
<!-- .element: class="fragment fade-in" -->

---
#### Fehler
<pre><code class="hljs Java" data-trim data-noescape>
Runnable runnable = () -> foo();
<span class="fragment strike" data-fragment-index="1">new Thread(runnable).run();</span>
<span class="fragment" data-fragment-index="2">new Thread(runnable).start();</span>
</code></pre>
`run` läuft im Mainthread
<!-- .element: class="fragment" data-fragment-index="1"-->

---
#### Meta
Interfaces trennen das *was* vom *wie*  
Threads trennen das *was* vom *wann*

---
#### Kontrolle
<div class="fragment fade-in-then-semi-out">
<pre><code class="hljs Java" data-trim>
Thread thread = Thread.currentThread();
 </code></pre>
Liefert den aktuellen Thread
</div>
<div class="fragment fade-in-then-semi-out">
    <pre><code class="hljs Java" data-trim>
Thread.sleep(1000); //1000ms
 </code></pre>
    <p>Pausiert den aktuellen Thread</p>
</div>

Zum Stoppen muss `run` enden
<!-- .element: class="fragment" -->
note: Statische Methoden

----
#### `InterruptedException`
```Java
Thread.currentThread().interrupt();
```
* wird <!-- .element: class="fragment fade-in-then-semi-out" -->von vielen `Thread`-Methoden geworfen
* unterbricht <!-- .element: class="fragment fade-in-then-semi-out" -->einen laufenden Thread
* im unterbrochenen <!-- .element: class="fragment fade-in-then-semi-out" -->Thread wird eine `InterruptedException` ausgelöst
<li class="fragment">
<em>wie</em> der Thread darauf reagiert, wird im <code>catch</code> definiert
</li>
----
#### `join`
```Java
Thread thread = new Thread(() -> {
    sleep(1000);
    System.out.println("guaranteed");
});
thread.start();
thread.join();
System.out.println("order");
```
Wenn <!-- .element: class="fragment fade-in" -->Thread **A** während seiner Ausführung `B.join` aufruft, dann wartet **A** mit seiner weiteren Ausführung, bis **B** *dead* ist.
  
note: Sinnfreies Beispiel, da zu keinem Zeitpunkt mehrere Threads aktiv sind. Dead wird beim Lifecycle erklärt

----
#### Daemon Threads
```Java
thread.setDaemon(true);
```
* in UNIX ein Hintergrundprozess, in Windows genannt Service
<!-- .element: class="fragment fade-in-then-semi-out" -->
* wenn nur mehr Daemon Threads laufen, beendet sich die JVM ⟹ Hintergrundaufgaben
<!-- .element: class="fragment fade-in-then-semi-out" -->
* muss vor dem Starten gesetzt sein
<!-- .element: class="fragment" -->

---
#### Synchronisation
```Java
public class IdGenerator {
    int lastUsedId;

    public int incrementValue() {
        return ++lastUsedId;
    }
}
```
* ein Thread ✔️
* zwei<!-- .element: class="fragment"--> Threads mit demselben `IdGenerator`:
  - A <!-- .element: class="fragment"-->kriegt 1, B kriegt 2, `lastUsedId` ist 2
  - A <!-- .element: class="fragment"-->kriegt 2, B kriegt 1, `lastUsedId` ist 2
  - A <!-- .element: class="fragment"-->kriegt 1, B kriegt 1, `lastUsedId` ist 1 ❌
  
Der dritte Fall ist selten (1:20000), aber er passiert
<!-- .element: class="fragment"-->

----
#### Atomare Operationen
* ganz oder gar nicht<!-- .element: class="fragment fade-in-then-semi-out" -->
* auf<!-- .element: class="fragment fade-in-then-semi-out" --> Datenbanken *Transaktion*
* in Java:<!-- .element: class="fragment fade-in-then-semi-out" --> 
 - Variablen lesen (außer `long/double`)
 - Variable schreiben (außer `long/double`)
* im <!-- .element: class="fragment" -->Package `java.util.concurrent.atomic` finden sich Klassen wie `AtomicInteger` mit hilfreichen Methoden
note:long/double werden JVM-Abhängig als 2x32 Blöcke behandelt. Fix: volatile

---
#### `synchronized` Methoden
```Java
public synchronized int incrementValue() {
    return ++lastUsedId;
}
```
* erzeugt einen *Mutex* (MUTual EXclusion)
* nur ein Thread kann diesen Block gleichzeitig betreten
note: *Semaphore*: Maximal n Threads können Block betreten

----
#### `synchronized` Blöcke
```Java
public void foo() {
    // code
    synchronized(object) {
        // code
    }
}
```
* nur ein Thread kann pro Objekt das *Lock* / den *Monitor* halten
* bevor <!-- .element: class="fragment" -->ein Thread den Block betreten darf, muss er das Lock requesten
  - bekommt er das Lock, fährt der Thread fort
  - ansonsten blockt der Thread und wartet
* ein <!-- .element: class="fragment" -->Thread kann dasselbe Lock mehrmals halten
  
note: Warten mehrere Threads, so bekommt irgendeiner das Lock

----
#### Locking
![synchronized](assets/Threads/mutex.png)

----
#### Methoden Locks
eine `synchronized` **Methode** locked
<!-- .element: style="text-align:left" -->
* `this`
* `XXX.class` bei statischen Methoden  
* 👍 Alternative: gut benanntes Lock-Objekt
<!-- .element: class="fragment" -->

---
#### Lifecycle
![Lifecycle](assets/Threads/thread-life-cycle.png)
<!-- .element: class="stretch" -->

---
#### Busy waiting
```Java
public class MySignal {
    private boolean hasDataToProcess = false;

    public synchronized boolean hasDataToProcess() {
        return this.hasDataToProcess;
    }
```
Thread **A** übergibt Daten, Thread **B** wartet: 
```Java
private MySignal sharedSignal;

while(!sharedSignal.hasDataToProcess()){
  // do nothing... busy waiting
}
sharedSignal.processData();
```
note: CPU rennt auf 100% Auslastung.  
Sind wir schon da?  
Sind wir schon da?  
Sind wir schon da?  
Sind wir schon da?
----
#### `wait`/`notify`
```Java
synchronized(elevator) {
    while(burning)
        elevator.wait();
}
```
<!-- .element: class="fragment" -->
```Java
synchronized(elevator) {
    extinguishFire();
    elevator.notifyAll();
}
```
<!-- .element: class="fragment" -->

----
#### `wait`
```Java
elevator.wait();
```
<ul>
<li class="fragment fade-in-then-semi-out"><code>Object</code>-Methode</li>
<li class="fragment fade-in-then-semi-out"><strong>muss</strong> in <code>synchronized</code>-Block gecalled werden, sonst <code>IllegalMonitorStateException</code> 💥</li>
<li class="fragment fade-in-then-semi-out">Thread geht in den Zustand <em>blocked</em></li>
<li class="fragment fade-in-then-semi-out">Thread gibt <strong>alle</strong> Locks frei</li>
<li class="fragment"><strong>immer</strong> in einem <code>while</code>
<ul>
  <li>mehrere Threads callen <code>wait</code></li>
  <li class="fragment"><em>Spurious Wakeups</em> 👻</li>
  </ul>
</li>
</ul>
----
#### `notify`/`notifyAll`
```Java
elevator.notify();
```
<ul>
<li class="fragment fade-in-then-semi-out"><code>Object</code>-Methode</li>
<li class="fragment fade-in-then-semi-out"><strong>muss</strong> in <code>synchronized</code>-Block gecalled werden, sonst <code>IllegalMonitorStateException</code> 💥</li>
<li class="fragment fade-in-then-semi-out">holt <em>einen</em>/<em>alle</em> Threads aus dem Zustand <em>waiting</em>, diese müssen nun wieder das Lock requesten</li>
<li class="fragment fade-in-then-semi-out">gehaltene Locks werden <strong>nicht</strong> freigegeben</li>
</ul>
note: Frage: welches Lock?

----
```Java
private boolean hasDataToProcess = false;
private MonitorObject monitor = new MonitorObject();

public void setData(Object data) throws InterruptedException {
    synchronized (monitor) {
        while (hasDataToProcess) 
            monitor.wait();
        hasDataToProcess = true;
        monitor.notifyAll();
    }
}

public void processData() throws InterruptedException {
    synchronized (monitor) {
        while (!hasDataToProcess) 
            monitor.wait();
        doStuff(dataToProcess);
        hasDataToProcess = false;
        monitor.notifyAll();
    }
}
```
<!-- .element: class="stretch" -->
note: Exception Handling Code weggelassen

---
#### Deadlock
* Website connected zu zwei Datenbanken 
<!-- .element: class="fragment fade-in-then-semi-out" -->
* n mögliche Connections je Db
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Operation<!-- .element: class="fragment fade-in-then-semi-out" --> **A** verbindet sich zu Db**1**, dann Db**2**
* Operation<!-- .element: class="fragment fade-in-then-semi-out" --> **B** verbindet sich zu Db**2**, dann Db**1**
* n<!-- .element: class="fragment fade-in-then-semi-out" --> User führen **A** aus
* n<!-- .element: class="fragment fade-in-then-semi-out" --> User führen **B** aus
* alle<!-- .element: class="fragment fade-in-then-semi-out" --> **A**-User warten darauf, dass ein **B**-User seine Connection hergibt 
* die<!-- .element: class="fragment fade-in-then-semi-out" --> warten allerdings alle auf eine Connection der **A**-User

----
#### weitere Probleme
<dl>
    <div class="fragment fade-in-then-semi-out">
        <dt>Starvation</dt>
        <dd>Thread bekommt nie alle Resourcen, um seine Arbeit zu beenden</dd>
    </div>
    <div class="fragment fade-in-then-semi-out">
        <dt>Livelock</dt>
        <dd>
            wie eine Deadlock, aber Threads sind <em>busy waiting</em>
        </dd>
    </div>
</dl>

---
#### Best practice
* so wenig Abhängigkeiten wie möglich
<!-- .element: class="fragment fade-in-then-semi-out" -->
* so<!-- .element: class="fragment fade-in-then-semi-out" --> wenig `synchronized` wie möglich
* testen, testen, testen
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Serverseitig synchronisieren (kapseln)
<!-- .element: class="fragment" -->
