# IO

---

#### Überblick

<dl>
    <dt class="fragment"><code>File</code></dt>
    <dd class="fragment fade-in-then-semi-out">OOP-Repräsentation einer Datei</dd>
    <dd class="fragment fade-in-then-semi-out">Pfad- und Dateimanipulation</dd>
    <dd class="fragment fade-in-then-semi-out">alte <code>java.io</code> Implementierung</dd>
    <dt class="fragment"><code>Path</code></dt>
    <dd class="fragment fade-in-then-semi-out">OOP-Repräsentation eines Dateipfads</dd>
    <dd class="fragment fade-in-then-semi-out">Pfadmanipulation</dd>
    <dt class="fragment"><code>Files</code></dt>
    <dd class="fragment fade-in-then-semi-out">nur statische Methoden</dd>
    <dd class="fragment">Dateimanipulation</dd>
</dl>

---
#### `Path`
```Java
public interface Path 
        extends Comparable<Path>, Iterable<Path>, Watchable {

    Path getRoot();
    Path getParent();
    Path getFileName();
    int getNameCount();     
    Path getName(int index);
    Path subpath(int beginIndex, int endIndex);
    boolean startsWith(Path other);
    boolean endsWith(Path other);
    Path resolve(Path other);
    Path relativize(Path other);
    URI toUri();
    default File toFile() { ... }
```
<!-- .element: class="fragment stretch" --> 

----
<pre class="fragment stretch"><code class="hljs Java" data-trim data-noescape>
Path path  = Path.of("C:/data/images/2020/img.jpg");
Path root = path.getRoot();         <span class="fragment">=> C:\</span>
Path parent = path.getParent();     <span class="fragment">=> C:\data\images\2020</span>
Path fileName = path.getFileName(); <span class="fragment">=> img.jpg</span>

List<Path> parts = new ArrayList<>();
for (int i = 0; i < path.getNameCount(); i++)
    parts.add(path.getName(i)); <span class="fragment">    => [data, images, 2020, img.jpg]</span>
</code></pre>

<pre class="fragment stretch"><code class="hljs Java" data-trim data-noescape>
Path subPath = path.subpath(2, 4);          <span class="fragment">=> 2020\img.jpg</span>
Path stuff = Path.of("other/stuff");
Path resolved = stuff.resolve(subPath);     <span class="fragment">=> other\stuff\2020\img.jpg</span>
Path music = Path.of("other/music");    
Path relativized = stuff.relativize(music); <span class="fragment">=> ..\music</span>
</code></pre>

---
## `Files`

note: `Channel`/`Buffer` -> nio für non-blocking-Streams

---
#### `Input`-/`OutputStream`
```Java
try (DataOutputStream output = new DataOutputStream(
        Files.newOutputStream(path))) {
    output.writeByte(2);
}
```
<!-- .element: class="fragment" --> 

```Java
try (DataOutputStream output = new DataOutputStream(
        Files.newOutputStream(path, StandardOpenOption.APPEND))) {
    output.writeByte(3);
}
```
<!-- .element: class="fragment" --> 

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
try (DataInputStream input = new DataInputStream(
        Files.newInputStream(path))) {
    System.out.println(input.readByte());   <span class="fragment">=> 2</span>
    System.out.println(input.readByte());   <span class="fragment">=> 3</span>
}
</code></pre>

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
byte[] all = Files.readAllBytes(path);  <span class="fragment">=> [2, 3] ⚠️ RAM</span>
byte[] toWrite = {5, 7, 9};
Files.write(path, toWrite, StandardOpenOption.APPEND);
all = Files.readAllBytes(path);         <span class="fragment">=> [2, 3, 5, 7, 9]</span>
</code></pre>

----
#### `Reader`/`Writer`
```Java
try (PrintWriter writer = new PrintWriter(
        Files.newBufferedWriter(path, StandardOpenOption.CREATE))) {
    writer.println("line 1");
    writer.println("line 2");
}
```
<!-- .element: class="fragment stretch" --> 

<pre class="fragment stretch"><code class="hljs Java" data-trim data-noescape>
try (BufferedReader reader = Files.newBufferedReader(path)) {
    String line = reader.readLine();    <span class="fragment">=> line 1</span>
}
</code></pre>

```Java
String line = "line 3" + System.lineSeparator();
Files.writeString(path, line, StandardOpenOption.APPEND);
```
<!-- .element: class="fragment stretch" --> 

```Java
List<String> lines = List.of("line 4", "line 5");
Files.write(path, lines, StandardOpenOption.APPEND);
```
<!-- .element: class="fragment stretch" --> 

<pre class="fragment stretch"><code class="hljs Java" data-trim data-noescape>
String content = Files.readString(path);    <span class="fragment">=> line 1%nline 2%n... ⚠️ RAM</span>
</code></pre>

<pre class="fragment stretch"><code class="hljs Java" data-trim data-noescape>
List<String> lines = Files.readAllLines(path);    <span class="fragment">=> [line 1, ...] ⚠️ RAM</span>
</code></pre>

```Java
try (Stream<String> lines = Files.lines(path)) { ... }   ✔️ RAM
```
<!-- .element: class="fragment stretch" --> 

---
#### Checks
```Java
public static boolean exists(Path path, LinkOption... options)
public static boolean notExists(Path path, LinkOption... options)
public static boolean isDirectory(Path path, LinkOption... options)
public static boolean isRegularFile(Path path, LinkOption... options)
public static boolean isSymbolicLink(Path path)
public static boolean isSameFile(Path path, Path path2)
public static boolean isReadable(Path path)
public static boolean isWritable(Path path)
public static boolean isExecutable(Path path)
public static boolean isHidden(Path path)
```
<!-- .element: class="fragment stretch" --> 

----
#### File/Directory Handling
```Java
Path path = Path.of("C:/data");
Path images = path.resolve(Path.of("images"));
Files.createDirectory(images);
```
<!-- .element: class="fragment stretch " --> 

```Java
Path images2020 = path.resolve(Path.of("images/2020/2/"));
Files.createDirectory(images2020);
```
<!-- .element: class="fragment stretch " --> 

```Plain Text
Exception in thread "main" java.nio.file.NoSuchFileException: 
        C:\data\images\2020\2
```
<!-- .element: class="fragment exception stretch" --> 

```Java
Path images2020 = path.resolve(Path.of("images/2020/2/"));
Files.createDirectories(images2020);
```
<!-- .element: class="fragment stretch " --> 

erzeugt *alle* notwendigen Directories
<!-- .element: class="fragment " --> 

```Java
public static void delete(Path path)
public static boolean deleteIfExists(Path path)
public static Path copy(Path source, Path target, CopyOption... options)
public static Path move(Path source, Path target, CopyOption... options)
```
<!-- .element: class="fragment stretch " --> 

----
#### `list` etc.
```Java
try(Stream<Path> paths = Files.list(Path.of("C:/Windows"))) {
    List<Path> all = paths.collect(Collectors.toList());
}
```
<!-- .element: class="fragment " --> 

```Plain Text
[alle Folder, alle Files]
```
<!-- .element: class="fragment " --> 

mit Glob

```Java
List<Path> inis = new ArrayList<>();
try (DirectoryStream<Path> paths = Files.newDirectoryStream(
        Path.of("C:/Windows"), "*.ini")) {
    for (Path entry : paths)
        inis.add(entry);
}
```
<!-- .element: class="fragment " --> 

```Plain Text
[C:\Windows\system.ini, C:\Windows\win.ini]
```
<!-- .element: class="fragment " --> 

beide bleiben im übergebenen `Path`
<!-- .element: class="fragment " --> 

----
#### Rekursiv
```Java
PathMatcher matcher = FileSystems.getDefault()
        .getPathMatcher("glob:**IntelliJ**runner*.exe");
```
<!-- .element: class="fragment " --> 

```Plain Text
root\folder\folder\...\%IntelliJ%\folder\folder\...\%runner%.exe
```
<!-- .element: class="fragment " --> 

```Java
try (Stream<Path> paths = Files.find(
        Path.of("C:/Program Files/JetBrains"),
        3,
        (p, a) -> matcher.matches(p))) {
    List<Path> runners = paths.collect(Collectors.toList());
}
```
<!-- .element: class="fragment " --> 

```Plain Text
[runnerw.exe, runnerw64.exe]
```
<!-- .element: class="fragment " --> 
