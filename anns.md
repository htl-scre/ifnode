# Artificial Neural Networks

---
### Motivation
Funktionale Nachbildung eines Neurons
<img src="./assets/anns/neuron.webp" alt="neuron" style="height:60vh;"/>

---
### Forward pass

$$out = f(b + \sum_i {x_i \cdot w_i}) = f(b + \textbf{x}\textbf{w}^T)$$
<!-- .element: class="fragment" -->
* $x_i\dots$ Inputfeatures
<!-- .element: class="fragment" -->
* $w_i\dots$ Weights, Parameter
<!-- .element: class="fragment" -->
* $b\dots$ Bias
<!-- .element: class="fragment" -->
* $f\dots$ Aktivierungsfunktion
<!-- .element: class="fragment" -->

----
### Network
![neuron](./assets/anns/ann.webp)
<!-- .element: class="fragment" -->

----
$$\textbf{out}_1 = f(\textbf{x}\cdot\textbf{W}_1)$$
<!-- .element: class="fragment" -->
$$\textbf{out}_2 = f(\textbf{out}_1\cdot\textbf{W}_2)$$
<!-- .element: class="fragment" -->
$$\textbf{y} = f(\textbf{out}_2\cdot\textbf{W}_3)$$
<!-- .element: class="fragment" -->

$$\textbf{y} = f(f(f(\textbf{x}\cdot\textbf{W}_1)\cdot\textbf{W}_2)\cdot\textbf{W}_3)$$
<!-- .element: class="fragment" -->

---
### Aktivierungsfunktion
$$f_1(x) = k_1x + d_1, f_2(x) = k_2x + d_2$$
<!-- .element: class="fragment" -->
$$f_1(f_2(x)) = k_1f_2(x) + d_1 = k_1(k_2x + d_2) + d_1$$
<!-- .element: class="fragment" -->
$$f_1(f_2(x)) = k_1k_2x + k_1d_2 + d_1$$
<!-- .element: class="fragment" -->
$$f_1(f_2(x)) = \underbrace{k_1k_2}_\textrm{k}x + \underbrace{k_1d_2 + d_1}_\textrm{d}$$
<!-- .element: class="fragment" -->

Ohne Aktivierungsfunktionen nur lineare Zusammenhänge
<!-- .element: class="fragment" -->

----
![](./assets/anns/activation-functions.png)

----
### Auswahl

<ul>
  <li class="fragment">Hidden Layer: *elu</li>
  <li class="fragment">Output layer
  <dl>
    <div class="fragment">
      <dt>Regression</dt>
      <dd>Keine $\rightarrow y=x$</dd>
      <dd>Loss: <code>MSELoss()</code></dd>
    </div>
  </dl>
  </li>
</ul>

----
### Klassifikation
Logits ⟹ Probabilities ⟹ Classes
<!-- .element: class="fragment" -->

<dl>
    <div class="fragment">
      <dt>Binäre Klassifikation</dt>
      <dd><code>Sigmoid()</code> / <code>BCELoss()</code></dd>
    </div>
      <dd class="fragment">keine / <code>BCEWithLogitsLoss()</code></dd>
      <dd class="fragment">
      <pre class="hljs python"><code data-trim data-noescape>
Linear(in_features=42, out_features=2)
Softmax(dim=1)
      </code></pre>
      </dd>
    <div class="fragment">
      <dt>Multi-class-classification</dt>
      <dd class="fragment">keine / <code>CrossEntropyLoss()</code></dd>
      <dd class="fragment"><code>Softmax()</code> / <code>CrossEntropyLoss()</code></dd>
<dd class="fragment">
      <pre class="hljs python"><code data-trim data-noescape>
Linear(in_features=42, out_features=class_count)
Softmax(dim=1)
      </code></pre>
      </dd>
    </div>
</dl>

Note: CrossEntropyLoss kann mit logits und probabilites arbeiten

---
### Training
1. Alle $w_i$ random initialisieren
<!-- .element: class="fragment" -->
2. forward pass:  
  Observation -> Output
<!-- .element: class="fragment" -->
3. Loss $L(y_{true}, y_{pred})$ berechnen
<!-- .element: class="fragment" -->
4. backward pass/backpropagation:  
  $w_i$ anpassen vom output-Layer bis zum input-Layer
<!-- .element: class="fragment" -->

```python
for X, y in data:
  prediction = model(X)
  error = loss_fn(y, prediction)
  update_weights(error, loss)
```
<!-- .element: class="fragment" -->

----
### Forward pass
![weight-matrices](./assets/anns/weight-matrix.webp)

$$ 1 \times 3 \cdot 3 \times 4  = 1 \times 4$$
<!-- .element: class="fragment" -->

----
![forward-pass](./assets/anns/forward-pass.webp)

note: weights fehlen -> Übersicht 

---
### Backward pass
* $L(y_{true}, y_{pred}),\qquad y_{pred}(\textbf{x}, \textbf{w})$
<!-- .element: class="fragment" -->
* $\implies L(y_{true}, \textbf{x}, \textbf{w})$
<!-- .element: class="fragment" -->
* $\implies$ Anpassen der weights $\textbf{w}$, sodass Fehler $L$ minimal
<!-- .element: class="fragment" -->

----
### Gradient Descent
![](./assets/anns/optimizers.gif)

----
<img src="./assets/anns/gradient-descent.webp" alt="gradient-descent" class="fragment" style="height:80vh;"/>

----
### Learning rate
![learning rate](./assets/anns/learning-rate.webp)
<!-- .element: class="fragment" -->

----
### Probleme
![gd problems](./assets/anns/gd-problems.webp)
<!-- .element: class="fragment" -->

----
### Momentum
![](./assets/anns/opt2.gif)
<!-- .element: class="fragment" -->

---
### Backpropagation
$$w_{t+1} = w_t - \eta\frac{\partial L}{\partial w_t}$$
$\eta\dots $ learning rate
<!-- .element: class="fragment" -->

----
### Kettenregel
$$y = f(u), u = g(x)) \implies y=f(g(x))$$
<!-- .element: class="fragment" -->
$$\implies y' = f(g(x))'= f'(g) \cdot g'(x)$$
<!-- .element: class="fragment" -->
$$y = sin(x^2) \implies y' = cos(x^2) \cdot 2x$$
<!-- .element: class="fragment" -->
$$\frac{dy}{dx} = \frac{dy}{du}\frac{du}{dx}$$
<!-- .element: class="fragment" -->

----
<img src="./assets/anns/backward-pass.webp" alt="backward-pass" class="fragment" style="height:85vh;"/>

----
$$\frac{\partial L}{\partial w^2_{11}} = 
\frac{\partial L}{\partial \hat{y}} 
\frac{\partial \hat{y}}{\partial z_1^2}
\frac{\partial z_1^2}{\partial w^2_{11}}$$
<!-- .element: class="fragment" -->

\begin{align}
\frac{\partial z_1^2}{\partial w^2_{11}} = &\frac{\partial}{\partial w^2_{11}}(f_1^1 w^2_{11} + f_2^1 w^2_{12} + f_3^1 w^2_{13} + f_4^1 w^2_{14} + b_1^2)\\\\
= &f_1^1
\end{align}
<!-- .element: class="fragment" -->


$\hat{y}(z) = \frac{1}{1+e^{-z}}$ aka sigmoid, $\hat{y}(z)' = \dots$
<!-- .element: class="fragment" -->
$$\frac{\partial L}{\partial w^2_{11}} = -0.405$$
<!-- .element: class="fragment" -->

---
### `Adam` - Adaptive Moment Estimation
```python
optimizer = Adam(model.parameters(), lr=1e-3)
```
<!-- .element: class="fragment" -->

* für jedes $w$ werden die letzten Änderungen ausgewertet
<!-- .element: class="fragment" -->
* und diese in die Backpropagation einbezogen
<!-- .element: class="fragment" -->

---
### `batch_size`
Bei jeder observation weights updaten problematisch:
<!-- .element: class="fragment" -->
* viel Aufwand
<!-- .element: class="fragment" -->
* ständig auf/ab
<!-- .element: class="fragment" -->
* 💡 ein paar processen, Fehler mitteln
<!-- .element: class="fragment" -->

```python
for X, y in data:
  # X.shape[0] = batch_size
  predictions = model(X) # shape = (batch_size, 1)
  error = loss_fn(y, prediction) # shape = (batch_size, 1)
  total_error = error.sum() # shape = (1, 1)
  optimizer.update_weights()
```
<!-- .element: class="fragment" -->
