![logo](assets/flutter/logo.png)

---

#### Motivation

![traffic-share](assets/flutter/traffic-share.png)
<!-- .element: class="fragment" -->

---

#### Development

<dl>
    <dt class="fragment fade-in">Native</dt>
    <dd class="fragment fade-in-then-semi-out">Eine App je für Android/iOs</dd>
    <dt class="fragment fade-in">Cross Platform</dt>
    <dd class="fragment fade-in-then-semi-out"><strong>Eine</strong> App, welche <em>irgendwie</em> auf beiden Systemen läuft </dd>
    <dd class="fragment fade-in">Player:</dd>
    <ul>
        <li class="fragment fade-in-then-semi-out">React Native</li>
        <li class="fragment fade-in-then-semi-out">Ionic</li>
        <li class="fragment">Flutter</li>
    </ul>


</dl>

----

#### (React) Native vs Flutter

![comparison](assets/flutter/react-native-vs-flutter.webp)
<!-- .element: class="fragment" -->

notes:
Bridge - js/native Interface

----

#### Architektur

![architektur](assets/flutter/flutter-architecture.png)

---

#### Setup

<ol>
    <li><span class="fragment fade-in-then-semi-out"><a  href="https://docs.flutter.dev/get-started/install">Flutter</a>
        [Get the Flutter SDK, Windows setup[</span>
        <pre class="fragment fade-in-then-semi-out"><code class="hljs" data-trim data-noescape>
            PS C:\> flutter doctor
            Doctor summary (to see all details, run ...):
            [√] Flutter (Channel stable, 2.5.3, on Microsoft ...)
            [√] Android toolchain - develop for Android ...
            [√] Chrome - develop for the web
            [√] Android Studio (version 2020.3)
            [√] IntelliJ IDEA Ultimate Edition (version 2021.3)
            [√] Connected device (2 available)
        </code></pre>
    </li>
    <li class="fragment">Android Studio Plugins <code>Dart</code>/<code>Flutter</code></li>

</ol>

---

#### Hello World

```Dart
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class HelloWorldApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // abstract
    return MaterialApp(
      home: Text('Hello World'), // , -> Autoformatter
    );
  }
}
```

---

#### Android

* Einstellungen ->  
  (Telefoninfo -> Softwareinformation -> )   
  Buildnummer -> 7 mal tappen

<!-- .element: class="fragment" -->

* Developermodus aktiviert ✔️

<!-- .element: class="fragment" -->

* USB-Debugging ✔️

<!-- .element: class="fragment" -->

---
Flutter Inspector

---

#### Widget Tree

![widget](./assets/flutter/widget-tree-result.png)
<!-- .element: class="fragment" -->

![widget](./assets/flutter/widget-tree.png)
<!-- .element: class="fragment" -->

----

#### Widget Tree

![widget tree](./assets/flutter/widget-animation.gif)

----

#### `Widget`

<ul>
    <li class="fragment fade-in-then-semi-out">immutable description of UI</li>
    <li class="fragment fade-in-then-semi-out"><em>inflated</em> zu <code>Element</code>s</li>
    <li class="fragment fade-in-then-semi-out">
        <code>build()</code> updated oder deleted und inflated den Subtree neu
    </li>
</ul>

---

#### `Scaffold`

<img src="./assets/flutter/scaffold.png" height="600px">

----

#### Container

<iframe width="1062" height="598" src="https://www.youtube.com/embed/c1xLMaTUWCY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

----

#### `Container`/`Column`/`Row`

![container/row/column](./assets/flutter/container-vs-row-column.png)
<!-- .element: class="fragment" -->  

----
#### `ListView`

![listview/builder](./assets/flutter/listview-builder.png)
<!-- .element: class="fragment" -->  

Builder ⟹ Performance
<!-- .element: class="fragment" -->  

----
#### `Expanded`
<iframe width="1062" height="598" src="https://www.youtube.com/embed/_rnZaagadyo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

#### `StatefulWidget`

```Dart
class Counter extends StatefulWidget {
  @override
  _CounterState createState() => _CounterState();
}
```

<!-- .element: class="fragment" -->

```Dart
class _CounterState extends State<Counter> {
  int _count = 0;
```

<!-- .element: class="fragment" -->

```Dart
  @override
Widget build(BuildContext context) {
  return Row(
    children: [
      ElevatedButton(
        onPressed: _updateCount,
        child: Text('$_count'),
      ),
    ],
  );
}
```

<!-- .element: class="fragment" -->

```Dart
  void _updateCount() {
  setState(() => _count++);
}
```

<!-- .element: class="fragment" -->

----
![state](./assets/flutter/stateless-vs-stateful.png)
<!-- .element: class="fragment" -->

----

#### UI updated nicht

```Dart
class Changer extends StatefulWidget {
  @override
  _ChangerState createState() => _ChangerState();
}

class _ChangerState extends State<Changer> {
  String _text = 'Lorem';

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ElevatedButton(
          onPressed: () => _text = 'ipsum',
          child: Text('$_text'),
        ),
      ],
    );
  }
```

<!-- .element: class="fragment stretch" -->

kein Update ⟹ **`setState`**
<!-- .element: class="fragment" -->

----

#### `widget`

```Dart
class Changer extends StatefulWidget {
  final Object data;

  Changer(this.data);

  @override
  _ChangerState createState() => _ChangerState();
}

class _ChangerState extends State<Changer> {
  final myOwnData = widget.data;
  final myContext = context;

  ...
}
```

<!-- .element: class="fragment stretch" -->

<ul>
  <li class="fragment"><code>widget.x</code> greift auf Variable <code>x</code> im Widget zu</li>
  <li class="fragment"><code>context</code> liefert den Context des Widgets</li>

</ul>

---

#### State Management

Wohin mit dem State?
<!-- .element: class="fragment" -->

So tief wie möglich, so hoch wie nötig
<!-- .element: class="fragment" -->

----
![state](./assets/flutter/state.png)
<!-- .element: class="fragment" -->
`TextField` fügt zum `ListView` hinzu
<!-- .element: class="fragment" -->

notes:

* State in Column - Widget
* Function-Pointer an MyWidget

---

#### Widget Tree - Element Tree - Render Tree

![3 trees](./assets/flutter/internals_3_trees.png)

----

#### Widget Lifecycle

![lifecycle](./assets/flutter/lifecycle.png)
<!-- .element: class="fragment" -->

---

#### `? :`

```Dart
Container(
  child: input.isEmpty 
      ? Image.asset('assets/images/error.png')
      : Text('Yay'),
);
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Dart
Flex(
  children: [
    if(isPortrait) Widget(...)
```
<!-- .element: class="fragment" -->

---

#### Theme

```Dart
Text(
  'lorem ipsum',
  style: const TextStyle(
    color: Colors.blue,
    fontWeight: FontWeight.bold,
    fontSize: 16,
  ),
)
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

don't repeat yourself!
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Dart
MaterialApp(
  theme: ThemeData(
    fontFamily: 'Quicksand',
    colorScheme: ColorScheme.fromSwatch(
      primarySwatch: Colors.blue,
      accentColor: Colors.amber,
    ),
  )
);
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Dart
color: Theme.of(context).colorScheme.primary
```
<!-- .element: class="fragment" -->

---

#### Responsiveness

![responsiveness](./assets/flutter/responsiveness.png)
<!-- .element: class="fragment" -->

----

```Dart
MediaQuery.of(context).
size // width/height
    .textScaleFactor
    .orientation // portrait/landscape
    .viewInsets // onscreen-Keyboard etc.
```
<!-- .element: class="fragment" -->

👍 Speichern für mehr Performance👍
<!-- .element: class="fragment" -->

```Dart
@override
Widget build(BuildContext context) {
  final mediaQuery = MediaQuery.of(context);
```
<!-- .element: class="fragment" -->

----

#### Verwendung

```Dart
...children: [
  if (mediaQuery.orientation == Orientation.landscape) MyWidget(),
```
<!-- .element: class="fragment" -->

```Dart
  mediaQuery.size.width > 400 
      ? Row(...)
      :Widget,
```
<!-- .element: class="fragment" -->

```Dart
  Text(
    'text',
    style: TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 20 * mediaQuery.textScaleFactor,
      color: Theme.of(context).colorScheme.primary,
    ),
  ),
],
```

<!-- .element: class="fragment" -->

----

#### `LayoutBuilder`

<iframe width="1062" height="598" src="https://www.youtube.com/embed/IYDVcriKjsw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

#### Adaptiveness

![android-vs-ios](./assets/flutter/android-vs-ios.jpg)
<!-- .element: class="fragment" -->

----

#### Revenue

![android-vs-ios-revenue](./assets/flutter/android-vs-ios-revenue.jpg)
<!-- .element: class="fragment" -->

----

#### Implementierung

```Dart
Switch.adaptive(...) // Switch/CupertinoSwitch
```

<!-- .element: class="fragment" -->

```Dart
Platform.isIOS
  ? Container()
  : FloatingActionButton()
```
<!-- .element: class="fragment" -->

👍 kapseln in `MyWidget.adaptive()` 👍
<!-- .element: class="fragment" -->

---

#### Navigation

![stack](./assets/flutter/route-stack.png)
<!-- .element: class="fragment" -->

Top ist sichtbar
<!-- .element: class="fragment" -->

----

#### Simple Routing

```Dart
Navigator.of(context).push(
  MaterialPageRoute(
  builder: (context) =>
      OtherPage(data),
  ),
);
```
<!-- .element: class="fragment" -->

----

#### Named Routes

```Dart
class OtherPage extends StatelessWidget {
  static const route = '/other-page';

  @override
  Widget build(BuildContext context) {
    var data = ModalRoute
        .of(context)
        ?.settings
        .arguments as Data;
```

<!-- .element: class="fragment" -->

```Dart
MaterialApp(
  // home: const CategoriesPage(),
  routes: {
    '/': (context) => Home(),
    OtherPage.route: (context) => OtherPage(),
  },
  onUnknownRoute: (settings) {
    return MaterialPageRoute(builder: (context) => ErrorPage());
  },
```

<!-- .element: class="fragment" -->

```Dart
Navigator.of(context).pushNamed(
    OtherPage.route, arguments: arguments,);
```

<!-- .element: class="fragment" -->

----

#### Returning

![push-pop](./assets/flutter/push-pop.png)
<!-- .element: class="fragment" -->

---

#### Tabs

```Dart
static const _pages = [
  PageA(),
  PageB(),
];
```
<!-- .element: class="fragment" -->

```Dart
int _selectedPageIndex = 0;

void _selectPage(int index) {
  setState(() => _selectedPageIndex = index);
}
```
<!-- .element: class="fragment" -->

```Dart
Widget build(BuildContext context) {
  return Scaffold(
    body: _pages[_selectedPageIndex],
```
<!-- .element: class="fragment" -->

```Dart
    bottomNavigationBar: BottomNavigationBar(
      onTap: _selectPage,
      currentIndex: _selectedPageIndex,
      items: const [
        BottomNavigationBarItem(icon: Icon(Icons.category)),
        BottomNavigationBarItem(icon: Icon(Icons.star)),],),
```
<!-- .element: class="fragment" -->

---

#### State again
![state-problems](./assets/flutter/state-problems.png)
<!-- .element: class="fragment" -->

State forwarding ⟹ Kompletter Rebuild
<!-- .element: class="fragment" -->

----
$$UI = f(state)$$
![state-ui](./assets/flutter/state-ui.png)
<!-- .element: class="fragment" -->

---

#### Provider
```Dart
class DataProvider with ChangeNotifier {
  final items = <Data>[ ... ];
  
  void addItem(Data data) {
    ...
    notifyListeners();
  }

  Data findById( ... ) { ... }
}
```
<!-- .element: class="fragment stretch" -->

```Dart
@override
Widget build(BuildContext context) {
  return ChangeNotifierProvider(
    create: (context) => DataProvider(),
    child: MaterialApp( ... )
```
<!-- .element: class="fragment stretch" -->

```Dart
@override
Widget build(BuildContext context) {
  final dataProvider = Provider.of<DataProvider>(context, listen: true);
  final data = dataProvider.items;
```
<!-- .element: class="fragment stretch" -->

----
#### Advanced
`ListView`/`GridView` recyclen Elemente  
bzw wenn `builder` keinen Konstruktor aufruft
<!-- .element: class="fragment" -->

```Dart
ChangeNotifierProvider.value(
  value: data, 
  child: ...,
```
<!-- .element: class="fragment stretch" -->

Feingranularer
<!-- .element: class="fragment" -->

```Dart
Widget build(BuildContext context) {
  return Consumer<Data>(
    builder: (context, data, child) => FooWidget(foo: data, child: child),
    child: NotChanging(), //---^ not rebuilt
  );
}
```
<!-- .element: class="fragment stretch" -->  


