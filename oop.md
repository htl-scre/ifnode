# OOP
#### Object - oriented programming

---
#### Motivation
```Python
dev_names = []
dev_ages = []
current_name = input("Enter new developer's name: ")
current_age = input("Enter new developer's age: ")
dev_names.append(current_name)
dev_ages.append(current_age)

foo(dev_names[i], dev_ages[j])
```
<!-- .element: class="fragment" -->

* nicht skalierbar
<!-- .element: class="fragment" -->
* gefährlich
<!-- .element: class="fragment" -->

---
#### Object
<ul>
    <li class="fragment fade-in-then-semi-out">besteht aus Daten und Methoden</li>
    <li class="fragment">z.B. Developer</li>
        <dl>
            <div class="fragment">
                <dt>Daten</dt>
                <dd>Name</dd>
                <dd>Alter</dd>
                <dd>bekannte Sprachen</dd>
            </div>
            <div class="fragment">
                <dt>Methoden</dt>
                <dd>code()</dd>
                <dd>debug()</dd>
                <dd>fixBug()</dd>
            </div>
        </dl>
</ul>

----
#### Klasse
* Bauplan
* definiert Objekte der Klasse

```Java
class Developer {

    String name;
    int age;
    Language[] knownLanguages;

    Code code() {
        // code
    }

    Bug debug() {
        // code
    }

    void fixBug(Bug bug) {
        // code
    }
}
```
<!-- .element: class="fragment stretch" -->

----
#### Instanzierung
<ul>
    <li class="fragment fade-in-then-semi-out">
        Anlegen <strong>eines</strong> bestimmten Objektes
    </li>
    <li class="fragment fade-in-then-semi-out">
        nur über <code>new ClassName(params)</code>
    </li>
</ul>

```Java
Developer scre = new Developer();
scre.name = "Christoph";
scre.age = 42;
```
<!-- .element: class="fragment" -->

```Java
Developer refr = new Developer();
refr.name = "Franz";
```
<!-- .element: class="fragment" -->

```Java
System.out.println(scre.name);
System.out.println(refr.name);
```
<!-- .element: class="fragment" -->

```Plain Text
Christoph
Franz
```
<!-- .element: class="fragment" -->

----
#### Referenz
Variable, welche ein Objekt zugewiesen hat
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
Developer scre = new Developer();
scre.name = "Christoph";
Developer schreiber = scre;
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
schreiber.name = "Schreiber";
System.out.println(schreiber.name); -> <span class="fragment">"Schreiber"</span>
System.out.println(scre.name);      -> <span class="fragment">"Schreiber"</span>
</code></pre>

dasselbe Objekt (dieselbe *Instanz* der Klasse)
<!-- .element: class="fragment" -->

```Java
Developer refr = new Developer();
refr.name = "Franz";
```
<!-- .element: class="fragment" -->

<ul>
    <li class="fragment">1 Klasse: <code>Developer</code></li>
    <li class="fragment">2 Objekte: 2 x <code>new Developer()</code></li>
    <li class="fragment">3 Referenzen: <code>scre</code>, <code>schreiber</code>, <code>refr</code></li>
</ul>

---
#### Konstruktor
```Java
class Developer {
    
    String name;
    int age;    
    
    Developer(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
```
<!-- .element: class="fragment" -->

```Java
Developer scre = new Developer("Christoph", 42);
```
<!-- .element: class="fragment" -->

* zur Initialisierung
<!-- .element: class="fragment" -->
* heißt wie die Klasse
<!-- .element: class="fragment" -->

----
#### `this`
Referenz auf das entsprechende Objekt
<!-- .element: class="fragment" -->

```Java
void printName() {
    System.out.println(this.name);
}
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
Developer scre = new Developer("Christoph", 42);
Developer refr = new Developer("Franz", 42);
scre.printName();    -> <span class="fragment">"Christoph"</span>
refr.printName();    -> <span class="fragment">"Franz"</span>
</code></pre>

----
#### Konstruktorverkettung
```Java
Developer(String name, int age) {
    this.name = name;
    this.age = age;
}
```
<!-- .element: class="fragment" -->

```Java
Developer(String name) {
    this.name = name;
    age = 0;
}
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
Developer(String name) {
    this(name, 0);
}
```
<!-- .element: class="fragment" -->

----
#### Defaultkonstruktor
```Java
class Dog {
}
```
<!-- .element: class="fragment" -->

```Java
new Dog();
```
<!-- .element: class="fragment" -->

hat eine Klasse **keinen** Konstruktor, wird ein parameterloser ergänzt
<!-- .element: class="fragment" -->

```Java
class Dog {
    public Dog() {
    }
}
```
<!-- .element: class="fragment" -->

----
#### `toString`
```Java
Developer scre = new Developer("Christoph", 42);
System.out.println(scre);
```
<!-- .element: class="fragment" -->

```Plain Text
Developer@490ab905
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```
class Developer {

    String name;
    int age;
    ...
    
    public String toString() {
        return name + ", " + age;
    }
}
```
<!-- .element: class="fragment" -->

```Plain Text
Christoph, 42
```
<!-- .element: class="fragment" -->

---
#### 🐍
```Python
class Developer:
    pass
```
<!-- .element: class="fragment" -->

```Python
scre = Developer()
```
<!-- .element: class="fragment" -->

```Python
class Developer:

    def __init__(self, name, age=0):
        self.name = name
        self.age = age
```
<!-- .element: class="fragment" -->

```Python
scre = Developer('scre', 42)
refr = Developer('refr')
print(scre, scre.name)
```
<!-- .element: class="fragment" -->

```Plain Text
&ltdeveloper.Developer object at 0x0000027863986588> scre
```
<!-- .element: class="fragment" -->

```Python
def __str__(self) -> str:
    return f'{self.name}, {self.age}'
```
<!-- .element: class="fragment" -->

```Plain Text
scre, 42
```
<!-- .element: class="fragment" -->

---
#### Statische Member
```Java
class Developer {
    static int count;

    static Developer[] getAll() { ... }

    Developer(String name, int age) {
        this.name = name;
        this.age = age;
        count++;
    }    
}
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
Developer scre = new Developer("Christoph", 42);
Developer refr = new Developer("Franz", 42);
System.out.println(Developer.count + " " + refr.name); -> <span class="fragment">2 Franz</span>
</code></pre>

```Java
Developer[] devs = Developer.getAll();
```
<!-- .element: class="fragment" -->

```Java
instance.instanceMethod()
instance.variable
Class.staticMethod()
Class.staticVariable
```
<!-- .element: class="fragment" -->

---
#### Access Modifier
Implementierungsdetails sind zu verbergen
<!-- .element: class="fragment" -->

||eigene Klasse|eigenes Package|Subklassen|Jeder|
|---:|:---:|:---:|:---:|:---:|
|private    |✔️|🚫   |🚫   |🚫|
|           |✔️|✔️|🚫   |🚫|
|protected  |✔️|✔️|✔️|🚫|
|public     |✔️|✔️|✔️|✔️|
<!-- .element: class="fragment" -->

<ul>
    <li class="fragment"><strong>alle</strong> Instanzvariablen <code>private</code></li>
    <li class="fragment">Methoden so wenig wie nötig</li>
</ul>

notes: mit *modules* können selektiv *packages* exportiert werden

----
#### Setter/Getter
```
class Developer {
    
    private String name;
    private boolean responsible;
    ...
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;   
    }
    
    public boolean isResponsible() {
        return responsible;
    }
}
```
<!-- .element: class="fragment stretch" -->

wenn **nötig** ⟹ Kapselung
<!-- .element: class="fragment" -->