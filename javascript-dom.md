## Javascript & Browser

---
#### window
![window](./assets/javascript-dom/windowObjects.svg)
<!-- .element: class="fragment" -->

kapselt Browserfenster
<!-- .element: class="fragment fade-in-then-semi-out" -->
```Javascript
window.outerHeight; -> 1080 
```
<!-- .element: class="fragment" -->
note: window.height

---
#### BOM - BrowserObjectModel
Daten für Browser
<!-- .element: class="fragment fade-in-then-semi-out" -->

<ul>
    <li class="fragment fade-in-then-semi-out">welcher Browser? -> <code>navigator.userAgent</code></li>
    <li class="fragment fade-in-then-semi-out">welche Version?</li>
    <li class="fragment fade-in-then-semi-out">welches OS? -> <code>navigator.platform</code></li>
    <li class="fragment fade-in-then-semi-out">aktuelle URL -> <code>location</code></li>
    <li class="fragment fade-in-then-semi-out"><code>alert</code>/<code>confirm</code>/<code>prompt</code></li>
</ul>

```Javascript
if(prompt('Go to HTL', 'yes') === 'yes')
    location = 'https://www.htlstp.ac.at'
```
<!-- .element: class="fragment" -->

---
![window](./assets/javascript-dom/dom.png)
<!-- .element: style="height:90vh" class="fragment" -->

note: Im Browser herzeigen

----
![window](./assets/javascript-dom/dom-links.svg)
<!-- .element: style="height:170vh" class="fragment"-->

----
```html
&lt;html>
&lt;body>
  <div>Users:</div>
  <ul>
    <li>John</li>
    <li>Pete</li>
  </ul>
&lt;/body>
&lt;/html>
```
<!-- .element: class="fragment" -->

```javascript
let liPete = $0;
liPete...; -> 'Users:'
```
<!-- .element: class="fragment" -->

---
#### getting Elements
<dl>
    <dt class="fragment"><code>document.getElementById(id)</code></dt>
    <dd class="fragment fade-in-then-semi-out"><pre><code class="hljs javascript" data-trim data-noescape>
let name = document.getElementById('name');
    </code></pre>
    </dd>
    <dt class="fragment"><code>element.querySelectorAll(css)</code></dt>
    <dd class="fragment fade-in-then-semi-out">returnt alle Elemente in <code>element</code>, welche den css-Selector <code>css</code> erfüllen</dd>
    <dd class="fragment fade-in-then-semi-out"><pre><code class="hljs javascript" data-trim data-noescape>
let images = list.querySelectorAll('.left > img');
    </code></pre>
    </dd>
<dt class="fragment"><code>element.querySelector(css)</code></dt>
    <dd class="fragment fade-in-then-semi-out"><code>=element.querySelectorAll(css)[0]</code></dd>
    <dd class="fragment"><pre><code class="hljs javascript" data-trim data-noescape>
let logo = list.querySelector('img#logo');
    </code></pre>
    </dd>
</dl>

---
#### Manipulationen
```html
<input type="text" id="name" value="Bob">
<a href="http://www.google.com">Google</a>
<p>can't touch this</p>
```
<!-- .element: class="stretch fragment" -->
```javascript
let input = document.getElementById('name');
input.id = 'alias';
input.value = 'Destroyah';
```
<!-- .element: class="stretch fragment fade-in-then-semi-out" -->
```javascript
let link = document.querySelector('a[href="https://www.google.com"]');
link.href = 'https://www.htlstp.ac.at';
link.textContent = 'HTL St. Pölten';
```
<!-- .element: class="stretch fragment fade-in-then-semi-out" -->
```javascript
let paragraphs = document.querySelectorAll('p');
for (let p of paragraphs) {
    p.hidden = false;
    p.textContent = 'touched';
    p.remove(); // overkill
}
```
<!-- .element: class="stretch fragment" -->

----
```html
<ol>
  <li>html</li>
  <li>css</li>
</ol>
```
<!-- .element: class="fragment" -->
```javascript
let ol = document.querySelector('ol');
ol.innerHTML += '<li>javascript</li>';
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```javascript
let ol = document.querySelector('ol');
let li = document.createElement('li');
li.className = 'item';
li.textContent = 'javascript';
ol.append(li);
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

![Möglichkeiten des Einfügens](./assets/javascript-dom/before-prepend-append-after.svg)
<!-- .element: class="fragment" -->

note: `+=` schreibt/lädt ganze Seite neu

----
#### Style-Manipulation
```html
<p class='red huge'>lorem ipsum</p>
```
<!-- .element: class="fragment" -->

```javascript
let p = document.querySelector('p');
p.style.color = 'blue';
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```javascript
p.classList.remove('red');
p.classList.add('blue');
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

👍 Single Responsibility Principle 👍
<!-- .element: class="fragment fade-in-then-semi-out" -->

```javascript
p.classList.toggle('huge');
```
<!-- .element: class="fragment" -->

---
#### Events
```html
<button id="button" onclick="handleClick()">X</button>
```
<!-- .element: class="fragment" -->

```javascript
function handleOnClick(event) {
    //... this = getriggertes Element    
}
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```javascript
document.body.onkeydown = handleKeyPress; // ⚠️ ohne () ⚠️
function handleKeyPress(event) {
    // event.key
}
```
<!-- .element: class="fragment" -->

<ul>
    <li class="fragment fade-in-then-semi-out"><code>contextmenu</code></li>
    <li class="fragment fade-in-then-semi-out"><code>mousemove</code></li>
    <li class="fragment fade-in-then-semi-out"><code>submit</code></li>
    <li class="fragment fade-in-then-semi-out"><code>transitioned</code></li>
    <li class="fragment">...</li>
</ul>