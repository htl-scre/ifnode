# Spring Rest Repositories

---

#### Motivation

REST - APIs 95% Boilerplate
<!-- .element: class="fragment" -->

```Java
@RestController
@AllArgsConstructor
public class Api {

    private final Repository repository; 
    private final Service service;

    @GetMapping
    public Optional<Entity> findById() { ... }

    @GetMapping
    public List<Entity> getAll() { ... }

    @GetMapping
    public List<Entity> query(condition) {
        return repository.query(condition);
    }

    @PostMapping
    public Entity save() { ... }
```
<!-- .element: class="stretch fragment" -->

---

#### Repository

```Java
public interface StudentRepository
        extends CrudRepository<Student, Long> { ...
}
```
<!-- .element: class="fragment" -->

+
<!-- .element: class="fragment" -->

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-rest</artifactId>
</dependency>
```
<!-- .element: class="fragment" -->

= GET /students ⟹
<!-- .element: class="fragment" -->

```json
{
  "_embedded": {
    "students": [
      ...
```
<!-- .element: class="fragment" -->

---
#### Customization

```Java

@RepositoryRestResource(
        collectionResourceRel = "students-rel",
        path = "students-path")
public interface StudentRepository
        extends CrudRepository<Student, Long> { ...
}
```
<!-- .element: class="fragment" -->

```json
"_links": {
  "students-rel": {
  "href": "http://localhost:8080/students-path",
  "templated": true
}
...
```
<!-- .element: class="fragment" -->

----
```Java
public interface StudentRepository 
        extends CrudRepository<Student, Long> {

    @Override
    @RestResource(exported = false) // internal use only -> 405
    void deleteById(Long id);

    List<Student> findAllByName(String name);
}
```
<!-- .element: class="fragment" -->

GET /students
<!-- .element: class="fragment" -->

```json
{
  "_embedded": {
    "students": [
      ...
    ],
    "_links": {
      ...,
      "search": {
        "href": "http://localhost:8080/students/search"
      }
```
<!-- .element: class="fragment" -->

----
GET /students/search
<!-- .element: class="fragment" -->

```json
"_links": {
  "findAllByName": {
    "href": "http://localhost:8080/students/search/findAllByName{?name}",
    "templated": true
  },
  "self": {
    "href": "http://localhost:8080/students/search"
  }
```
<!-- .element: class="fragment stretch" -->

GET /students/search/findAllByName?name=Alfred
<!-- .element: class="fragment" -->

```json
"_embedded": {
  "students": [
    {
      "name": "Alfred",
      ...
    }
  ]
},
...
```
<!-- .element: class="fragment stretch" -->

---
#### `@Override`

```Java
@RepositoryRestController   // nicht RestController
@AllArgsConstructor
public class StudentController {

    private final StudentRepository studentRepository;

    @GetMapping("students/{id}")
    @ResponseBody
    ResponseEntity<EntityModel<Student>> serveId1(@PathVariable Long id) {
        var student = studentRepository
                .findById(1L)
                .orElseThrow();

        var resource = EntityModel.of(student);
        var link = linkTo(methodOn(StudentController.class).serveId1(1L));
        resource.add(link.withSelfRel());

        return ResponseEntity.ok(resource);
    }
}
```
<!-- .element: class="fragment stretch" -->