# Rekursion
![Google recursion](./assets/recursion/recursion-google.png)

---
#### Motivation
Die Fibonacci-Folge 
$$0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89,144,\dots$$
<!-- .element: class="fragment" -->
* $$ F_0 = 0, F_1 = 1 $$
<!-- .element: class="fragment" -->
* $$ F_n = F_{n-1} + F_{n-2} $$
<!-- .element: class="fragment" -->


----
![Fibonacci-Spirale](./assets/recursion/fibonacci-spiral.svg)
<!-- .element: class="fragment" -->
![Pascal-Dreieck](./assets/recursion/pascal-dreieck.png)
<!-- .element: style="height:8em" class="fragment"-->

----
#### $$F_4?$$
$$ F_0 = 0, F_1 = 1 $$
<!-- .element: class="fragment" data-fragment-index="1"-->
$$ F_n = F_{n-1} + F_{n-2} $$
<!-- .element: class="fragment" data-fragment-index="1"-->

$$ F_4 \rightarrow F_3, F_2 \rightarrow F_2, F_1 \rightarrow F_1, F_0$$
<!-- .element: class="fragment" -->
$$ F_2 = F_1 + F_0 = 1 + 0 = 1 $$
<!-- .element: class="fragment" -->
$$ F_3 = F_2 + F_1 = 1 + 1 = 2 $$
<!-- .element: class="fragment" -->
$$ F_4 = F_3 + F_2 = 2 + 1 = 3 $$
<!-- .element: class="fragment" -->

----
#### Code
```Java
public static long fibonacci(int n) {
    if (n < 0)
        throw new IllegalArgumentException(
                "Can only compute for positive numbers. Given: " + n);
    if (n < 2)
        return n;
    return fibonacci(n - 1) + fibonacci(n - 2);
}
```
<!-- .element: class="fragment stretch" -->

```Java
fibonacci(7);
```
<!-- .element: class="fragment stretch" -->

---
## Call Stack
Methoden sind am Stack bis sie verlassen werden
<!-- .element: class="fragment" -->
```Java
static void preparePancakes() {
    makeDough();
    heatPan();
    putDoughInPan();
}

static void makeDough() {
    addIngredients();
    stir();
}

static void stir() { ... }
```
<!-- .element: class="fragment stretch" -->
![stack](./assets/recursion/stack.png)
<!-- .element: class="fragment" -->

----
```Java
public static long fibonacci(int n) {
    if (n < 0)
        throw new IllegalArgumentException(
                "Can only compute for positive numbers. Given: " + n);
    if (n < 2)
        return n;
    return fibonacci(n - 1) + fibonacci(n - 2);
}
```
<!-- .element: class="stretch" -->

```Java
fibonacci(7);
```
<!-- .element: class="fragment stretch" -->

![stack](./assets/recursion/stack-fibonacci.png)
<!-- .element: class="fragment" -->

---
#### StackOverflow
```Java
static void recursive() {
    recursive();
}
```
<!-- .element: class="fragment" -->


```Plain Text
Exception in thread "main" java.lang.StackOverflowError
	at recursion.StackDemo.recursive(StackDemo.java:10)
	at recursion.StackDemo.recursive(StackDemo.java:10)
	at recursion.StackDemo.recursive(StackDemo.java:10)
	at recursion.StackDemo.recursive(StackDemo.java:10)
	at recursion.StackDemo.recursive(StackDemo.java:10)
```
<!-- .element: class="fragment exception" -->

---
#### Faktorielle
$$ n! = n \cdot (n-1) \cdot (n-2) \cdot \dots 1$$
<!-- .element: class="fragment" -->
$$ 4! = 4 \cdot 3 \cdot 2 \cdot 1 = 24 $$
<!-- .element: class="fragment" -->

```Java
static long factorial(int n) {
    if (n < 0)
        throw new IllegalArgumentException(
                "Can only compute for positive numbers. Given: " + n);
    if (n < 2)
        return 1;
    return n * factorial(n-1);
}
```
<!-- .element: class="fragment stretch" -->
```Java
long result = 1;
for (int i = n; i > 0; i--)
    result *= i;
return result;
```
<!-- .element: class="fragment stretch" -->


---
#### Best practice
<ul>
    <li class="fragment">anwenden, wenn <em>natürliche</em> Lösung</li>
    <li class="fragment">Performance mies</li>
    <li class="fragment">Umwandlung in nicht-rekursive Lösung?</li>
</ul>

---
#### Fibonacci iterativ
```Java
public static long fibonacci(int n) {
    if (n < 0)
        throw new IllegalArgumentException(
                "Can only compute for positive numbers. Given: " + n);
    if (n < 2)
        return n;
    long parent = 1;
    long grandparent = 0;
    long curr = -1;
    for (int i = 2; i <= n; i++) {
        curr = parent + grandparent;
        grandparent = parent;
        parent = curr;
    }
    return curr;
}
```
<!-- .element: class="fragment stretch" -->

---
#### Fibonacci mathematisch
$$ F_n = F_{n-1} + F_{n-2} $$
<!-- .element: class="fragment" -->
$$ x^n = x^{n-1} + x^{n-2} | : x^{n-2} $$
<!-- .element: class="fragment" -->
$$ x^2 = x + 1 $$
<!-- .element: class="fragment" -->
$$ x_1 = \phi = \frac{1 + \sqrt{5}}{2}, x_2 = \psi = \frac{1 - \sqrt{5}}{2} $$
<!-- .element: class="fragment" -->
$$ \Rightarrow \phi^n = \phi^{n-1} + \phi^{n-2}, \psi^n = \psi^{n-1} + \psi^{n-2} $$
<!-- .element: class="fragment" -->

----
$$ F_n = a\phi^n + b\psi^n $$
<!-- .element: class="fragment" -->
$$ F_0 = 0 \implies 0 = a + b $$
<!-- .element: class="fragment" -->
$$ F_1 = 1 \implies 1 = a\phi + b\psi $$
<!-- .element: class="fragment" -->
$$ \Rightarrow a = \frac{1}{\sqrt{5}}, b= -\frac{1}{\sqrt{5}}$$
<!-- .element: class="fragment" -->
$$ F_n = \frac{\phi^n-\psi^n}{\sqrt{5}} $$
<!-- .element: class="fragment" -->