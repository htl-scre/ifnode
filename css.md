<style>
@import url("https://dev-cats.github.io/code-snippets/JetBrainsMono.css");
.box {
    position:relative;
    width:50px;
    height:50px;
    left:20%;
}
</style>

# CSS

---
#### Einbindung
<ul>
    <li class="fragment fade-in-then-semi-out">
    inline
    <pre><code style="width: 600px" class="hljs html">
<p style="color: red">mies</p></code></pre></li>
    <li class="fragment fade-in-then-semi-out">
        <code>&ltstyle></code>
    <pre><code style="width: 600px" class="hljs html" data-trim>
&lthead>
    <style>
        p {
            color: red;
        }   
    </style>
    </code></pre></li>
    <li class="fragment">
    .css Datei <span class="fragment">👍</span>
    <pre><code style="width: 600px" class="hljs html" data-trim>
&lthead>
    <link rel="stylesheet" href="styles.css"></code></pre></li>
</ul>

---
#### Rules
```css
selector {
    property: value;
    property: value;
    property: value;
}
```
<!-- .element: class="fragment" -->

* eine Rule gilt für alle selektierten Elemente
<!-- .element: class="fragment" -->
* setzt die angeführten properties
<!-- .element: class="fragment" -->
* values können durch spezifischere selectors überschrieben werden
<!-- .element: class="fragment" -->

----
```css
.text1 {
    font-size: 20px;
}

#menu-bar {
    float: left;
    color: #BBB529;
}

@media (max-width: 1199px) {
    font-size: 16px;
}

body {
    background: url("barn.jpg"), 
        url("stars.jpg"), 
        linear-gradient(
            rgba(0, 0, 255, 0.5), rgba(255, 255, 0, 0.5)
        );
}

h1 {
    background–color: purple;
}
```
<!-- .element: class="stretch" -->

---
#### Simple selectors
**alle** Elemente eines Typs
<!-- .element: class="fragment" -->

```css
html-tag { }
p { }
```
<!-- .element: class="fragment" -->

ein Element mit einer bestimmten id (**unique**)
<!-- .element: class="fragment" -->
```css
#id { }
#next-button { }
```
<!-- .element: class="fragment" -->

```html
<button id="next-button">Next</button>
```
<!-- .element: class="fragment" -->

alle Elemente einer Klasse
<!-- .element: class="fragment" -->

```css
.class { }
.menu-item { }
```
<!-- .element: class="fragment" -->

```html
<a class="menu-item" href="help.html">Help</a>
```
<!-- .element: class="fragment" -->
----
ein *child* innerhalb eines *parents*
<!-- .element: class="fragment" -->

```css
parent child { }
li em { }
```
<!-- .element: class="fragment" -->

ein *second* direkt nach einem *first*
<!-- .element: class="fragment" -->
```css
first + second { }
h1 + p { }
```
<!-- .element: class="fragment" -->

*alle* Elemente
<!-- .element: class="fragment" -->
```css
* { }
```
<!-- .element: class="fragment" -->

---
#### Wichtige properties
Schriftfarbe
<!-- .element: class="fragment" -->
```css
color: red;
color: #FF00FF;
```
<!-- .element: class="fragment" -->

Rahmen
<!-- .element: class="fragment" -->
```css
border: 2px solid #123456;
border: breite typ farbe
```
<!-- .element: class="fragment" -->

Höhe / Breite
<!-- .element: class="fragment" -->
```css
width: 200px;
height: auto;
```
<!-- .element: class="fragment" -->

----
#### Background
```css
background: grey;
```
<!-- .element: class="fragment" -->
<div style="background:grey" class="box fragment"></div>

```css
background: url('https://www.htlstp.ac.at/logo') grey;
```
<!-- .element: class="fragment" -->
<div style="background: url('https://www.htlstp.ac.at/logo') grey" 
class="box fragment"></div>


```css
background: center/100% url('https://www.htlstp.ac.at/logo') grey;
```
<!-- .element: class="fragment" -->
<div style="background: center/100% url('https://www.htlstp.ac.at/logo') grey" 
class="box fragment"></div>

```css
background: no-repeat 
    center/100% 
    url('https://www.htlstp.ac.at/logo') 
    grey;
```
<!-- .element: class="fragment" -->
<div style="background: no-repeat center/100% url('https://www.htlstp.ac.at/logo') grey" 
class="box fragment"></div>

----
#### Font
```css
font: 1em "Comic Sans MS";
```
<!-- .element: class="fragment" -->
<p style="font: 1em 'Comic Sans MS'" class="fragment">1em = aktuelle Fontgröße</p>

```css
font: 2em "JetBrains Mono";
```
<!-- .element: class="fragment" -->
<p style="font: 2em 'JetBrains Mono' " class="fragment">1em default = 16px</p>

```css
font: bold 32px  "Courier New";
```
<!-- .element: class="fragment" -->
<p style="font: bold 32px 'Courier New'" class="fragment">32px, immer; 4k Displays...</p>

---
#### State-based Styling
```css
a:link {
    color: dodgerblue;
}

a:visited {
    color: green;
}

a:hover {
    font-size: 1.5em;
    text-decoration: none;
}
```