# REST

---
#### REpresentational State Transfer

<dl>
    <dt class="fragment fade-in-then-semi-out">Client-Server Trennung</dt>
    <dt class="fragment fade-in-then-semi-out">Stateless</dt>
        <dd class="fragment fade-in-then-semi-out">Kurze Request-Response Kommunikation</dd>
    <dt class="fragment fade-in-then-semi-out">Einheitliches Interface</dt>
        <dd class="fragment fade-in-then-semi-out">URI - Uniform Resource Identifier</dd>
        <dd class="fragment fade-in-then-semi-out">Resourcen getrennt von ihrer Repräsentation</dd>
    <dt class="fragment fade-in-then-semi-out">Cachebar</dt>
    <dt class="fragment fade-in-then-semi-out">Zugriff üblicherweise über HTTP</dt>
</dl>

note: https://www.boredapi.com/

---
#### HTTP
![http](./assets/REST/http.png)
<!-- .element: class="fragment"-->

<div class="fragment">URL - Uniform Resource Locator
<pre><code class="hljs http request" data-trim data-noescape>
protocol://hostname:port/path?field=value
</code></pre>
</div>
note: defaultport 80 für http, Request Parameter für GET

----
#### Request
```http request
GET http://www.htlstp.ac.at/logo
Accept: image/webp, image/apng, image/*, */*;q=0.8
```
<!-- .element: class="fragment fade-in-then-semi-out"-->

```http request
POST http://www.htlstp.ac.at/api/teachers/
Content-Type: application/json

{
  "name": "SCRE",
  "dept": "IF"
}
```
<!-- .element: class="fragment"-->

----
#### Response
```http request
HTTP/1.1 200 OK
Server: nginx
Date: Mon, 18 May 2020 18:08:47 GMT
Content-Type: image/png
Content-Length: 4663
Connection: keep-alive
Expires: Mon, 25 May 2020 04:35:30 +0200

PNG

IHDR  
N£%É;%ÚCl³Úí:º0ºRî`Ýze++¡·1Úqõ.¥ôBëm7¹·îd%á
...
```

---
#### Request Methoden
<dl>
    <dt class="fragment">GET</dt>
    <dd class="fragment"><pre><code class="hljs http request" data-trim data-noescape>
        GET http://www.appdomain.com/users
        GET http://www.appdomain.com/users/123
        GET http://www.appdomain.com/users?size=20&page=5
    </code></pre></dd>
    <dd class="fragment">lädt die angegebene Resource</dd>
    <dd class="fragment">sollte bei jedem Aufruf dasselbe Resultat liefern</dd>
    <dt class="fragment">DELETE</dt>
    <dd class="fragment"><pre><code class="hljs http request" data-trim data-noescape>
        DELETE http://www.appdomain.com/users/123
    </code></pre></dd>
    <dd class="fragment">löscht die angegebene Resource</dd>
</dl>
note: DELETE auf Collection-Resource bei Bedarf

----
<dl>
    <dt class="fragment">POST</dt>
    <dd class="fragment"><pre><code class="hljs http request" data-trim data-noescape>
        POST http://www.appdomain.com/users
        Content-Type: application/json<br>
        {resource}
    </code></pre></dd>
    <dd class="fragment">Resource im RequestBody speichern</dd>
    <dd class="fragment">Response sollte URI der angelegten Resource sein</dd>
    <dt class="fragment">PUT</dt>
    <dd class="fragment"><pre><code class="hljs http request" data-trim data-noescape>
        PUT http://www.appdomain.com/users/123
        Content-Type: application/json<br>
        {resource}
    </code></pre></dd>
    <dd class="fragment">Resource im RequestBody <em>unter Request-URI</em> updaten/speichern</dd>
</dl>
note: POST auch bei Requests mit vielen Daten statt Request Parameter  
PATCH ebenfalls möglich für teilweises Updaten

----
#### Status Codes
<dl>
    <dt class="fragment fade-in-then-semi-out">200 - OK</dt>
    <dt class="fragment fade-in-then-semi-out">400 - Bad Request</dt>
        <dd class="fragment fade-in-then-semi-out">Request fehlerhaft, später sicher Fehler</dd>
        <dd class="fragment fade-in-then-semi-out">Besellt Kebab bei McDonalds</dd>
    <dt class="fragment fade-in-then-semi-out">500 - Internal Server Error</dt>
        <dd class="fragment fade-in-then-semi-out">Request ok, Serverfehler, später vielleicht Erfolg</dd>
        <dd class="fragment fade-in-then-semi-out">Bestellt Pommes, die sind noch nicht fertig</dd>
    <dt class="fragment fade-in-then-semi-out">201 - Created</dt>
    <dt class="fragment fade-in-then-semi-out">204 - No Content</dt>
        <dd class="fragment fade-in-then-semi-out">nach DELETE</dd>
    <dt class="fragment">404 - Not Found</dt>
</dl>

----
#### Evolution
![rest_evolution](./assets/REST/REST_evolution.png)

Hypermedia as the Engine of Application State
<!-- .element: class="fragment"-->

---
## Spring

----
#### Inversion of Control
![library vs framework](./assets/REST/framework_vs_library.png)

---
<dl>
    <div class="fragment fade-in-then-semi-out">
        <dt>Spring Boot</dt>
        <dd>Erledigt Setup</dd>
    </div>
    <div class="fragment fade-in-then-semi-out">
        <dt>Spring Core</dt>
        <dd>Inversion of Control</dd>
        <dd>Dependency Lookup/Injection</dd>
    </div>
    <dt class="fragment fade-in-then-semi-out">Spring Web</dt>
    <dt class="fragment">Spring Data JPA</dt>
</dl>
note: Convention over configuration, opinionated

----
#### Inversion of Control / Dependency Injection
```Java
public class TextEditor {
    private SpellChecker checker;

    public TextEditor() {
        this.checker = new EnglishSpellChecker();
    }
}
```
<!-- .element: class="fragment fade-in-then-semi-out"-->
"new is glue"
<!-- .element: class="fragment fade-in-then-semi-out"-->
```Java
public class TextEditor {
    private SpellChecker checker;

    public TextEditor(SpellChecker checker) {
        this.checker = checker;
    }
}
```
<!-- .element: class="fragment"-->

note: Setter Injection, Field Injection

---
#### DI - Container
![IoC-Container](./assets/REST/container-magic.png)
 
* Implementierung <!-- .element: class="fragment fade-in-then-semi-out"--> von `ApplicationContext` 
* managed <!-- .element: class="fragment fade-in-then-semi-out"--> konfigurierte Objekte(*beans*)
 
----
```Java
@Configuration
public class AppConfig {

    @Bean
    public MyBean myBean() {
        // instantiate, configure and return bean ...
    }
}
```

---
#### IntelliJ
![new project](./assets/REST/new_project.png)

----
![dependencies](./assets/REST/dependencies.png)

---
<h4><code class="yellow">@SpringBootApplication</code></h4>

```Java
package rest;

@SpringBootApplication
public class App {

    public static void main(String[] args) {
        var appContext = SpringApplication.run(App.class, args);
    }
```
<dl>
    <div class="fragment fade-in-then-semi-out">
        <dt><code class="yellow">@EnableAutoConfiguration</code></dt>
        <dd>Baut den IoC-Container auf etc.</dd>
    </div>
    <div class="fragment fade-in-then-semi-out">
        <dt><code class="yellow">@Configuration</code></dt>
        <dd>In dieser Klasse <code class="yellow">@Bean</code> suchen</dd>
    </div>
    <div class="fragment">
        <dt><code class="yellow">@ComponentScan</code></dt>
        <dd>In Subpackages (<code>rest.*</code>) <code class="yellow">@Component</code> suchen</dd>
    </div>
</dl>

note: `@Component` - Klasse ist Bean, @Bean - Methode returnt Bean

----
```Java
@SpringBootApplication
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Bean
    void printHelloWorld() {
        System.out.println("Hello World");
    }
}
```
```Java
@Component
class MyComponent {

    public MyComponent() {
        System.out.println("Constructing Component");
    }
}
```
<!-- .element: class="fragment"-->

```plaintext
Constructing Component
Hello World
```
<!-- .element: class="fragment"-->

----
#### Beans
```Java
@RequestScope
@Component
public class Logger {

    private final Path log;

    public Logger(Path logDirectory) throws IOException {
        log = Files.createTempFile(logDirectory, "request", "log");
    }

    @PreDestroy
    private void deleteFile() throws IOException {
        Files.delete(log);
    }
}
```
<!-- .element: class="stretch"-->
```Java
@PreDestroy
private void preDestroy() {
    this.close();
    this.shutdown();
}
```
<!-- .element: class="fragment stretch"-->

----
#### Scope
<dl>
    <dt class="fragment"><code class="yellow">@Scope("singleton")</code></dt>
    <dd class="fragment fade-in-then-semi-out">default</dd>
    <dd class="fragment fade-in-then-semi-out">eine Instanz pro <code>ApplicationContext</code></dd>
    <dt class="fragment"><code class="yellow">@Scope("prototype")</code></dt>
    <dd class="fragment fade-in-then-semi-out">eine Instanz pro Injection</dd>
    <dt class="fragment"><code class="yellow">@RequestScope</code></dt>
    <dd class="fragment fade-in-then-semi-out">eine Instanz pro HTTP Request</dd>
    <dt class="fragment"><code class="yellow">@SessionScope</code></dt>
    <dd class="fragment">eine Instanz pro HTTP Session</dd>
</dl>

note: 
* Singleton GOF - @ApplicationScope - 1 pro JVM

---
#### Entity
```Java
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@Entity
public class Student extends AbstractPersistable<Long> 
        implements Serializable {

    @NotBlank
    private String name;

    @Past
    @NotNull
    private LocalDate dateOfBirth;
}
```
<!-- .element: class="fragment stretch"-->

* `AbstractPersistable` überschreibt `equals/hashCode`
* id in Entity definieren für mehr Kontrolle

---
#### Repository
```Java
@Entity
public class Student {
    @Id
    @GeneratedValue
    private Integer id;

    protected Student() {
    }
}
```
<!-- .element: class="fragment"-->

```Java
public interface StudentRepository extends 
        JpaRepository<Student, Integer> {
```
<!-- .element: class="fragment"-->

```Java
@Repository     // Alias für @Component
@Transactional(readOnly = true)
public class SimpleJpaRepository<T, ID> 
        implements JpaRepositoryImplementation<T, ID>
```
<!-- .element: class="fragment"-->

Zur Laufzeit wird eine Klasse von `SimpleJpaRepository` abgeleitet und injected
<!-- .element: class="fragment"-->

note: Eigene **Implementierungen** eines Repositories sollten mit `@Repository` annotiert werden

----
```Java
public interface StudentRepository 
        extends JpaRepository<Student, Integer> {

    Student findStudentByName(String name);
    
    Stream<Student> findAllByNameContaining(String substring); 

    @Query("select s from Student s where s.id = 1")
    Student findFirstStudent();
}
```

----
#### `CommandLineRunner`
```Java
@Configuration
public class DatabaseSetup {

    @Bean
    CommandLineRunner saveStudents(StudentRepository repository) {
        return args -> {
            repository.save(new Student("Alfred", 1));
            repository.save(new Student("Bernd", 2));
        };
    }
}
```

<ul>
    <li class="fragment fade-in-then-semi-out">Bean wird bei der Konfiguration erzeugt</li>
    <li class="fragment fade-in-then-semi-out"><code>StudentRepository</code> wird injected</li>
    <li class="fragment"><code>CommandLineRunner</code> wird ausgeführt</li>
</ul>

---
#### Controller
```Java
@Controller
@RequestMapping("path")
public class MyController {

    @GetMapping("/entity")
    public HttpEntity<Student> responseEntity() {
        return new ResponseEntity<>(new Student("", 0), HttpStatus.OK);
    }
}
```
<!-- .element: class="fragment stretch"-->

<dl>
    <dt class="fragment"><code class="yellow">@Controller</code></dt>
    <dd class="fragment fade-in-then-semi-out">Alias für <code class="yellow">@Component</code></dd>
    <dd class="fragment fade-in-then-semi-out">Kontrolliert Http Requests</dd>
    <dt class="fragment"><code class="yellow">@RequestMapping</code></dt>
    <dd class="fragment">alle HTTP Methoden an <span class="monospace">www.server.com/path</span></dd>
</dl>

----
```Java
@GetMapping("/entity")
public HttpEntity<Student> one() {
    return new ResponseEntity<>(STUDENT, HttpStatus.OK);
}
```
<!-- .element: class="fragment"-->
<dl style="width:90%">
    <dt class="fragment"><code class="yellow">@GetMapping</code></dt>
    <dd class="fragment fade-in-then-semi-out"><span class="monospace">GET</span>-Requests an <span class="monospace">/path/entity</span></dd>
    <dt class="fragment"><code>HttpEntity&lt;Body></code></dt>
    <dd class="fragment fade-in-then-semi-out">kapselt HTTP Response und Status-Code</dd>
</dl>

```Java
@ResponseBody
@GetMapping("/body")
public Student one() {
    return STUDENT;
}
```
<!-- .element: class="fragment"-->
<dl style="width:90%">
    <dt class="fragment"><code class="yellow">@ResponseBody</code></dt>
    <dd class="fragment">returnter Wert ist Response-Body</dd>
</dl>

----
```Java
@Controller
@RequestMapping("path")
@ResponseBody // alle Methoden
public class RestController {
```
<!-- .element: class="stretch fragment fade-in-then-semi-out"-->

```Java
@RestController     // @Controller + @ResponseBody
@RequestMapping("api")
public class StudentRestController {

    private final StudentRepository repository;

    public StudentRestController(StudentRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/students")
    List<Student> all() {
        return repository.findAll();
    }
}
```
<!-- .element: class="stretch fragment"-->

---
#### GET Collection
```Java
@GetMapping("/students")
List<Student> all() {
    return repository.findAll();
}
```
<!-- .element: class="fragment"-->
```JSON
Response Code: 200 OK 
[
    {
        "id": 1,
        "name": "Alfred",
        "number": 1
    },
    {
        "id": 2,
        "name": "Bernd",
        "number": 2
    }
]
```
<!-- .element: class="fragment"-->

----
```Java
@GetMapping("/students")
List<Student> findByName(
        @RequestParam(
                name="name", 
                required=true)  // default, bei false -> null
                                // Alternativ: Optional
                String name) {
    return repository.findByName();
}
```
<!-- .element: class="fragment"-->

```JSON
GET /students?name=Alfred

[
    {
        "id": 1,
        "name": "Alfred",
        "number": 1
    }
]
```
<!-- .element: class="fragment"-->

----
#### GET one
```Java
@GetMapping("/students/{id}")
Student one(@PathVariable Integer id) {
    return repository.findById(id)
            .orElseThrow(() -> new StudentNotFoundException(id));
}
```
<!-- .element: class="fragment"-->
```Java
Response Code: 200 OK 
{
    "id": 1,
    "name": "Alfred",
    "number": 1
}
```
<!-- .element: class="fragment"-->
```Java
Response Code: 500 Internal Server Error
{
  "timestamp": "2020-05-20T19:27:41.773+0000",
  "status": 500,
  "error": "Internal Server Error",
  "message": "Could not find Student 404",
  "path": "/api/students/404"
}
```
<!-- .element: class="fragment"-->

---
#### Exceptions
```Java
@GetMapping("/students/{id}")
Student one(@PathVariable Integer id) {
    return repository.findById(id)
            .orElseThrow(() -> new StudentNotFoundException(id));
}

@ResponseBody
@ExceptionHandler(StudentNotFoundException.class)
ProblemDetail handleStudentNotFound(StudentNotFoundException ex) {
    return ProblemDetail.forStatus(HttpStatus.NOT_FOUND);
}
```
<!-- .element: class="fragment"-->

```JSON
Response code: 404 Not Found
{
"type": "about:blank",
"title": "Not Found",
"status": 404,
"instance": "/api/students/404"
}
```
<!-- .element: class="fragment"-->

besser gesammelt außerhalb des Controllers
<!-- .element: class="fragment"-->

----
#### Advice
```Java
@ResponseBody
@ControllerAdvice
public class StudentRestAdvice {

    @ExceptionHandler(StudentNotFoundException.class)
    ProblemDetail handleStudentNotFound(StudentNotFoundException ex) {
        return ProblemDetail.forStatus(HttpStatus.NOT_FOUND);
    }
}
```
<!-- .element: class="fragment stretch"-->

```Java
@Controller + @ResponseBody = @RestController
```
<!-- .element: class="fragment stretch"-->

```Java
@RestControllerAdvice
public class StudentRestAdvice
```
<!-- .element: class="fragment stretch"-->

---
#### POST
```Java
@PostMapping("/students")
ResponseEntity<Student> newStudent(@RequestBody Student student) {
    Student saved = repository.save(student);
    return new ResponseEntity<>(saved, HttpStatus.CREATED);
}
```
<!-- .element: class="fragment"-->

```JSON
POST /students

{
  "name": "Cäsar",
  "number": 3
}
```
<!-- .element: class="fragment"-->

```JSON
Response code: 201 Created
{
    "id": 3,
    "name": "Cäsar",
    "number": 3
}
``` 
<!-- .element: class="fragment"-->

----
#### Constraint Violation
```Java
@PostMapping("/students")
ResponseEntity<Student> newStudent(@RequestBody Student student) {
    Student saved = repository.save(student);
    return new ResponseEntity<>(saved, HttpStatus.CREATED);
}
```
<!-- .element: class="fragment"-->

```Java
@Entity
public class Student {

    @NotNull
    private String name;
```
<!-- .element: class="fragment"-->

```JSON
POST /students

{
  "number": 400
}
```
<!-- .element: class="fragment"-->

```JSON
Response code: 500 Internal Server Error
An internal Server Error occurred.
``` 
<!-- .element: class="fragment"-->

note: Error im persistence-Layer. Besser vorher abfangen

----
<h4><code class="yellow">@Valid</code></h4>

```Java
@PostMapping("/students")
ResponseEntity<Student> newStudent(@Valid @RequestBody Student student) {
    Student saved = repository.save(student);
    return new ResponseEntity<>(saved, HttpStatus.CREATED);
}
```
<!-- .element: class="stretch fragment"-->

* Validierung <!-- .element: class="fragment"--> beim Unmarshalling
* wirft <!-- .element: class="fragment"--> `MethodArgumentNotValidException`

```Java
@ExceptionHandler({MethodArgumentNotValidException.class, 
        StudentValidationException.class})
ProblemDetail handleValidationErrors(Exception e) {
    return ProblemDetail.forStatusAndDetail(
            HttpStatus.BAD_REQUEST, e.getMessage());
}
```
<!-- .element: class="stretch fragment"-->

----
#### Location
```Java
@PostMapping("/students")
ResponseEntity<Student> newStudent(@Valid @RequestBody Student student) {
    Student saved = repository.save(student);
    URI uri = ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("/{id}")
            .build(saved.getId());
    return ResponseEntity
            .created(uri)
            .body(saved);
}
```
<!-- .element: class="stretch fragment"-->

```JSON
Response code: 201 Created
Location: /students/3

{
    "id": 3,
    "name": "Cäsar",
    "number": 3
}
``` 
<!-- .element: class="stretch fragment"-->

---
#### PUT
```Java
@PutMapping("/students/{id}")
ResponseEntity<Student> replaceStudent(
       @Valid @RequestBody Student newStudent, @PathVariable Integer id) {
    Student toSave = repository.findById(id)
            .map(student -> {
                student.setName(newStudent.getName());
                student.setNumber(newStudent.getNumber());
                return student;
            }).orElseGet(() -> {
                newStudent.setId(id);
                return newStudent;
            });
    Student saved = trySave(toSave);

    boolean newStudentCreated = toSave == newStudent;
    if (newStudentCreated) {
        URI uri = getCreatedUri(saved);
        return ResponseEntity.created(uri).body(saved);
    } else
        return ResponseEntity.ok(saved);
}
```
<!-- .element: class="fragment stretch"-->

----
#### Update
```JSON
PUT /students/2
Content-Type: application/json

{
  "name": "Brigitte", 
  "number": 42
}
```
<!-- .element: class="fragment"-->

```JSON
Response code: 200 OK

{
  "id": 2,
  "name": "Brigitte", // vorher Bernd
  "number": 42
}
```
<!-- .element: class="fragment"-->

----
#### INSERT
```Java
PUT http://localhost:8080/api/students/201
Content-Type: application/json

{
  "name": "Newly created",
  "number": 201
}
```
<!-- .element: class="fragment"-->

```JSON
Response code: 201 Created
Location: /students/3   // != 201

{
    "id": 3,
    "name": "Newly created",
    "number": 201
}
``` 
<!-- .element: class="fragment"-->

---
#### DELETE
```Java
@ResponseStatus(HttpStatus.NO_CONTENT)
@DeleteMapping("/students/{id}")
void deleteStudent(@PathVariable Integer id) {
    try {
        repository.deleteById(id);
    } catch (DataAccessException e) {
        throw new StudentNotFoundException(id, e);
    }
}
```
<!-- .element: class="fragment"-->

```JSON
DELETE /students/2
```
<!-- .element: class="fragment"-->

```JSON
Response code: 204 No Content
<Response body is empty>
```
<!-- .element: class="fragment"-->

---
#### Beziehungen

```Java
@Entity
public class Student {
    @ManyToOne
    private School school;
```
<!-- .element: class="stretch fragment"-->

```JSON
GET /students/4
```
<!-- .element: class="stretch fragment"-->

```JSON
Response code: 200 OK
{
  "id": 4,
  "name": "HTL Student",
  "number": 100,
  "school": {
    "id": 3,
    "name": "HTL",
    "students": [
      {
        "id": 4,
        "name": "HTL Student",
        "number": 100,
        "school": {
        ...
```
<!-- .element: class="stretch fragment"-->

----
```Java
@Entity
public class Student {

    @ManyToOne
    private School school;
```
<!-- .element: class="stretch fragment"-->

```Java
@Entity
public class School {

    @OneToMany(mappedBy = "school")
    @JsonIgnore
    private Collection<Student> students;
```
<!-- .element: class="stretch fragment"-->

```JSON
GET /students/4
```
<!-- .element: class="stretch fragment"-->

```JSON
Response code: 200 OK
{
  "id": 4,
  "name": "HTL Student",
  "number": 100,
  "school": {
    "id": 3,
    "name": "HTL"
  }
}
```
<!-- .element: class="stretch fragment"-->

---
#### RPC
* Client <!-- .element: class="fragment fade-in-then-semi-out"--> muss für jede Ressource URI *wissen*
* keine Contextabhängigkeiten
<!-- .element: class="fragment fade-in-then-semi-out"-->
* statisch
<!-- .element: class="fragment"-->

---
#### HAL
Hypertext Application Language
![hal_model](./assets/REST/HAL.png)

----
```JSON
{
  "id": 4,
  "name": "HTL Student",
  "number": 100,
  "school": {
    "id": 3,
    "name": "HTL"
  },
  "_links": {
    "self": {
      "href": "http://localhost:8080/api/students/4"
    },
    "students": {
      "href": "http://localhost:8080/api/students"
    },
    "school": {
      "href": "http://localhost:8080/api/schools/3"
    }
  }
}
```
<!-- .element: class="stretch"-->

---
#### HATEOAS
```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-hateoas</artifactId>
</dependency>
```

---
#### GET
```Java
@GetMapping("/students/{id}")
Student one(@PathVariable Integer id) {
    return repository.findById(id)
            .orElseThrow(() -> new StudentNotFoundException(id));
}
```
<!-- .element: class="stretch"-->


```Java
@GetMapping("/students/{id}")
EntityModel<Student> one(@PathVariable Integer id) {
    var student = repository.findById(id)
            .orElseThrow(() -> new StudentNotFoundException(id));
    return EntityModel.of(student,
            linkTo(methodOn(StudentHateosRestController.class).one(id))
                    .withSelfRel(),
            linkTo(methodOn(StudentHateosRestController.class).all())
                    .withRel("students"));
}
```
<!-- .element: class="stretch fragment"-->

----
```Java
@GetMapping("/students/{id}")
public EntityModel<Student> one(@PathVariable Integer id) {
    var student = repository.findById(id)
            .orElseThrow(() -> new StudentNotFoundException(id));
    return assembler.toModel(student);
}
```
<!-- .element: class="stretch"-->

```Java
@Component
public class StudentModelAssembler implements 
        RepresentationModelAssembler<Student, EntityModel<Student>> {

    @Override
    public EntityModel<Student> toModel(Student student) {
        return EntityModel.of(student,
                linkTo(methodOn(StudentHateoasRestController.class)
                        .one(student.getId())).withSelfRel(),
                linkTo(methodOn(StudentHateoasRestController.class)
                        .all()).withRel("students"));
    }
}
```
<!-- .element: class="stretch fragment"-->

---
#### POST
```Java
@PostMapping("/students")
ResponseEntity<EntityModel<Student>> newStudent(
        @Valid @RequestBody Student student) {

    Student saved;
    saved = trySave(student);
    var studentModel = assembler.toModel(saved);
    return ResponseEntity
            .created(studentModel
                    .getRequiredLink(IanaLinkRelations.SELF).toUri())
            .body(studentModel);
}
```
<!-- .element: class="stretch"-->
