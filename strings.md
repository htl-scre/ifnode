# Strings

---
#### Facts
```Java
public final class String implements ... {
    @Stable     // most hyper mega final
    private final byte[] value;
```

<ul>
    <li class="fragment fade-in-then-semi-out">Klasse <code>final</code></li>
    <li class="fragment fade-in-then-semi-out">Strings sind <em>immutable</em> (unveränderbar)</li>
    <li class="fragment fade-in-then-semi-out">Jede Stringoperation returnt <strong>entweder</strong>
    <ul>
        <li>den übergebenen String (keine Änderung)</li>
        <li>einen <code>new String</code></li>
    </ul></li>
</ul>
note: @Stable @Stable auch auf alle ArrayEintrag und alle Memberfields. Nur für JDK interna

----
#### Stringpool
```Java
String a = "literal";
String b = "literal";
String c = new String("literal");
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
a == b           <span class="fragment">-> true</span>
</code></pre>

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
a == c           <span class="fragment">-> false</span>
a.equals(c)      <span class="fragment">-> true</span>
</code></pre>

<p class="fragment">⟹ <code>equals</code>👍</p>

note: Literal: Fixwert im Sourcecode

---
#### Concatenation
```Java
String alphabet = "";
for (char c = 'a'; c <= 'z'; c++)
    alphabet += c;
```
<!-- .element: class="fragment fade-in-then-semi-out" -->
<p class="fragment fade-in-then-semi-out">Verarbeitung Compilersache</p> 
<p class="fragment"> ⟹ nicht in Schleifen! ⚠️</p>

```Java
StringBuilder sb = new StringBuilder();
for (char c = 'a'; c <= 'z'; c++)
    sb.append(c);
String alphabet = sb.toString();
```
<!-- .element: class="fragment" -->

👍
<!-- .element: class="fragment" -->

---
#### Methoden
<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
"Seperated by delimiter".split(" ") 
<span class="fragment">-> ["Seperated", "by", "delimiter"]</span>
</code></pre>

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
String.join(" ", "Seperated", "by", "delimiter") 
<span class="fragment">-> "Seperated by delimiter"</span>
</code></pre>

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
String.format(Locale.US,"%08.3f", 314.15) <span class="fragment">-> "0314.150"</span>
"%08.3f".formatted(314.15) <span class="fragment">-> "0314,150"</span>
</code></pre>

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
"String".charAt(2)  <span class="fragment">-> 'r'</span>
"String".charAt(20) <span class="fragment">-> IndexOutOfBoundsException 💥</span>
</code></pre>

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
"String".indexOf('r')   <span class="fragment">-> 2</span>
"String".indexOf('X')   <span class="fragment">-> -1</span>
</code></pre>

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
"String".length()       <span class="fragment">-> 6</span>
"🦥🏴‍☠️".length()        <span class="fragment">-> 7   ⚠️ Emoji ⚠</span>
</code></pre>

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
"String".substring(2, 5)   <span class="fragment">-> "rin"</span>
<span class="fragment">[beginIndex, endIndex[</span>
<span class="fragment">length = endIndix - beginIndex</span>
</code></pre>