# Lambda Expressions

---
#### Motivation
```Java
interface Predicate<T> {
    boolean test(T t);
}
```
<!-- .element: class="fragment" -->

```Java
Predicate<Integer> isEven = new Predicate<>() {
    @Override
    public boolean test(Integer i) {
        return i % 2 == 0;
    }
};
```
<!-- .element: class="fragment" -->

Redundante Information
<!-- .element: class="fragment" -->

<ul>
    <li class="fragment"><code>new Predicate()</code></li>
    <li class="fragment">Signatur der Funktion (welche sonst)</li>
</ul>

Interessant: `i % 2 == 0`
<!-- .element: class="fragment" -->

---
#### Functional Interface
```Java
@FunctionalInterface
public interface Predicate<T> {

    boolean test(T t);
    default Predicate<T> and(Predicate<? super T> other) { ... }
    static <T> Predicate<T> not(Predicate<? super T> target) { ... }
}
```
<!-- .element: class="fragment stretch" -->

<ul>
    <li class="fragment">Single Abstract Method</li>
    <li class="fragment">beliebig viele implementierte</li>
    <li class="fragment"><code class="yellow">@FunctionalInterface</code></li>
</ul>

----
#### Lambda Expression
<p class="fragment">Voraussetzung: <code class="yellow">@FunctionalInterface</code></p>

```Java
Predicate<Integer> isEven = new Predicate<>() {
    @Override
    public boolean test(Integer i) {
        return i % 2 == 0;
    }
};
```
<!-- .element: class="fragment" -->

```Java
Predicate<Integer> isEven = (i) -> {
    return i % 2 == 0;
};
isEven.test(42);
```
<!-- .element: class="fragment" -->

Generelle Syntax
<!-- .element: class="fragment" -->
```Java
Typ name = (params) -> { logic };
```
<!-- .element: class="fragment" -->

----
#### Kürzer!
```Java
Predicate<Integer> isEven = (i) -> {
    return i % 2 == 0;
};
```
<!-- .element: class="fragment" -->
`return`, `{` und `}` weglassen
<!-- .element: class="fragment" -->

```Java
Predicate<Integer> isEven = (i) -> i % 2 == 0;
```
<!-- .element: class="fragment" -->

bei nur einem Parameter: `()` weglassen
<!-- .element: class="fragment" -->

```Java
Predicate<Integer> isEven = i -> i % 2 == 0;
```
<!-- .element: class="fragment" -->

```Java
Comparator<String> byLength = (s1, s2) -> 
        Integer.compare(s1.length(), s2.length());
```
<!-- .element: class="fragment" -->

---
#### Methodenreferenzen
```Java
Predicate<Character> isDigit = c -> Character.isDigit(c);
```
<!-- .element: class="stretch fragment" -->

```Java
Predicate<Character> isDigit = Character::isDigit;
```
<!-- .element: class="stretch fragment" -->

```Java
Predicate<String> isEmpty = s -> s.isEmpty();
Predicate<String> isEmpty = String::isEmpty;
```
<!-- .element: class="stretch fragment" -->

```Java
final String name = "not changing";
Predicate<String> isContainedInName = name::contains;
```
<!-- .element: class="stretch fragment" -->

```Java
BiFunction<String, Integer, Character> charAt = String::charAt;
char c = charAt.apply("not recommended", 42);
```
<!-- .element: class="stretch fragment" -->

<ul>
    <div class="fragment">
        <li>statische Methoden / auf Argument anwenden</li>
        <pre><code class="hljs Java" data-trim data-noescape>
Class::method
        </code></pre>
    </div>
    <div class="fragment">
        <li>auf bestimmtes Objekt anwenden</li>
        <pre><code class="hljs Java" data-trim data-noescape>
reference::method
        </code></pre>
    </div>
</ul>
