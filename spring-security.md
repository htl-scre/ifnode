# Spring Security

---
#### Security-Aspekte

<dl>
  <dt class="fragment fade-in">Authentifizierung</dt>
  <dd>
    <ul>
      <li class="fragment fade-in-then-semi-out">Anmeldung</li>
      <li class="fragment fade-in-then-semi-out">Registrierung</li>
    </ul>
  </dd>
  <dt class="fragment fade-in">Autorisierung</dt>
  <dd class="fragment fade-in"><ul>
    <li>Rechte</li>
  </ul></dd>
</dl>

---
#### Setup

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
```
<!-- .element: class="stretch fragment" -->

![](./assets/spring-security/sign-in.png)
<!-- .element: class="fragment" -->

----
#### Default sign in

```text
Using generated security password: abde9925-920b-4278-8ff5-bc9e533f8abc

This generated password is for development use only.
```
<!-- .element: class="stretch fragment" -->

```yaml
username: user
password: abde9925-920b-4278-8ff5-bc9e533f8abc
```
<!-- .element: class="stretch fragment" -->

---
#### Settings

```Java
@EnableWebSecurity
@Configuration
public class SecurityConfig {

  @Bean
  public AuthenticationProvider authenticationManager(
        PasswordEncoder passwordEncoder,
        UserDetailsService userDetailsService) {
    var authenticationProvider = new DaoAuthenticationProvider();
    authenticationProvider.setPasswordEncoder(passwordEncoder);
    authenticationProvider.setUserDetailsService(userDetailsService);
    return authenticationProvider;
  }
```
<!-- .element: class="stretch fragment" -->

```Java
  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder(10, new SecureRandom());
  }
```
<!-- .element: class="stretch fragment" -->

---
#### `UserDetailsService`
```Java
public interface UserDetailsService {
  UserDetails loadUserByUsername(String username) 
        throws UsernameNotFoundException;
}
```
<!-- .element: class="stretch fragment" -->

```Java
@Service
public record UserDetailsServiceImpl(UserRepository userRepository) 
      implements UserDetailsService {
      
  @Override
  public UserDetails loadUserByUsername(String username) 
      throws UsernameNotFoundException {
    var user = userRepository
          .findByUsername(username)
          .orElseThrow(() -> new UsernameNotFoundException());
    return new UserDetailsImpl(user);
  }
}
```
<!-- .element: class="stretch fragment" -->

----
#### `UserDetails`

```Java
public interface UserDetails extends Serializable {

  Collection<? extends GrantedAuthority> getAuthorities();
  String getPassword();
  String getUsername();
  boolean isAccountNonExpired();
  boolean isAccountNonLocked();
  boolean isCredentialsNonExpired();
  boolean isEnabled();
}
```

----
```Java
public class UserDetailsImpl implements UserDetails {

  private final UserEntity user;

  public UserDetailsImpl(UserEntity user) {
    this.user = user;
  }

  // Spring braucht den ROLE_-Präfix
  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return Set.of(new SimpleGrantedAuthority("ROLE_" + user.getRole()));
  }

  @Override
  public String getPassword() {
    return user.getPassword();
  }
  
  // ... Properties vom user holen
}
```
<!-- .element: class="stretch fragment" -->

----
#### `User`
```Java
@Entity
@...
public class UserEntity {

  @Id
  private String username;

  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  @NotBlank
  private String password;

  @NotNull
  private String role;
  
  // ... weitere Felder
}
```
<!-- .element: class="stretch fragment" -->

User-Klasse nach Belieben implementierbar
<!-- .element: class="fragment" -->

---
#### Autorisierung
```Java
public class SecurityConfig {
  ...
  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) 
        throws Exception {
    return http
        // alle Routen nur für Authentifizierte
        .authorizeHttpRequests(auth -> auth.anyRequest().authenticated())
        // Anmeldung über ein html-form
        .formLogin(withDefaults())
        .build();           
  }
}
```
<!-- .element: class="fragment stretch" -->

default
<!-- .element: class="fragment" -->

----
#### Route authorization

```Java
http.authorizeHttpRequests(auth ->
        auth
                .requestMatchers(GET, "/", "/home").permitAll()
                .requestMatchers("/admin/**").hasRole("admin")
                .anyRequest().authenticated())
```
<!-- .element: class="fragment stretch" -->

first match handles request
<!-- .element: class="fragment" -->

----
#### Customization

```Java
http
        .authorizeHttpRequests( ... )
        .formLogin(form -> form
                .loginPage("/login")
                .permitAll()
                .defaultSuccessUrl("/greeting", true))
        .rememberMe(withDefaults());
```
<!-- .element: class="fragment stretch" -->

rememberMe setzt langlebigen Cookie
<!-- .element: class="fragment" -->

---
#### csrf
![csrf](./assets/spring-security/csrf-attack.webp)
<!-- .element: class="fragment" style="height:80vh" -->

----
```html
<img src="http://bank.com/transfer?accountNo=5678&amount=1000"/>
```
<!-- .element: class="fragment" -->

```html
<form action="http://bank.com/transfer" method="POST">
    <input type="hidden" name="accountNo" value="5678"/>
    <input type="hidden" name="amount" value="1000"/>
    <input type="submit" value="Apply for free"/>
</form>
```
<!-- .element: class="fragment" -->

```javascript
<body onload="document.forms[0].submit()">
```
<!-- .element: class="fragment" -->

POST, PATCH, PUT, DELETE ist forbidden(403)
<!-- .element: class="fragment" -->

----
#### Countermeasures
![csrf-denied](./assets/spring-security/csrf-denied.png)
<!-- .element: class="fragment" style="height:80vh" -->

----
#### Website liefert html

* keine Aktion erforderlich
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Spring added automatisch bei jedem template csrf als hidden field
<!-- .element: class="fragment" -->

----
#### Service
```Java
return http
    .csrf(csrf -> csrf.csrfTokenRepository(withHttpOnlyFalse()))
    .authorizeHttpRequests(auth -> auth.anyRequest().authenticated())
    .httpBasic(withDefaults());
return http.build();
```
<!-- .element: class="fragment stretch" -->

oder Stateless 👍
<!-- .element: class="fragment" -->

---
#### JWT

![](./assets/spring-security/jwt-sequence.png)
<!-- .element: class="r-stretch" -->

----
#### Token

![](./assets/spring-security/jwt-structure.png)

----
* payload per default nur base64-encrypted
<!-- .element: class="fragment fade-in-then-semi-out" -->
* mäßig für Sessions geeignet
<!-- .element: class="fragment fade-in" -->
  * logout implementieren
<!-- .element: class="fragment fade-in-then-semi-out" -->
  * Token-Blacklist?
<!-- .element: class="fragment fade-in" -->

---
#### OAuth2

![](./assets/spring-security/oauth2.png)