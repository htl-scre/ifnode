# Generics

---
#### IntBox
```Java
public class IntBox {
    private int content;

    public void set(int content) { 
        this.content = content; 
    }

    public int get() { 
        return content; 
    }
}
```
<!-- .element: class="fragment" -->

```Java
IntBox intBox = new IntBox();
int i = intBox.get(); 
```
<!-- .element: class="fragment" -->

----
#### DoubleBox
```Java
public class DoubleBox {
    private double content;

    public void set(double content) { 
        this.content = content; 
    }

    public double get() { 
        return content; 
    }
}
```
<!-- .element: class="fragment" -->

```Java
DoubleBox doubleBox = new DoubleBox();
double d = doubleBox.get(); 
```
<!-- .element: class="fragment" -->

----
#### Box
```Java
public class Box {
    private Object content;

    public void set(Object content) { 
        this.content = content; 
    }

    public Object get() { 
        return content; 
    }
}
```
<!-- .element: class="fragment" -->

```Java
Box box = new Box();
Object d = box.get(); 
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
Box box = new Box();
box.set(42);
int i = box.get();  <span class="fragment">🚫</span>
<span class="fragment">int i = (int) box.get();</span>
</code></pre>

* Keine Typchecks zur Compilezeit

---
<h4>Box&ltT></h4>

```Java
public class Box<T> {
   private T content;
   
   public void set(T content) { 
       this.content = content; 
   }
   
   public T get() { 
       return content; 
   }
}
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
Box&ltInteger> intBox = new Box<>();
intBox.set(42);
int i = intBox.get();
intBox.set(3.14);   <span class="fragment">🚫</span>
</code></pre>

```Java
Box<Double> doubleBox = new Box<>();
doubleBox.set(3.14);
```
<!-- .element: class="fragment" -->


---
#### Generell
```Java
class Name<T, E, K, V, S, U>      
        extends X<T> implements Y<T, E>, Z { }
```
<!-- .element: class="fragment stretch" -->

<ul>
    <li class="fragment">Parameter: Übergabe Wert <code>foo(value)</code></li>
    <li class="fragment">Typparameter: Übergabe Typ <code>Foo&ltType></code></li>
    <li class="fragment">Typparameter wird beim Erzeugen gesetzt</li>
</ul>

```Java
new Box<Integer>(3);
new Box<Double>();
```
<!-- .element: class="fragment stretch" -->

Falls der Typ "*klar*" ist, kann auch `<>` stehen
<!-- .element: class="fragment" --> 

```Java
Box<String> box = new Box<>();
Box< Box<String> > boxBox = new Box<>();
```
<!-- .element: class="fragment stretch" -->

----
#### Implementierung
```Java
public class Box<T> {
   private T content;

   public void set(T content) { 
       this.content = content; 
   }
}
```
<!-- .element: class="fragment" --> 

wird beim Kompilieren zu
<!-- .element: class="fragment" -->
 
```Java
public class Box {
   private Object content;

   public void set(Object content) { 
       this.content = content; 
   }
}
```
<!-- .element: class="fragment" --> 

---
#### Generische Methoden
```Java
modifier... <T, U, ...> returntyp name(params...)
```
<!-- .element: class="fragment" --> 

```Java
class Utils {

    public static <T> void foo(T t) { }
}
```
<!-- .element: class="fragment" --> 

Meist werden Typargumente automatisch ergänzt (*Type inference*)
<!-- .element: class="fragment" --> 
```Java
Utils.foo(42);
```
<!-- .element: class="fragment" --> 

---
#### Konsequenzen
Note wird schlechter bei
<ul>
    <li class="fragment fade-in-then-semi-out"><code>catch Exception</code></li>
    <li class="fragment fade-in-then-semi-out"><code>return null</code></li>
    <li class="fragment fade-in-then-semi-out"><code>foo(null)</code></li>
    <li class="fragment">Raw Types in Abgabe
    <ul>
        <li class="fragment fade-in-then-semi-out">IntelliJ unterlegt <span class="monospace" style="background: #52503A">Box</span> färbig</li>
        <li class="fragment fade-in-then-semi-out">IntelliJ <span class="monospace">Problems</span></li>
        <li class="fragment">IntelliJ <span class=monospace>Analyze -> Inspect Code</span></li>
    </ul></li>

</ul>

---
#### Bounded Types
```Java
public class NumberBox<T extends Number> {

    private T content;

    public NumberBox(T content) {
        this.content = content;
    }

    public int intValue() {
        return content.intValue();
    }
}
```
<!-- .element: class="fragment" --> 

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
new NumberBox<>(3);
new NumberBox<>(3.1415);
new NumberBox<>("legit");   <span class="fragment">🚫</span>
</code></pre>

<p class="fragment"><code>extends</code> steht hier auch für Interfaces</p>

----
#### Vererbung
<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
Integer i = 42;
Number number = i;  <span class="fragment">✔️</span>
Object object = i;  <span class="fragment">✔️</span>
</code></pre>

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
Box&ltNumber> box = new Box<>();
box.set(10);
box.set(3.1415);
</code></pre>

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
Box&ltNumber> box = new Box&ltNumber>();
box = new Box&ltInteger>();     <span class="fragment">🚫</span>
</code></pre>

![generics-inheritance](./assets/generics/generics-subtype-relationship.gif)
<!-- .element: class="fragment" --> 

----
![generics-inheritance](./assets/generics/generics-subtype-relationship.gif)

```Java
Box<Integer> integerBox = new Box<>();
Box<Number> numberBox = new Box<>();
numberBox = integerBox; 🚫
```
<!-- .element: class="fragment" -->

<div class="fragment">
ansonsten
<pre><code class="hljs Java" data-trim data-noescape>
numberBox.set(3.1415);
int i = integerBox.get();
</code></pre>
</div>

---
#### `? extends` - Wildcards

```Java
Box<Number> box = new Box<>();
box = new Box<Integer>();   🚫
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
Box<? extends Number> box;
box = new Box<Number>();
box = new Box<Integer>();
box = new Box<Double>();
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
box.set(3.1415);     🚫 <span class="fragment">box könnte Box&ltInteger> sein</span>
</code></pre>

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
double d = box.get();     🚫 <span class="fragment">box könnte Box&ltLongLong> sein</span>
</code></pre>

```Java
Number number = box.get();  ✔️
```
<!-- .element: class="fragment" -->

```Java
Box<?> box = new Box<AnyClass>();️
```
<!-- .element: class="fragment" -->

notes: 
* raw types gehen immer
* `box.set(null)` ✔️

----
#### `? super`
```Java
Box<? super Integer> box;
box = new Box<Integer>();
box = new Box<Number>();
box = new Box<Object>();
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
box.set(number);     🚫 <span class="fragment">box könnte Box&ltInteger> sein</span>
</code></pre>

```Java
box.set(42);    ✔️
```
<!-- .element: class="fragment" -->

<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
int i = box.get();     🚫 <span class="fragment">box könnte Box&ltNumber> sein</span>
</code></pre>

notes: 
* raw types gehen immer
* `Object object = box.get()` ✔️

----
#### Verwendung

<code>extends</code> für Parameter 
<!-- .element: class="fragment" -->

<div class="fragment">
<pre><code class="hljs Java" data-trim data-noescape>
static int intValue(Box&ltNumber> box) {
    return box.get().intValue();
}
</code></pre>

<pre><code class="hljs Java" data-trim data-noescape>
intValue(new Box&ltNumber>());      ✔️
intValue(new Box&ltInteger>());     🚫
</code></pre>
könnte man verbessern zu
</div>

<div class="fragment">
<pre><code class="hljs Java" data-trim data-noescape>
static int intValue(Box&lt? extends Number> box) {
    return box.get().intValue();
}
</code></pre>

<pre><code class="hljs Java" data-trim data-noescape>
intValue(new Box&ltNumber>());      ✔️
intValue(new Box&ltInteger>());     ✔️
</code></pre>
</div>

---
#### Einschränkungen
<ul>
    <li class="fragment">
    Keine primitiven Typen als Typargument
    <pre><code class="hljs Java" data-trim data-noescape>
Box&ltint> box;   🚫</code></pre>
    </li>
    <li class="fragment">
    Keine Konstruktoraufrufe für Typparameter
        <pre><code class="hljs Java" data-trim data-noescape>
new T();   🚫</code></pre>
    </li>
    <li class="fragment">
    Keine statischen Typparameter - Referenzen
        <pre><code class="hljs Java" data-trim data-noescape>
class Foo&ltT> {
    static T t; 🚫
}</code></pre>
    </li>
</ul>

----
<ul>
    <li class="fragment">
    Keine generischen Arrays erzeugen
    <pre><code class="hljs Java" data-trim data-noescape>
static &ltT> void foo() {
    T[] array;        ✔️
    array = new T[1]; 🚫
}</code></pre>
    </li>
    <li class="fragment">
    Keine Arrays parametrisierter Typen
    <pre><code class="hljs Java" data-trim data-noescape>
static void foo() {
    Box&ltInteger>[] array;        ✔️
    array = new Box&ltInteger>[1]; 🚫
}</code></pre>
    </li>
    <li class="fragment">
        Typparameter - varargs sind ok, solang die Methode nicht für ein <code>Object[]</code> aufgerufen wird
        <pre><code class="hljs Java" data-trim data-noescape>
@SafeVarargs
static &ltT> void foo(T... ts) {
}</code></pre>
        </li>
</ul>