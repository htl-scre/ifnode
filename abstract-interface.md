# Abstrakte Klassen

---
#### Motivation
![uml chess](assets/OOP/uml_chess.png)
```Java
public class ChessPiece {
    public void move() {
        // ?
    }
}
```
<!-- .element: class="fragment" -->

<ul>
    <li class="fragment"><code>chessPiece.play()</code>?</li>
    <li class="fragment"><code>new ChessPiece()</code>?</li>
</ul>

---
#### `abstract`
```Java
public abstract class ChessPiece {
    private Position position;

    protected ChessPiece(Position position) {
        this.position = position;
    }

    public abstract void move();
}
```

<ul>
    <li class="fragment fade-in-then-semi-out"><code>abstract</code> Methode hat keinen Body</li>
    <li class="fragment fade-in-then-semi-out"><code>abstract</code> Methode ⟹ Klasse <code>abstract</code></li>
    <li class="fragment fade-in-then-semi-out"><code>new AbstractClass()</code> 🚫</li>
    <li class="fragment">Konstruktor für <code>super()</code> ✔️</li>
</ul>

----
<ul>
<li>Childklassen müssen <strong>alle</strong> <code>abstract</code> Methoden überschreiben</li>
<pre><code class="hljs Java" data-trim data-noescape>
public class King extends ChessPiece {
    @Override
    public void move() {
        moveKnightly();
    }
}
</code></pre>
<li>oder selbst <code>abstract</code> sein</li>
<pre><code class="hljs Java" data-trim data-noescape>
public abstract class RoyalChessPiece extends ChessPiece {}
</code></pre>

</ul>

----
#### Sicher nicht
<dl>
    <div class="fragment fade-in-then-semi-out">
        <dt><code>private abstract</code></dt>
        <dd>man kann nicht overriden</dd>
    </div>
    <div class="fragment fade-in-then-semi-out">
        <dt><code>final abstract</code></dt>
        <dd>man darf nicht overriden</dd>
    </div>
    <div class="fragment fade-in-then-semi-out">
        <dt><code>static abstract</code></dt>
        <dd>statische Member werden nicht vererbt</dd>
    </div>
    <div class="fragment fade-in-then-semi-out">    
        <dt><code>native abstract</code></dt>
        <dd><code>native</code> - Code liegt kompiliert vor</dd>
    </div>
</dl>



---
#### `interface`
```Java
public interface Flyable {
    double GRAVITY = 9.80665;
    void fly();
}
```
* definiert Verhalten
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Fields <!-- .element: class="fragment fade-in-then-semi-out" -->implizit `public static final`
* Methoden <!-- .element: class="fragment fade-in-then-semi-out" -->implizit `public abstract`
* keine Konstruktoren
<!-- .element: class="fragment fade-in-then-semi-out" -->
* als Compiletyp zulässig
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Klassen <!-- .element: class="fragment fade-in-then-semi-out" -->*implementieren* das Interface
* kann <!-- .element: class="fragment fade-in-then-semi-out" -->**mehrere** andere Interfaces extenden
* Implementierung <!-- .element: class="fragment" -->checken mit `instanceof` 

----
```Java
class Eagle extends Bird implements Flyable, Serializable {
    @Override
    void fly() {
        flyHigh(GRAVITY);
    }
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
abstract class FlyingBird extends Bird implements Flyable {}
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
Flyable[] airForce = new Flyable[50];
airForce[0] = new Jet();
airForce[1] = new Airship();
airForce[2] = new Balloon();
...

for (Flyable flyer : airForce)
    flyer.fly();
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
interface Crashable extends Flyable, Speedable {
    void crash();
}
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

----
#### `default`/`static`
<pre class="stretch"><code class="hljs Java" data-trim data-noescape>
public interface Speedable {
    double KM_PER_MPH = 1.60934;
    void speed();

    <span class="fragment">default void callForHelp() {
        PhoneService.call("122");
    }</span>

    <span class="fragment">static double convertMphToKmh(double mph) {
        assertValidMph(mph);
        return mph * KM_PER_MPH;
    }</span>

    <span class="fragment">private static void assertValidMph(double mph) {
        if (!Double.isFinite(mph)) 
            throw new IllegalArgumentException("Invalid speed: " + mph);
    }</span>
}
</code></pre>

Klassen<!-- .element: class="fragment" --> *können* `default`-Methoden overriden
