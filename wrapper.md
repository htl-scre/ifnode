# Wrapper

---
#### Motivation
<pre><code class="hljs Java" data-trim data-noescape>
public class ThoughtSender() {
    public native void send(Object o);

    public static void main(String... args) {
        ThoughtSender sender = new ThoughtSender();
        <span class="fragment">sender.send("Sleep");</span>
        <span class="fragment">sender.send(new OutOfMemoryError());</span>
        <span class="fragment">sender.send(42);</span>
    }
</code></pre>

---
#### `Integer`
<pre><code class="hljs Java" data-trim data-noescape>
public final class Integer extends Number implements ... {
    private final int value;

    <span class="fragment">public int intValue() {
        return value;
    }</span>

    <span class="fragment">public static Integer valueOf(int i) {
        ...
        return new Integer(i);
    }</span>
</code></pre>

<div class="fragment">
Autoboxing
    <pre><code class="hljs Java" data-trim data-noescape>
Integer wrapper = 3;
int primitive = wrapper;
    </code></pre>
</div>

note: `final`, Konstruktoren deprecated

----
![uml wrapper](assets/OOP/uml_wrapper.png)

<div class="fragment">
Kein Upcasting!
    <pre><code class="hljs Java" data-trim data-noescape>
byte -> short -> int -> long -> float -> double
          char  -^
    </code></pre>
</div>


<pre class="fragment"><code class="hljs Java" data-trim data-noescape>
double primitive = 3.14f; <span class="fragment">✔️</span>
Double wrapper = Float.valueOf(3.14f); <span class="fragment">🚫</span>
Long wrapper = 3; <span class="fragment">🚫</span>
</code></pre>

----
#### caching/`equals`
<pre><code class="hljs Java" data-trim data-noescape>
Integer.valueOf(3) == Integer.valueOf(3)          <span class="fragment">-> true</span>
Integer.valueOf(128) == Integer.valueOf(128)      <span class="fragment">-> false</span>
Integer.valueOf(128).equals(Integer.valueOf(128)) <span class="fragment">-> true</span>
</code></pre>

für alle Wrapper in <span class="monospace">[-128, 127]</span>
<!-- .element: class="fragment fade-in-then-semi-ou" -->
<p class="fragment">⟹ <code>equals</code>👍</p>

note: Obergrenze customizable