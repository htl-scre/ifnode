# Thymeleaf

---

### Motivation

* A und B loggen sich in WebUntis ein, beide sehen ihren Stundenplan

<!-- .element: class="fragment fade-in-then-semi-out" -->

* ⟹ dynamisches html

<!-- .element: class="fragment fade-in-then-semi-out" -->

* Thymeleaf ist eine template engine

<!-- .element: class="fragment" -->

----
![](./assets/thymeleaf/trad-vs-spa.ppm)

----

#### Multi/Single Page Application

<dl>
  <dt class="fragment">Multi</dt>
  <dd class="fragment fade-in-then-semi-out">Flow durch Controller & mehrere html</dd>
  <dd class="fragment fade-in-then-semi-out">Back-button ✔️</dd>

  <dt class="fragment">Single</dt>
  <dd class="fragment fade-in-then-semi-out"><strong>Eine</strong> html Seite, Flow durch js (+ REST)</dd>
  <dd class="fragment fade-in-then-semi-out">Back-button 🤔</dd>
</dl>

---

### Statisches html

* sollte in `resources\static\` liegen
  ![location](./assets/thymeleaf/static-html.png)
* wird automatisch ausgeliefert

<!-- .element: class="fragment fade-in-then-semi-out" -->

![location](./assets/thymeleaf/hello-world.png)
<!-- .element: class="fragment" -->

---

### Spring MVC

![mvc diagram](./assets/thymeleaf/mvc-diagram.png)
<!-- .element: class="fragment" -->

----

### Controller

```Java
@Controller
@RequestMapping("/greetings")
public record GreetingController {

    @GetMapping("/greeting")
    public String greeting(
                @RequestParam(name = "name", 
                    required = false, 
                    defaultValue = "World") String name, 
                Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }
}
```

<!-- .element: class="fragment" -->

----

### Template

```html
<!DOCTYPE HTML> <!-- greeting.html -->
<html xmlns:th="http://www.thymeleaf.org" lang="en">
<head>
    <title>Greetings</title>
    <meta http-equiv="Content-Type"
          content="text/html; charset=UTF-8"/>
</head>
<body>
<p th:text="'Hello, ' + ${name} + '!'"/>
</body>
</html>
```

<!-- .element: class="fragment fade-in-then-semi-out" -->

`${xxx}` greift auf Modelattribut `xxx` zu
<!-- .element: class="fragment fade-in-then-semi-out" -->

![rendered](./assets/thymeleaf/greetings.png)
<!-- .element: class="fragment" -->

---

### Views

```Java
@GetMapping(value = "/")
public String all(Model model) {
    model.addAttribute("students", studentRepository.findAll());
    return "student-list";
}
```

<!-- .element: class="fragment" -->

```html
...
<head>,
<body>,
<table>
    <tr>
        <th>Name</th>
        <th>Id</th>
        <th>School</th>
    </tr>
    <tr th:each="student: ${students}">
        <td th:text="${student.id}">Id</td>
        <td>
            <a th:text="${student.name}"
               th:href="@{/students/{id} (id=${student.id})}">Name</a>
        </td>
        <td th:if="${student.school}"
            th:text="${student.school.name}">School
        </td>
    </tr>
```

<!-- .element: class="fragment" -->

---

### Forms

```Java
@GetMapping(value = "/new")
public String enrollStudentForm(Model model) {
    model.addAttribute("student", new Student());
    model.addAttribute("interests", Interest.getCommons());
    model.addAttribute("schools", schoolRepository.findAll());
    return "student-enroll";
}
```

<!-- .element: class="fragment" -->

#### Form

<!-- .element: class="fragment fade-in-then-semi-out" -->

```html
...
<head>,
<body>
<form method="post"
      th:action="@{/students/new}"
// @ = baseurl
th:object="${student}"> // ${student.name} = *{name}
...
</form>
```

<!-- .element: class="fragment" -->

----

#### Text

<!-- .element: class="fragment fade-in-then-semi-out" -->

<div class="fragment fade-in-then-semi-out">
    <label for="name">Name: </label>
    <input type="text" id="name" autocomplete="off"/>
</div>

```html
...
<form th:object="${student}">
    <div>
        <label th:for="name">Name: </label>
        <input type="text" th:field="*{name}"/>
    </div>
```

<!-- .element: class="fragment" -->

----

#### Checkboxen

<!-- .element: class="fragment fade-in-then-semi-out" -->

<div class="fragment fade-in-then-semi-out">
    <input type="checkbox" value="Sports">Sports<br>
    <input type="checkbox" value="Sports">Movies<br>
    <input type="checkbox" value="Sports">Programming<br>
</div>

```html
...
<form th:object="${student}">
    <div>
        <label>Interests: </label>
        <ul>
            <li th:each="interest: ${interests}">
                <input type="checkbox"
                       th:field="*{interests}"
                       th:value="${interest.id}"
                       th:text="${interest.description}"/>
            </li>
        </ul>
    </div>
```

<!-- .element: class="fragment" -->

----

#### Select

<!-- .element: class="fragment fade-in-then-semi-out" -->

<div class="fragment fade-in-then-semi-out">
    <label for="school">School: </label>
    <select id="school">
        <option>HTL</option>
        <option>HAK</option>
        <option>AHS</option>
    </select>
</div>

```html
...
<form th:object="${student}">
    <div>
        <label th:for="school">School: </label>
        <select th:field="*{school}">
            <option th:each="school: ${schools}"
                    th:value="${school.id}"
                    th:text="${school.name}"></option>
        </select>
    </div>
```

<!-- .element: class="fragment" -->

---

#### Action-Endpoint

```Java
@PostMapping(value = "/new")
public String addStudent(@Valid Student student, 
        BindingResult bindingResult, Model model) {
    if (bindingResult.hasErrors()) {
        model.addAttribute("interests", Interest.getCommons());
        model.addAttribute("schools", schoolRepository.findAll());
        return "student-enroll";
    }
    var saved = studentRepository.save(student);
    return "redirect:/students/%d".formatted(saved.getId());
}
```

---

#### Fehler

```html

<div th:if="${#fields.hasErrors('*')}">
    <h1>Errors</h1>
    <ul>
        <li th:each="err : ${#fields.errors('*')}"
            th:text="${err}">Error
        </li>
    </ul>
</div>

<div>
    <label for="name">Name: </label>
    <input type="text" id="name" th:field="*{name}"/>
    <span th:if="${#fields.hasErrors('name')}"
          th:errors="*{name}"/>
</div>
```
<!-- .element: class="fragment stretch" -->

---

#### Fragments

```html

<body>
<div th:insert="footer :: copy"></div>
<div th:replace="fragments/header :: header"></div>
</body>
```

<!-- .element: class="fragment fade-in" -->

<pre><code class="hljs html fragment fade-in" data-trim>
<!-- fragments/header.html -->
    <div th:fragment="header">
        Syntax:      templatename :: selector
    </div>
...
</code></pre>

selector wie css
<!-- .element: class="fragment fade-in" -->

---
#### htmx - Motivation

>Why should only &lt;a> &amp; &lt;form> make HTTP requests?  
>Why should only click & submit events trigger them?  
>Why should only GET & POST methods be available?  
>Why can you only replace the entire screen?  
<!-- .element: class="fragment stretch" -->

----
#### htmx

```html
<a href="/blog">Blog</a>
```
<!-- .element: class="fragment fade-in" -->

When a user clicks on this link, issue an HTTP GET request to ‘/blog’ and load the response content into the browser
window.
<!-- .element: class="fragment fade-in-then-semi-out" -->

```html

<button hx-post="/clicked"
        hx-trigger="click"
        hx-target="#parent-div"
        hx-swap="outerHTML"
>
```
<!-- .element: class="fragment fade-in" -->

When a user clicks on this button, issue an HTTP POST request to ‘/clicked’ and use the content from the response to
replace the element with the id parent-div
<!-- .element: class="fragment fade-in" -->

----
```html
<head>
    <script src="https://unpkg.com/htmx.org@2.0.1"></script>
</head>
<body>
<main class="container">
  <section>
    <h1>Htmx Demo</h1>
    <div id="parent-div"></div>
    <button hx-post="/clicked"
            hx-trigger="click"
            hx-target="#parent-div"
            hx-swap="outerHTML">
      Click Me!
    </button>
  </section>
</main>
</body>
```
<!-- .element: class="fragment fade-in stretch" -->

```java
@PostMapping("/clicked")
public String clicked(Model model) {
    model.addAttribute("now", LocalDateTime.now().toString());
    return "clicked :: result";
}
```
<!-- .element: class="fragment fade-in stretch" -->

----
```java
@PostMapping("/clicked")
public String clicked(Model model) {
    model.addAttribute("now", LocalDateTime.now().toString());
    return "clicked :: result";
}
```
<!-- .element: class="stretch" -->

```html
<!-- clicked.html -->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org" lang="en">
<head>
  <title>fragments</title>
</head>
<body>
  <div th:fragment="result" id="parent-div">
    <p th:text="${now}"></p>
  </div>
</body>
</html>
```
<!-- .element: class="fragment fade-in stretch" -->
