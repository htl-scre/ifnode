# Tables

---
```html
<table>
    <caption>Stundenplan</caption>  <!-- Tabellenüberschrift -->
    <thead>     <!-- table head -->
        <tr>    <!-- table row -->
            <th>Montag</th>     <!-- table heading -->
            <th>Dienstag</th>
        </tr>
    </thead>
    <tbody>     <!-- table body -->
        <tr>    
            <td>POS</td>    <!-- table data -->
            <td>TINF</td>
        </tr>
    </tbody>
</table>
```
<!-- .element: class="stretch" -->

<iframe class="html-render fragment" src="./assets/html-tables/basic.html">
</iframe>

<a class="fragment" href="./assets/html-tables/tables-basic.css" target="_blank">css</a>

---
#### Mehrere Zeilen/Spalten
```html
<tX rowspan="n" colspan="m">content</tX>
```

<iframe class="html-render fragment" src="./assets/html-tables/bigger.html" 
onload="this.style.height=(this.contentWindow.document.body.scrollHeight+20)+'px';">
</iframe>

---
#### Alternierender Hintergrund
```css
table tr:nth-child(odd) {
    background: #b8d1f3;
}

table tr:nth-child(even) {
    background: #dae5f4;
}
```

<iframe class="html-render fragment" src="./assets/html-tables/alternating.html" 
onload="this.style.height=(this.contentWindow.document.body.scrollHeight+10)+'%';">
</iframe>