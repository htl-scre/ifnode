## Enumerations

---

#### Definition
* Klasse, von der nur bestimmte Objekte erzeugt werden können
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
public class Direction {

    public static final Direction NORTH = new Direction();
    public static final Direction EAST = new Direction();
    public static final Direction SOUTH = new Direction();
    public static final Direction WEST = new Direction();

    private Direction() { 
    
    }
}
```
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
public enum Direction {
    NORTH, EAST, SOUTH, WEST;
}
```
<!-- .element: class="fragment" -->

----
#### Parametrisiert
```Java
public enum NumeralSystem {
    BINARY(2), OCTAL(8), DECIMAL(10), HEXADECIMAL(16);
    
    private final int base;

    NumeralSystem(int base) {
        this.base = base;
    }

    public int toDecimal(String number) {
        return Integer.parseInt(number, base);
    }
}
```
<!-- .element: class="fragment" -->

---
#### `Enum`
`enum` extended automatisch `Enum`
<!-- .element: class="fragment" -->

```Java
public enum Direction {
    NORTH, EAST, SOUTH, WEST;
}
```
<!-- .element: class="fragment stretch" -->

<pre class="fragment stretch"><code class="hljs Java" data-trim data-noescape>
Direction.valueOf("NORTH")  <span class="fragment">=> Direction.NORTH</span>
</code></pre>

```Java
for (Direction direction : Direction.values())
    System.out.printf("%d %s %n", direction.ordinal(), direction.name());
```
<!-- .element: class="fragment stretch" -->

```Plain Text
0 NORTH 
1 EAST 
2 SOUTH 
3 WEST 
```
<!-- .element: class="fragment stretch" -->

---
#### Anwendung
* konstante, komplexe Daten
    <!-- .element: class="fragment stretch" -->
* benannte Konstanten statt Strings
    <!-- .element: class="fragment stretch" -->