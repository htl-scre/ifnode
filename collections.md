<style>
:not(pre) > code {
    background: inherit;
    display: inline;
    padding: 2px 3px;
    color: #2a76dd;
}
.stretch {
    height: auto !important;
}
.hljs {
    border-radius:0.3em;
}
.monospace {
    font-family: monospace;
}
</style>

# Collections

---
#### Motivation

* Massendatenverarbeitung
<!-- .element: class="fragment" -->
* Arrays nicht erweiterbar
<!-- .element: class="fragment" -->

---
#### Vererbungshierarchie
![Collections-Vererbungshierarchie](assets/collections/java-collection-framework-hierarchy.jpg)
<!-- .element: class="fragment" -->

---
#### `Collection<T>`
```Java
public interface Collection<E> extends Iterable<E> {

    int size();
    boolean isEmpty();
    boolean contains(Object o);
    Iterator<E> iterator();
    Object[] toArray();
    <T> T[] toArray(T[] a);
    boolean add(E e);
    boolean remove(Object o);
    boolean containsAll(Collection<?> c);
    boolean addAll(Collection<? extends E> c);
    boolean removeAll(Collection<?> c);
    boolean retainAll(Collection<?> c);   // getSharedElements
    void clear();
    boolean equals(Object o);
    int hashCode();
}
```
<!-- .element: class="stretch fragment" -->

`boolean` returnwert ⟹ hat sich Collection geändert
<!-- .element: class="fragment" -->

notes: 
* `Stream` - Methoden fehlen
* `retainAll` enthält auch Duplikate

----
#### Konstruktoren
jede Collection hat zumindest 2 Konstruktoren:

<dl>
    <dt class="fragment"><code>public CollectionImpl()</code></dt>
    <dd class="fragment">erzeugt eine leere Collection</dd>
    <dt class="fragment"><code>public CollectionImpl(Collection c)</code></dt>
    <dd class="fragment">erzeugt eine neue Collection, welche alle Elemente der übergebenen enthält</dd>
</dl>

```Java
List<Integer> original = new ArrayList<>();
original.add(10);
original.add(20);
original.add(30);
List<Integer> copy = new ArrayList<>(original);
original.set(1, 42);
```
<!-- .element: class="fragment" -->

```plain
original: [10, 42, 30]
copy: [10, 20, 30]
```
<!-- .element: class="fragment" -->

---
#### `List<E>`
* Indexbasierter Zugriff
<!-- .element: class="fragment fade-in-then-semi-out" -->
* nicht sortiert, aber sortierbar
<!-- .element: class="fragment fade-in-then-semi-out" -->
* geordnet: Zuletzt eingefügtes Element am Ende
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
public interface List<E> extends Collection<E> {

    E get(int index);
    E set(int index, E element);
    void add(int index, E element);
    E remove(int index);
    int indexOf(Object o);
    int lastIndexOf(Object o);
    ListIterator<E> listIterator();
    default void sort(Comparator<? super E> c) { ... }
    List<E> subList(int fromIndex, int toIndex);
}
```
<!-- .element: class="fragment" -->

notes: `subList` returnt einen View

----
#### Implementierungen
<dl>
    <dt class="fragment"><code>ArrayList</code></dt>
    <dd class="fragment">Liste wird durch Array realisiert. <br>
    Bei Bedarf wird das Array vergrößert</dd>
    <dt class="fragment"><code>LinkedList</code></dt>
    <dd class="fragment">Wie in 2.Klasse implementiert
    <img src="assets/collections/double-linked-list.png"></dd>
</dl>

notes: `Vector` nicht benutzen; synchronisiert, aber veraltet

----
```Java
List<String> words = new ArrayList<>();
words.add("Apfel");
words.add("Biene");
words.add("Honig");
```
<!-- .element: class="fragment" -->

```plain
[Apfel, Biene, Honig]
```
<!-- .element: class="fragment" -->

```Java
words.add(1, "Imker");
```
<!-- .element: class="fragment" -->

```plain
[Apfel, Imker, Biene, Honig]
```
<!-- .element: class="fragment" -->

```Java
for (String word : words)
     word.toUpperCase();
```
<!-- .element: class="fragment" -->

```Java
Collections.sort(words);
words.sort(null);
```
<!-- .element: class="fragment" -->

```plain
[Apfel, Biene, Honig, Imker]
```
<!-- .element: class="fragment" -->

---
#### `Set<T>`
<ul>
    <li class="fragment">enthält nur <em>unique</em> Elemente</li>
    <li class="fragment">keine <em>neuen</em> Methoden</li>
</ul>

----
#### Implementierungen
<dl>
    <dt class="fragment"><code>HashSet</code></dt>
    <dd class="fragment">verwendet einen <em>Hashtable</em>; schnell</dd>
    <dt class="fragment"><code>TreeSet</code></dt>
    <dd class="fragment">speichert Elemente sortiert; langsam</dd>
    <dt class="fragment"><code>LinkedHashSet</code></dt>
    <dd class="fragment">verwendet zusätzlich eine <code>LinkedList</code> <br>
    ⟹ geordnet; mittelschnell</dd>
</dl>

----
```Java
Collection<Integer> set = new HashSet<>();
set.add(404);
set.add(201);
set.add(-37);
set.add(-37);
```
<!-- .element: class="fragment" -->

```plain
[404, -37, 201]
```
<!-- .element: class="fragment" -->


```Java
Collection<String> set = new TreeSet<>();
set.add("POS");
set.add("AM");
set.add("NVS");
```
<!-- .element: class="fragment" -->

```plain
[AM, NVS, POS]
```
<!-- .element: class="fragment" -->

```Java
Collection<String> set = new LinkedHashSet<>();
set.add("POS");
set.add("AM");
set.add("NVS");
```
<!-- .element: class="fragment" -->

```plain
[POS, AM, NVS]
```
<!-- .element: class="fragment" -->

---
#### Problem
```Java
public class Dog {
    private String name;

    public Dog(String name) {
        this.name = name;
    }
}
```
<!-- .element: class="fragment" -->

```Java
Collection<Dog> dogs = new HashSet<>();
dogs.add(new Dog("Ecco"));
dogs.add(new Dog("Ecco"));
int size = dogs.size();
```
<!-- .element: class="fragment" -->

```plain
2
```
<!-- .element: class="fragment" -->

notes: Mit `equals` immer noch 2

----
#### Hashing
```Java
Set<User> users = getAllUsers();
User user = new User(input);
if (users.contains(user))
    ...
```
<dl>
    <dt class="fragment"><code>==</code></dt>
    <dd class="fragment"><code>user</code> muss exakt <strong>dasselbe</strong> Objekt wie im Set sein </dd>
    <dt class="fragment"><code>equals</code></dt>
    <dd class="fragment"><code>User</code> hat 7 Instanzvariablen ⟹ langsam </dd>
    <dt class="fragment">Hashing</dt>
    <dd class="fragment"><code>user.hashCode()</code> wird berechnet</dd>
    <dd class="fragment">hat ein Element in <code>users</code> den gleichen Hashcode</dd>
    <dd class="fragment">könnte es das richtige sein<span class="fragment"> ⟹ jetzt <code>equals</code></span></dd>
</dl>

----
#### Kollisionen
![hashing](assets/collections/hashtable.svg)
<!-- .element: class="fragment" -->

* John Smith und Sandra Dee haben gleichen hash
<!-- .element: class="fragment" -->
* Wird nun Sandra Dee gesucht, so wird nur in Bucket 152 gesucht
<!-- .element: class="fragment" -->

---
#### Beispiel
```Java
class Student {

    private int katalognummer;
    private String name;

    public int hashCode() {
        return katalognummer;
    }
}
```

<ul>
    <li class="fragment fade-in-then-semi-out">gleiche KatNr ⟹ <span class="fragment">vielleicht gleiche Schüler</span></li>
    <li class="fragment fade-in-then-semi-out">andere KatNr ⟹ <span class="fragment">unterschiedliche Schüler</span></li>
    <li class="fragment fade-in-then-semi-out">gleiche Schüler ⟹ <span class="fragment">gleiche KatNr</span></li>
    <li class="fragment">unterschiedliche Schüler ⟹ </li>
</ul>

----
#### `hashCode` / `equals` - Contract
<dl>
    <dt class="fragment"><code>a.hashCode() == b.hashCode()</code></dt>
    <dd class="fragment">Objekte vielleicht <code>equal</code></dd>
    <dt class="fragment"><code>a.hashCode() != b.hashCode()</code></dt>
    <dd class="fragment">Objekte unterschiedlich</dd>
    <dt class="fragment"><code>a.equals(b)</code></dt>
    <dd class="fragment"><code>a.hashCode() == b.hashCode()</code></dd>
    <dt class="fragment"><code>!a.equals(b)</code></dt>
    <dd class="fragment"><code>a != b</code></dd>
</dl>

----
#### Mutablity
```Java
Collection<Dog> dogs = new HashSet<>();
Dog ecco = new Dog("Ecco")
dogs.add(ecco);
dogs.add(new Dog("Snoopy"));
ecco.setName("Snoopy");
```
<!-- .element: class="fragment" -->
⚠️ Set korrupt ⚠️
<!-- .element: class="fragment" -->  
Daten in einem Set sollten *immutable* sein
<!-- .element: class="fragment" -->

<ul>
    <li class="fragment fade-in-then-semi-out">wie <code>String</code></li>
    <li class="fragment fade-in-then-semi-out">Instanzvariable <code>private final</code></li>
    <li class="fragment"><code>setter</code> returnen neue Objekte</li>
</ul>

---
#### `TreeSet<T>`
```Java
public class Student {

    private String name;
    private int id;

    public Student(String name, int id) {
        this.name = name;
        this.id = id;
    }
}
```
<!-- .element: class="fragment" -->

```Java
Set<Student> students = new TreeSet<>();
Student student = new Student("Alfred", 1);
students.add(student);
```
<!-- .element: class="fragment" -->

```Plain Text
java.lang.ClassCastException: 
    class Student cannot be cast to class java.lang.Comparable
```
<!-- .element: class="fragment" -->

----
#### `Comparable<T>`
```Java
public interface Comparable<T> {
    /**
    *   this < o  ==> < 0
    *   this == o ==>  0
    *   this > o  ==> > 0
    */
    public int compareTo(T o);
}
```
<!-- .element: class="fragment" -->

<ul>
    <li class="fragment fade-in-then-semi-out">definiert eine <em>natural order</em></li>
    <li class="fragment fade-in-then-semi-out">beim Einfügen in ein <code>TreeSet</code> gecalled</li>
    <li class="fragment">beim Sortieren einer <code>List</code> / eines Arrays  gecalled</li>
</ul>

----
#### Implementierung
```Java
public class Student implements Comparable<Student>{

    @Override
    public int compareTo(Student o) {
        int result = Integer.compare(id, o.id);
        if (result != 0) 
            return result;
        return name.compareTo(o.name);
    }
```
<!-- .element: class="fragment" -->

```Java
Set<Student> students = new TreeSet<>();
students.add(new Student("Alfred", 1));
students.add(new Student("Bernd", 2));
students.add(new Student("Agatha", 1));
```
<!-- .element: class="fragment" -->

```plain
[{Agatha, id=1}, {Alfred, id=1}, {Bernd, id=2}]
```
<!-- .element: class="fragment" -->

```Java
public int compareTo(Student o) {
    return Comparator.comparingInt(Student::getId)
            .thenComparing(Student::getName)
            .compare(this, o);
}
```
<!-- .element: class="fragment" -->

----
#### `NavigableSet<T>`
```Java
public interface NavigableSet<E> extends SortedSet<E> {

    E lower(E e);   // returnt < e
    E floor(E e);   // returnt <=e
    E ceiling(E e); // returnt >=e
    E higher(E e);  // returnt > e
    E pollFirst();  // return min
    E pollLast();   // return max
    SortedSet<E> subSet(E fromElement, E toElement);    // from < all < to
    SortedSet<E> headSet(E toElement);      // min < all < to
    SortedSet<E> tailSet(E fromElement);    // from <= all < max
}
```
<!-- .element: class="fragment stretch" -->

Ermöglicht äquivalent zu SQL die höchsten / niedrigsten Werte zu selektieren
<!-- .element: class="fragment" -->

---
#### `Comparator<T>`
```Java
public interface Comparator<T> {

    /**
    *   o1 < o2  ==> < 0
    *   o1 == o2 ==>  0
    *   o1 > o2  ==> > 0
    */
    int compare(T o1, T o2);
}
```
<!-- .element: class="fragment" -->

* OO - Repräsentation von Sortierregeln
<!-- .element: class="fragment fade-in-then-semi-out" -->
* zum Überschreiben der natural Order
<!-- .element: class="fragment fade-in-then-semi-out" -->
* oder falls keine vorhanden ist
<!-- .element: class="fragment" -->

----
#### Anwendung
```Java
List<String> caesars = 
        Arrays.asList("Cäsar", "Augustus", "Justinian", "Nero");
```
<!-- .element: class="fragment" -->

```plain
[Cäsar, Augustus, Justinian, Nero]
```
<!-- .element: class="fragment" -->

```Java
Collections.sort(caesars);
```
<!-- .element: class="fragment" -->

```plain
[Augustus, Cäsar, Justinian, Nero]
```
<!-- .element: class="fragment" -->

```Java
Comparator<String> byLength = new Comparator<>() {
    @Override
    public int compare(String o1, String o2) {
        return Integer.compare(o1.length(), o2.length());
    }
};
caesars.sort(byLength);
System.out.println(caesars);
```
<!-- .element: class="fragment" -->

```plain
[Nero, Cäsar, Augustus, Justinian]
```
<!-- .element: class="fragment" -->

---
#### Map / Dictionary / Assoziatives Array
```python
🐍
dictionary = {'POS': 'SCRE', 'AM': 'RIEE', 'E': 'NEUL'}
dicionary['POS'] # -> 'SCRE'
```
<!-- .element: class="fragment" -->

```Java
array["POS"]
```
<!-- .element: class="fragment" -->

```Plain text
🚫 incompatible types: java.lang.String cannot be converted to int
```
<!-- .element: class="fragment" -->

```Java
map.put("POS", "SCRE");
map.get("POS") -> "SCRE"
```
<!-- .element: class="fragment" -->

Speichern von *key* / *value* - Paaren
<!-- .element: class="fragment" -->

----
#### `Map<K, V>`
```Java
public interface Map<K, V> {

    int size();
    boolean isEmpty();
    boolean containsKey(Object key);
    boolean containsValue(Object value);
    V get(Object key);
    V put(K key, V value);
    V remove(Object key);
    void putAll(Map<? extends K, ? extends V> m);
    void clear();
    Set<K> keySet();
    Collection<V> values();
    Set<Map.Entry<K, V>> entrySet();
    default V getOrDefault(Object key, V defaultValue) { ... }
    default V putIfAbsent(K key, V value) { ... }
}
```
<!-- .element: class="fragment stretch" -->

----
#### Implementierungen
<dl>
    <dd class="fragment"><code>HashMap</code></dd>
    <dt class="fragment">Keys werden in einem <code>HashSet</code> gespeichert</dt>
    <dd class="fragment"><code>TreeMap</code></dd>
    <dt class="fragment">Keys werden in einem <code>TreeSet</code> gespeichert</dt>
    <dd class="fragment"><code>LinkedHashMap</code></dd>
    <dt class="fragment">Keys werden in einem <code>LinkedHashSet</code> gespeichert</dt>
</dl>

----
#### Iterieren
<dl>
    <dd class="fragment"><code>Set&ltK> keySet()</code></dd>
    <dt class="fragment">Liefert alle keys</dt>
    <dd class="fragment"><code>Collection&ltV> values()</code></dd>
    <dt class="fragment">Liefert alle values</dt>
    <dd class="fragment"><code>Set&ltMap.Entry&ltK, V>> entrySet()</code></dd>
    <dt class="fragment">Liefert alle key / value - Paare</dt>
</dl>

----
#### Wichtige Methoden
```Java
if (map.contains(key))
    result = map.get(key);
else
    result = defaultValue;
```
<!-- .element: class="fragment" -->

```Java
return map.getOrDefault(key, defaultValue);
```
<!-- .element: class="fragment" -->

```Java
if (!map.contains(key))
    map.put(key, newValue);
```
<!-- .element: class="fragment" -->

```Java
map.putIfAbsent(key, newValue);
```
<!-- .element: class="fragment" -->

---
#### `Queue<T>`
* Warteschlange, FIFO (oder FILO)
<!-- .element: class="fragment fade-in-then-semi-out" -->
* Teilweise mit beschränkter Kapazität
<!-- .element: class="fragment fade-in-then-semi-out" -->

```Java
public interface Queue<E> extends Collection<E> {

    boolean add(E e);       // full  -> Exception
    boolean offer(E e);     // full  -> false
    E remove();             // empty -> Exception     
    E poll();               // empty -> null
    E element();            // empty -> Exception
    E peek();               // empty -> null
}
```
<!-- .element: class="fragment" -->

----
#### `PriorityQueue<T>`
`remove`/`poll` entfernt immer Minimum
<!-- .element: class="fragment" -->

```Java
Queue<String> queue = new PriorityQueue<>();
queue.add("NVS");
queue.add("AM");
queue.add("D");
queue.add("POS");
System.out.println(queue);
```
<!-- .element: class="fragment" -->

```PlainText
⚠️ [AM, NVS, D, POS] ⚠️
```
<!-- .element: class="fragment" -->

```Java
StringJoiner joined = new StringJoiner(", "); 
while(!queue.isEmpty())
    joined.add(queue.remove());
System.out.println(joined);
```
<!-- .element: class="fragment" -->

```PlainText
AM, D, NVS, POS
```
<!-- .element: class="fragment" -->

note: 
* Minimum nicht eindeutig -> zufällig
* Reihenfolge bei sout irgendwas -> heap