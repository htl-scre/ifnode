# Visualisierung

---
#### Motivation
- Kommunikation
<!-- .element: class="fragment fade-in-then-semi-out" -->
- Präsentation
<!-- .element: class="fragment fade-in-then-semi-out" -->
- Interpretation
<!-- .element: class="fragment fade-in-then-semi-out" -->
- Story Telling (Manipulation?)
<!-- .element: class="fragment fade-in-then-semi-out" -->
- Monitoring
<!-- .element: class="fragment fade-in-then-semi-out" -->
- Unterhaltung
<!-- .element: class="fragment fade-in-then-semi-out" -->

[Productchart](https://www.productchart.com/laptops/) [Geizhals](https://geizhals.at/?cat=nb)
<!-- .element: class="fragment fade-in-then-semi-out" -->

---
#### Entwicklung des Hausmülls in Deutschland
![](./assets/visualization/muell1.png)
<!-- .element: class="r-stretch" -->

----
#### y-Achse *clever* schneiden
<div style="display: flex; gap: .5em; justify-content: space-around" class="fragment fade-in">
    <img src="assets/visualization/muell2.png">
    <img src="assets/visualization/muell-barplot.png" style="width:50%">
</div>  

---
#### Reduktion des Verteidigungshaushaltes
![](./assets/visualization/verteidigung.png)
<!-- .element: class="r-stretch" -->

----
#### 3D
<div style="display: flex; height:7em; gap: .5em; justify-content: space-around" class="fragment fade-in">
    <img src="assets/visualization/verteidigung.png" style="width:40%">
    <img src="assets/visualization/verteidigung-barplot.png">
</div>  
<img src="assets/visualization/verteidigung.png" style="transform: rotate(-20deg); height:7em" class="fragment fade-in">

----
#### Marktanalyse
<div style="display: flex; gap: .5em; justify-content: space-around" class="fragment fade-in">
    <img class="fragment" src="assets/visualization/noe-wins.png" style="width:40%">
    <img class="fragment" src="assets/visualization/ooe-wins.png">
</div> 

Ist NÖ oder OÖ wichtiger?
<!-- .element: class="fragment" -->
----
<div style="display: flex; gap: .5em; justify-content: space-around" class="fragment fade-in">
    <img src="assets/visualization/w-wins.png" style="width:40%">
<table>
        <thead>
            <tr>
                <th></th>
                <th>Umsatz</th>
                <th>%</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>NÖ</td>
                <td>45</td>
                <td>37</td>
            </tr>
            <tr>
                <td>OÖ</td>
                <td>22</td>
                <td>18</td>
            </tr>
            <tr>
                <td>Wien</td>
                <td>56</td>
                <td>45</td>
            </tr>
        </tbody>
    </table>
</div> 

---
#### Verkehr
![](./assets/visualization/verkehr.png)
<!-- .element: class="r-stretch" -->

----
#### Maximal 6 Features
<img class="fragment" src="assets/visualization/verkehr.png" style="width:40%">
<br>
<img class="fragment" src="assets/visualization/verkehr-agg.png" style="width:60%">

---
#### Bubble Plots

![](./assets/visualization/bubble.png)
<!-- .element: class="r-stretch" -->

<table>
<tr>
    <td>Österreich</td>
    <td>10</td>
</tr>
<tr>
    <td>Schweiz</td>
    <td class="fragment">20</td>
</tr>
<tr >
    <td>Deutschland</td>
    <td class="fragment">100</td>
</tr>
</table>

---
#### Österreichische Olympiamedaillen
![](./assets/visualization/medaillen.png)
<!-- .element: class="r-stretch" -->

Lineplots nur wenn 
<!-- .element: class="fragment" -->
* stetig
<!-- .element: class="fragment" -->
* (Zeitabstände gleich)
<!-- .element: class="fragment" -->

----
![](./assets/visualization/fr-wahlen.webp)
<!-- .element: class="r-stretch" -->

----
![](./assets/visualization/nvda.png)
<!-- .element: class="r-stretch" -->

---
#### Farbe - Unterscheidungshilfe
![](./assets/visualization/aapl-nvda.png)
<!-- .element: class="fragment fade-in" -->

----
#### Farbe - Klassifikation
![](./assets/visualization/eu-wahl.webp)
<!-- .element: class="r-stretch" -->

----
#### Farbe - numerische Daten
![](./assets/visualization/hochzeiten.jpg)
<!-- .element: class="r-stretch" -->

Farbintensität sollte mit Messgröße korrelieren
<!-- .element: class="fragment fade-in" -->

----
#### Farbe - Auswahl
![](./assets/visualization/color-palettes.svg)
<!-- .element: class="r-stretch" -->

[seaborn colors](https://seaborn.pydata.org/tutorial/color_palettes.html)
<!-- .element: class="fragment fade-in" -->

---
#### Minimalismus

$$data-ink-ratio = \frac{data-ink}{total-ink}$$
<!-- .element: class="fragment" -->
* 1 ⟹ optimal, keine überflüssigen Elemente
<!-- .element: class="fragment" -->
* 0 ⟹ schlecht, viele überflüssige Elemente
<!-- .element: class="fragment" -->
  * Backgrounds
    <!-- .element: class="fragment" -->
  * Schatten
    <!-- .element: class="fragment" -->
  * Rasterlinien
    <!-- .element: class="fragment" -->

---
![](./assets/visualization/chart.png)
<!-- .element: class="r-stretch" -->

----
![](./assets/visualization/mortality-crimea.jpg)
<!-- .element: class="r-stretch" -->

----
![](./assets/visualization/global-gdp-pie.jpg)
<!-- .element: class="r-stretch" -->

----
![](./assets/visualization/global-gdp.webp)
<!-- .element: class="r-stretch" -->

----
![](./assets/visualization/global-gdp-share.png)
<!-- .element: class="r-stretch" -->

---
![](./assets/visualization/wc-paths.webp)
<!-- .element: class="r-stretch" -->

---
#### WTF
![](./assets/visualization/florida-gun-deaths.jpg)
<!-- .element: class="r-stretch" -->

----
![](./assets/visualization/color.png)
<!-- .element: class="r-stretch" -->

----
![](./assets/visualization/doctor-gendering.png)
<!-- .element: class="r-stretch" -->

----
![](./assets/visualization/india-petrol-prices.jpg)
<!-- .element: class="r-stretch" -->

----
![](./assets/visualization/inverse-time.jpg)
<!-- .element: class="r-stretch" -->

----
![](./assets/visualization/portuguese-right-winger.png)
<!-- .element: class="r-stretch" -->

----
![](./assets/visualization/y-axis.jpg)
<!-- .element: class="r-stretch" -->

---
#### Datenmanipulation
![](./assets/visualization/bars.png)
<!-- .element: class="r-stretch" -->

----
#### Steigerung akzentuieren, Bars
![](./assets/visualization/bars-compressed.png)
<!-- .element: class="r-stretch" -->

----
#### Steigerung akzentuieren, Lines
<div style="display: flex; gap: .5em; justify-content: space-around; position:relative; left:-6rem" class="r-stretch">
    <img src="assets/visualization/uncompressed.png">
    <img src="assets/visualization/compressed.png">
</div>

----
#### Kontinuität akzentuieren
![](./assets/visualization/bars-extended.png)
<!-- .element: class="r-stretch" -->